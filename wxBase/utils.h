//
//  utils.h
//  wxBase
//
//  Created by Gordon Freeman on 11.12.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__utils__
#define __wxBase__utils__

#include <inttypes.h>
#include <string>
#include <map>
#include <thread>
#include <vector>

extern int strToInt(const std::string &input, int base = 10);
extern uint64_t strToUInt64(const std::string &input, int base = 16);
extern std::string intToStr(int value, int base = 10);
extern std::string uint64ToStr(uint64_t value, int base = 16);

#ifdef _MSC_VER
extern char * strptime(const char *s, const char *format, struct tm *tm);
#endif

class ManageThreads {
public:
    ManageThreads();
    ~ManageThreads();
    std::map<std::thread::id, int> &get();
    void add();
    void remove();
    bool exists();
    int id();
    void abort();
    bool aborted();
private:
    std::map<std::thread::id, int> mapThread;
    int thread_count;
    bool threads_aborted;
}; extern ManageThreads *myThreads;

struct map_structure {
    int index;
    std::string first;
    std::string second;
    char keke[100];
};

const std::string emptyString = "";

class MyMap {
public:
    MyMap();
    MyMap(const std::string &first, const std::string &second);
    MyMap(const std::string &first, const int &second);
    MyMap(const std::string &first, const uint64_t &second);
    ~MyMap();
    void insert(const std::string &first, const std::string &second);
    void insert(const std::string &first, const int &second);
    void insert(const std::string &first, const uint64_t &second);
    void modify(const std::string &first, std::string second);
    const std::vector<map_structure> &get() const;
    const std::string &operator[](const char *key);
    std::string toXml();
    std::string toXmlRequest();
    int size();
    long asLong(const char *key, int base = 10);
    int asInt(const char *key, int base = 10);
    uint64_t asUint64(const char *key, int base = 16);
private:
    int currentIndex;
    std::vector<map_structure> entries;
};

#define LOOP_LISTCTRL_SELECTED(code) { int i = -1; while ((i = listCtrl->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != -1) { code } }
#define LOOP_LISTCTRL_DONTCARE(code) { int i = -1; while ((i = listCtrl->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_DONTCARE)) != -1) { code } }

#define LOOP_LISTCTRL_SELECTED_P(parent, code) { int i = -1; while ((i = parent->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != -1) { code } }
#define LOOP_LISTCTRL_DONTCARE_P(parent, code) { int i = -1; while ((i = parent->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_DONTCARE)) != -1) { code } }

#define LISTITEM_SELECTED() listCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)
#define LISTITEM_SELECTED_P(parent) parent->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)

#endif
