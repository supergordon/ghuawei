//
//  Status.h
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__Connection__
#define __wxBase__Connection__

#include <wx/wx.h>

struct update_values {
    char op_full[32];
    char op_short[16];
    int rat, sc, pci, cell_id;
    int mcc, mnc, lac;
    bool roaming, registered;
    int rssi, rscp, ecio;
    int rsrq, rsrp, sinr;
    int status, mode, type, servicestatus;
    uint64_t download_speed, upload_speed;
    uint64_t download_traffic, upload_traffic;
    uint64_t month_download, month_upload, month_traffic_limit;
    bool month_data;
    int session_time;
    char ip[16];
};

class Status {
public:
    Status(wxWindow *win);
    ~Status();
    wxPanel *getPanel();
    
    void createControls();
    void updateControls();
    void updateTooltips();
    void setupSizer();
    void OnThread();
    void networkUpdate();
    void resetAverage();
    void setMonthData();
    int updateRate = 500;
    bool update;
    update_values values;
    
    wxStaticText *labelOperator;
    wxTextCtrl *textOperator;
    
    wxStaticText *labelMCC;
    wxTextCtrl *textMCC;
    
    wxStaticText *labelMNC;
    wxTextCtrl *textMNC;
    
    wxStaticText *labelRAT;
    wxTextCtrl *textRAT;
    
    wxStaticText *labelLAC;
    wxTextCtrl *textLAC;
    
    wxStaticText *labelLAI;
    wxTextCtrl *textLAI;
    
    wxStaticText *labelASU;
    wxTextCtrl *textASU;
    wxStaticText *textASUAverage;
    wxGauge *gaugeAsu;
    
    wxCheckBox *checkRoaming;
    //wxCheckBox *checkFlymode;
    wxCheckBox *checkRegistered;
    
    wxStaticText *labelCID;
    wxTextCtrl *textCID;
    
    wxStaticText *labelSC;
    wxTextCtrl *textSC;
    
    wxStaticText *labelRSSI;
    wxTextCtrl *textRSSI;
    wxStaticText *textRSSIAverage;
    wxGauge *gaugeRssi;
    
    wxStaticText *labelRSCP;
    wxTextCtrl *textRSCP;
    wxStaticText *textRSCPAverage;
    wxGauge *gaugeRscp;
    
    wxStaticText *labelECIO;
    wxTextCtrl *textECIO;
    wxStaticText *textECIOAverage;
    wxGauge *gaugeEcio;
    
    wxStaticText *labelConnectionStatus;
    wxStaticText *labelConnectionStatusLabel;
    
    wxStaticText *labelModeLabel;
    
    wxStaticText *labelMonth;
    
    wxStaticText *labelSession;
    wxStaticText *labelSessionLabel;
    
    wxTextCtrl *textIpAddress;
    
    wxStaticText *labelSpeed;
    wxStaticText *labelSpeedLabel1, *labelSpeedLabel2;
private:
    wxPanel *panel;
    void OnTimer(wxTimerEvent &event);
};

#endif
