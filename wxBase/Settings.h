//
//  Settings.h
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__Settings__
#define __wxBase__Settings__

#include <wx/wx.h>
#include <inttypes.h>

class Settings {
public:
    Settings(wxWindow *win);
    ~Settings();
    wxPanel *getPanel();
    
    void setupSizer();
    void setupControls();
    void setBandValues();
    
    std::string ScanBand(wxListCtrl* list, std::string bandName, int NetworkMode, uint64_t NetworkBand, uint64_t LTEBand);
    
    void OnSpinUpdaterate(wxSpinEvent &event);
    void OnButtonProjectMode(wxCommandEvent &event);
    void OnButtonDebugMode(wxCommandEvent &event);
    void OnButtonReboot(wxCommandEvent &event);
    void OnButtonReconnect(wxCommandEvent &event);
	void OnButtonHop(wxCommandEvent& event);
    void OnButtonConnect(wxCommandEvent &event);
    void OnButtonDisconnect(wxCommandEvent &event);
    void OnButtonApply(wxCommandEvent &event);
    void OnButtonPlmn(wxCommandEvent &event);
    void OnButtonWebPanel(wxCommandEvent &event);
    void OnRegister();
    void OnScan();
    void OnFullScan();
    void OnCheckUpnp(wxCommandEvent &event);
    void OnCheckDataRoaming(wxCommandEvent &event);
    void OnCheckDmz(wxCommandEvent &event);
    void OnCheckSipAlg(wxCommandEvent &event);
    void OnCheckFirewall(wxCommandEvent &event);
    void OnCheckAutomatic(wxCommandEvent &event);
    void OnCheckLteAutomatic(wxCommandEvent &event);

    static bool confirmChanges();
    
    wxButton *buttonProjectMode;
    wxButton *buttonDebugMode;
    wxButton *buttonReboot;
    wxButton *buttonReconnect;
	wxButton *buttonHop;
    wxButton *buttonWebPanel;

    wxCheckBox *checkUpnp;
    wxCheckBox *checkDataRoaming;
    wxCheckBox *checkDmz;
    wxCheckBox *checkSipAlg;
    wxCheckBox *checkFirewall;
    
    wxStaticText *labelUpdateRate;
    wxSpinCtrl *spinUpdateRate;
    
    wxCheckBox *cb_automatic;
	wxCheckBox* cb_gsm1900;
    wxCheckBox *cb_gsm1800;
    wxCheckBox *cb_gsm900;
	wxCheckBox* cb_gsm850;
    wxCheckBox *cb_wcdma2100;
    wxCheckBox *cb_wcdma900;
    
    wxCheckBox *cb_lte_automatic;
    wxCheckBox *cb_lte_b1;
    wxCheckBox *cb_lte_b3;
    wxCheckBox *cb_lte_b7;
    wxCheckBox *cb_lte_b8;
    wxCheckBox *cb_lte_b20;
	wxCheckBox* cb_lte_b38;
	wxCheckBox* cb_lte_b40;
    
    //wxStaticText *labelMode;
    wxChoice *choiceMode;
    
    wxButton *buttonApply;
    
    wxButton *buttonConnect;
    wxButton *buttonDisconnect;
	wxButton* buttonRenewIp;
    
    wxButton *buttonSetPlmn;
    wxListCtrl *listPlmn;
    std::string result_scan;
    wxDialog *dialogPlmn;
private:
    wxPanel *panel;
};

#endif
