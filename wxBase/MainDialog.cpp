//
//  MainDialog.cpp
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "MainDialog.h"
#include "main.h"
#include "Huawei.h"

MainDialog *mainDialog = nullptr;
Status *status = nullptr;
Info *info = nullptr;
SMS *sms = nullptr;
Profiles *profiles = nullptr;
//Graph *graph = nullptr;
Settings *settings = nullptr;

wxTimer *timer = nullptr;

wxFont g_globalFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Helvetica Neue"));
wxFont g_listFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Verdana"));
wxFont g_smsFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Helvetica Neue"));

MainDialog::MainDialog(wxString title) :
    wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(490, 470))
{
    //wxPrintf("MainDialog()\n");
    mainDialog = this;
    
    wxString stringTitle = wxString::Format("%s (%s) [%s]", title, VERSION, wxPlatformInfo::Get().GetOperatingSystemFamilyName());
    SetTitle(stringTitle);
    
    //windows
    if(wxPlatformInfo::Get().GetPortId() == wxPORT_MSW) {
		wxInitAllImageHandlers();
		SetIcon(wxIcon("APPICON", wxBITMAP_TYPE_ICO_RESOURCE));

        SetBackgroundColour(wxColor(0xCC, 0xCC, 0xCC));
        
        g_globalFont = wxFont(11, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Calibri"));
        g_listFont = wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Verdana"));
        g_smsFont = wxFont(12, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxT("Calibri"));
    }

    SetFont(g_globalFont);
    
    setupNotebook();

    Bind(wxEVT_CLOSE_WINDOW, [=] (wxCloseEvent &event) { mainDialog->OnClose(event); });
    
    SetMinSize(GetSize());
    //SetMaxSize(GetSize());
    CenterOnScreen();
}

MainDialog::~MainDialog()
{
    //wxPrintf("~MainDialog()\n");
    
    if(status) delete status;
    if(info) delete info;
    if(settings) delete settings;
    if(sms) delete sms;
    if(profiles) delete profiles;
    
    status = nullptr;
    info = nullptr;
    settings = nullptr;
    sms = nullptr;
    profiles = nullptr;
}

void MainDialog::OnTimer(wxTimerEvent &event)
{
    //auto start = std::chrono::steady_clock::now();
	static bool once = false;
	if (!once) {
		if (info) {
			info->updateControls(0);
		}
		once = true;
	}
    status->updateControls();

    //auto end = std::chrono::steady_clock::now();
    //typedef std::chrono::duration<int,std::milli> millisecs_t ;
    //millisecs_t duration( std::chrono::duration_cast<millisecs_t>(end-start) ) ;
    //std::cout << "timer: " << duration.count() << " milliseconds.\n" ;
}

void MainDialog::OnClose(wxCloseEvent &event)
{
    //wxPrintf("MainDialog::CloseWindow()\n");
    timer->Stop();
    myThreads->abort();
    
    //TODO: wait for the threads to finish
    
    if(status) delete status;
    if(info) delete info;
    if(settings) delete settings;
    if(sms) delete sms;
    if(profiles) delete profiles;
    //delete myThreads;
    
    status = nullptr;
    info = nullptr;
    settings = nullptr;
    sms = nullptr;
    profiles = nullptr;
    //myThreads = nullptr;
    event.Skip();
}

void MainDialog::setupNotebook()
{
    notebook = new wxNotebook(this, 0x150, wxDefaultPosition, wxDefaultSize, wxNB_TOP /*| wxNB_FIXEDWIDTH | wxNB_NOPAGETHEME*/);

    status = new Status(notebook);
    info = new Info(notebook);
    sms = new SMS(notebook);
    profiles = new Profiles(notebook);
   // graph = new Graph(notebook);
    settings = new Settings(notebook);
    
    notebook->AddPage(status->getPanel(), "Status");
    notebook->AddPage(info->getPanel(), "Info");
    notebook->AddPage(sms->getPanel(), "SMS");
    notebook->AddPage(profiles->getPanel(), "Profiles");
   // notebook->AddPage(graph->getPanel(), "Graph");
    notebook->AddPage(settings->getPanel(), "Settings");
    
    notebook->SetSelection(0);
    
    timer = new wxTimer(this, 0x300);
    timer->Start(status->updateRate);
    this->Connect(0x300, wxEVT_TIMER, wxTimerEventHandler(MainDialog::OnTimer));
}