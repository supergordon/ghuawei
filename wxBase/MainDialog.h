//
//  MainDialog.h
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__MainDialog__
#define __wxBase__MainDialog__

#include <inttypes.h>
#include <chrono>
#include <thread>
#include <wx/wx.h>
#include <wx/notebook.h>
#include <wx/statline.h>

#include "Status.h"
#include "Info.h"
#include "SMS.h"
#include "Profiles.h"
//#include "Graph.h"
#include "Settings.h"
#include "Huawei.h"


class MainDialog : public wxFrame {
public:
    MainDialog(wxString title);
    ~MainDialog();
private:
    void setupNotebook();
    
    void OnTimer(wxTimerEvent &event);
    void OnClose(wxCloseEvent &event);
    
    wxNotebook *notebook;
};

extern Status *status;
extern Info *info;
extern Settings *settings;
extern SMS *sms;
extern Profiles *profiles;
extern wxTimer *timer;
extern wxFont g_globalFont, g_listFont, g_smsFont;

#endif
