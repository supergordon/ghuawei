//
//  Info.cpp
//  wxBase
//
//  Created by Gordon Freeman on 05.12.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "Info.h"
#include "MainDialog.h"

Info::Info(wxWindow *win)
{
    //wxPrintf("Info()\n");
    
    listOutput = nullptr;
    panel = new wxPanel(win, wxID_ANY);
    
    int i = 0;
    mapChoices.insert( {i++, "api/device/information" } );
    mapChoices.insert( {i++, "api/device/basic_information" } );
    mapChoices.insert( {i++, "api/device/signal" } );
    mapChoices.insert( {i++, "api/dhcp/settings" } );
    mapChoices.insert( {i++, "api/dialup/connection" } );
    mapChoices.insert( {i++, "api/dialup/dialup-feature-switch" } );
    mapChoices.insert( {i++, "api/dialup/mobile-dataswitch" } );
    mapChoices.insert( {i++, "api/global/module-switch" } );
	mapChoices.insert( {i++, "api/language/current-language" } );
    mapChoices.insert( {i++, "api/monitoring/check-notifications" } );
    mapChoices.insert( {i++, "api/monitoring/converged-status" } );
    mapChoices.insert( {i++, "api/monitoring/month_statistics" } );
    mapChoices.insert( {i++, "api/monitoring/start_date" } );
    mapChoices.insert( {i++, "api/monitoring/status" } );
    mapChoices.insert( {i++, "api/monitoring/traffic-statistics" } );
    mapChoices.insert( {i++, "api/net/current-plmn" } );
    mapChoices.insert( {i++, "api/net/signal-para" } );
    mapChoices.insert( {i++, "api/net/net-mode" } );
    mapChoices.insert( {i++, "api/net/network" } );
	mapChoices.insert( {i++, "api/net/net-feature-switch" } );
    mapChoices.insert( {i++, "api/pin/status" } );
    mapChoices.insert( {i++, "api/pin/simlock" } );
    mapChoices.insert( {i++, "api/sms/config" } );
    mapChoices.insert( {i++, "api/sms/send-status" } );
    mapChoices.insert( {i++, "api/sms/sms-count" } );
    mapChoices.insert( {i++, "api/security/dmz" } );
    mapChoices.insert( {i++, "api/security/firewall-switch" } );
    mapChoices.insert( {i++, "api/security/sip" } );
    //mapChoices.insert( {i++, "api/security/url-filter" } );
    //mapChoices.insert( {i++, "api/security/lan-ip-filter" } );
    mapChoices.insert( {i++, "api/security/upnp" } );
    mapChoices.insert( {i++, "api/webserver/SesTokInfo" } );
    //api/dialup/dialup-feature-switch
    
    wxArrayString arrayChoices;
    for(auto it : mapChoices)
        arrayChoices.Add(wxString(it.second));
    
    choice = new wxChoice(panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, arrayChoices);
	choice->SetSelection(0);
    choice->Bind(wxEVT_CHOICE, [] (wxCommandEvent &event) { info->updateControls(event.GetSelection()); });
    
    listOutput = new wxListCtrl(panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_VRULES | wxLC_HRULES);
    int c0 = listOutput->InsertColumn(0, "Name");
    int c1 = listOutput->InsertColumn(1, "Value");
    listOutput->SetColumnWidth(c0, 170);
    listOutput->SetColumnWidth(c1, 240);
    
    listOutput->SetFont(g_listFont);
    
    wxBoxSizer *sizerMain = new wxBoxSizer(wxVERTICAL);
    sizerMain->Add(choice, 0, wxGROW | wxALL, 5);
    sizerMain->Add(listOutput, 4, wxGROW | wxALL, 5);
    panel->SetSizerAndFit(sizerMain);
}

Info::~Info()
{
    //wxPrintf("~Info()\n");
}

wxPanel *Info::getPanel()
{
    return panel;
}

void Info::updateControls(int selection)
{
    hilink->UpdateToken();
    
    std::string tmp = hilink->getXml(mapChoices[selection]);
    auto _map = hilink->xmlToMap(tmp);
    
    listOutput->DeleteAllItems();
    int j = 0;
    
    for(auto it : _map.get()) {
        int i = listOutput->InsertItem(j++, wxString::Format(" %s", it.first));
        listOutput->SetItem(i, 1, wxString::Format(" %s", it.second));
    }
}