//
//  main.h
//  wxBase
//
//  Created by Gordon Freeman on 17.11.12.
//  Copyright (c) 2012 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__main__
#define __wxBase__main__

#include <inttypes.h>

#define VERSION "v1.0"

class Dialog: public wxApp {
private:
    virtual bool OnInit();
    bool isDeviceReady();
public:
};

#endif

/*
 PATH=$PATH:/usr/local/i386-mingw32-4.3.0/bin/
 
 cd $SRCROOT/wxBase
 i386-mingw32-g++ *.cpp -I/usr/local/i386-mingw32-4.3.0/lib/wx/include/i386-mingw32-msw-unicode-static-2.9 -I/usr/local/i386-mingw32-4.3.0/include/wx-2.9 -D_LARGEFILE_SOURCE=unknown -D__WXMSW__ -mthreads -L/usr/local/i386-mingw32-4.3.0/lib   -Wl,--subsystem,windows -mwindows /usr/local/i386-mingw32-4.3.0/lib/libwx_mswu_xrc-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_mswu_webview-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_mswu_qa-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_baseu_net-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_mswu_html-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_mswu_adv-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_mswu_core-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_baseu_xml-2.9-i386-mingw32.a /usr/local/i386-mingw32-4.3.0/lib/libwx_baseu-2.9-i386-mingw32.a -lwxregexu-2.9-i386-mingw32 -lwxexpat-2.9-i386-mingw32 -lwxtiff-2.9-i386-mingw32 -lwxjpeg-2.9-i386-mingw32 -lwxpng-2.9-i386-mingw32 -lwxzlib-2.9-i386-mingw32 -lrpcrt4 -loleaut32 -lole32 -luuid -lwinspool -lwinmm -lshell32 -lcomctl32 -lcomdlg32 -ladvapi32 -lwsock32 -lgdi32 -o $TARGET_BUILD_DIR/win32.exe
 i386-mingw32-strip $TARGET_BUILD_DIR/win32.exe
 */