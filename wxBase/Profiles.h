//
//  Profiles.h
//  wxBase
//
//  Created by Gordon Freeman on 08.12.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__Profiles__
#define __wxBase__Profiles__

#include "wx/wx.h"
#include "wx/listctrl.h"
#include "wx/spinctrl.h"
#include "Huawei.h"

class Profiles {
public:
    Profiles(wxWindow *win);
    ~Profiles();
    wxPanel *getPanel();
    void createControls();
    void setupSizers();
    void setupControls();
    void updateControls();
    void updateDetails(int index);
    void updateListProfiles();
    void OnButtonRefresh(wxCommandEvent &event);
    void OnButtonAdd(wxCommandEvent &event);
    void OnButtonChange(wxCommandEvent &event);
    void OnButtonDelete(wxCommandEvent &event);
    void OnButtonSetDefault(wxCommandEvent &event);
    void OnItemActivated(wxListEvent &event);
    
    wxListCtrl *listProfiles = nullptr;
    wxButton *buttonRefresh = nullptr;
    wxButton *buttonAdd = nullptr;
    wxButton *buttonEdit = nullptr;
    wxButton *buttonDelete = nullptr;
    wxButton *buttonDefault = nullptr;
    
    wxStaticText *labelIndex = nullptr;
    wxStaticText *labelIndexLabel = nullptr;
    wxCheckBox *checkIsValid = nullptr;
    wxStaticText *labelReadOnly = nullptr;
    wxSpinCtrl *spinReadOnly = nullptr;
    
    wxStaticText *labelName = nullptr;
    wxTextCtrl *textName = nullptr;
    wxStaticText *labelDialupNum = nullptr;
    wxTextCtrl *textDialupNum = nullptr;
    wxStaticText *labelUsername = nullptr;
    wxTextCtrl *textUsername = nullptr;
    wxStaticText *labelPassword = nullptr;
    wxTextCtrl *textPassword = nullptr;
    
    wxStaticText *labelApn = nullptr;
    wxTextCtrl *textApn = nullptr;
    wxCheckBox *checkApnIsStatic = nullptr;
    
    wxStaticText *labelIp = nullptr;
    wxTextCtrl *textIp = nullptr;
    wxCheckBox *checkIp = nullptr;
    wxCheckBox *checkDns = nullptr;
    
    wxStaticText *labelPrimaryDns = nullptr;
    wxTextCtrl *textPrimaryDns = nullptr;
    wxStaticText *labelSecondaryDns = nullptr;
    wxTextCtrl *textSecondaryDns = nullptr;
    
    wxStaticText *labelAuthMode = nullptr;
    wxSpinCtrl *spinAuthMode = nullptr;
    wxStaticText *labelIpType = nullptr;
    wxSpinCtrl *spinIpType = nullptr;
private:
    wxPanel *panel;
};

#endif
