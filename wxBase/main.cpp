#include <wx/wx.h>
#include <wx/textdlg.h>
#include "utils.h"
#include "Huawei.h"
#include "main.h"
#include "MainDialog.h"

IMPLEMENT_APP(Dialog)

bool Dialog::isDeviceReady()
{
    int tries = 0;
    
    while (!hilink->IsDeviceOnline()) {
        
        if(tries == 5) {
            wxMessageBox(L"Too many tries. Aborting...");
            return false;
        }
        
        wxString ipAddressFilter[11] = {L"0", L"1", L"2", L"3", L"4", L"5", L"6", L"7", L"8", L"9", L"."};
        wxArrayString arraystrIPAddress(11, ipAddressFilter);
        wxTextValidator ip_validator(wxFILTER_INCLUDE_CHAR_LIST);
        ip_validator.SetIncludes(arraystrIPAddress);
        
        wxTextEntryDialog *ted = new wxTextEntryDialog(NULL, L"Choose another ip", L"Device could not be found", hilink->GetIpAddress());
        ted->SetTextValidator(ip_validator);
        ted->ShowModal();
        ted->Destroy();
        hilink->SetIpAddress(std::string(ted->GetValue()));
        
        ++tries;
    }

    return true;
}

bool Dialog::OnInit()
{
    curl_global_init(CURL_GLOBAL_ALL);
    
    myThreads = new ManageThreads();
    myThreads->add();

    if(!isDeviceReady())
        return false;
    
    hilink->Init();
    
    if(hilink->loginNeeded()) {
        wxMessageBox(L"Devices with logins are not supported yet!\nExiting...", L"Notice", wxICON_WARNING);
        return false;
    }
    
    hilink->UpdateToken();
    
    //auto tmp = hilink->api_device_basic_information();
    //tmp["sdcard_enabled"] = "1";
    //wxPrintf("%s\n", tmp.toXmlRequest());
    //std::string lol = "<request><multimode>1</multimode></request>";
    //wxPrintf("%s\n", hilink->postXml("api/device/basic_information", lol));
    //wxPrintf("%s\n", hilink->getXml("api/ddns/ddns-list"));
    

    if(wxGetApp().argc == 2) {
        
        wxString arg = wxString(wxGetApp().argv[1]);
        wxPrintf("Arg: %s\n", arg);
        
        if(arg == L"reconnect") {
            hilink->MobileReconnect();
        }
        
        myThreads->remove();
    }
    else {
        MainDialog *mainDialog = new MainDialog(L"Huawei E3372h Tool by Gordon`");
        mainDialog->Show();
    
        return true;
    }
    
    return false;
}