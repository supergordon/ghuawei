//
//  Status.cpp
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//
#include <wx/gauge.h>
#include <wx/tooltip.h>

#include <thread>
#include <chrono>
#include <mutex>
#include <algorithm>
#include <iterator>

#include "Status.h"
#include "Huawei.h"
#include "MainDialog.h"
#include "main.h"

//int g_mode = Hilink::NETWORK_3G;

std::vector<int> vRssi, vRscp, vEcio, vAsu;

double average(const std::vector<int> &vec)
{
    int count = int(vec.size());
    if(!count)
        return 0;
    
    double average = 0;
    
    for(auto it : vec)
        average += it;
    
    average /= count;
    
    return average;
}

int min(const std::vector<int> &vec)
{
    if(!vec.size()) return 0;
    return *std::min_element(std::begin(vec), std::end(vec));
}

int max(const std::vector<int> &vec)
{
    if(!vec.size()) return 0;
    return *std::max_element(std::begin(vec), std::end(vec));
}

Status::Status(wxWindow *win)
{
    //wxPrintf("Status()\n");
    
    panel = new wxPanel(win, 0x100);
    
    updateRate = 500;
    update = false;
    memset(&values, 0, sizeof(update_values));

    std::thread t1([this]() { OnThread(); });
    t1.detach();
    
    createControls();
    updateControls();
    setupSizer();
}

Status::~Status()
{
    //wxPrintf("~Status()\n");
}

wxPanel *Status::getPanel()
{
    return panel;
}

void Status::createControls()
{
    int textFlags = wxTE_CENTRE | wxALIGN_CENTRE_HORIZONTAL;
    
    if(wxPlatformInfo::Get().GetPortId() == wxPORT_OSX)
        textFlags |= wxTE_READONLY;
    
    labelOperator = new wxStaticText(panel, 0x101, "Operator:", wxDefaultPosition, wxSize(65, -1), wxALIGN_RIGHT);
    textOperator = new wxTextCtrl(panel, 0x102, wxEmptyString, wxDefaultPosition, wxSize(150, -1), textFlags);
    
    labelMCC = new wxStaticText(panel, 0x103, "MCC:", wxDefaultPosition, wxSize(40, -1), wxALIGN_RIGHT);
    textMCC = new wxTextCtrl(panel, 0x104, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    
    labelMNC = new wxStaticText(panel, 0x105, "MNC:", wxDefaultPosition, wxSize(40, -1), wxALIGN_RIGHT);
    textMNC = new wxTextCtrl(panel, 0x106, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    
    labelRAT = new wxStaticText(panel, 0x107, "RAT:", wxDefaultPosition, wxSize(40, -1), wxALIGN_RIGHT);
    textRAT = new wxTextCtrl(panel, 0x108, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);

    labelLAC = new wxStaticText(panel, 0x109, "LAC:", wxDefaultPosition, wxSize(40, -1), wxALIGN_RIGHT);
    textLAC = new wxTextCtrl(panel, 0x10A, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    
    labelLAI = new wxStaticText(panel, 0x10B, "LAI:", wxDefaultPosition, wxSize(40, -1), wxALIGN_RIGHT);
    textLAI = new wxTextCtrl(panel, 0x10C, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    
    checkRoaming = new wxCheckBox(panel, 0x10D, "Roaming", wxDefaultPosition, wxSize(100, -1));
    checkRegistered = new wxCheckBox(panel, 0x10E, "Registered", wxDefaultPosition, wxSize(100, -1));

    labelCID = new wxStaticText(panel, 0x10F, "Cell-ID:", wxDefaultPosition, wxSize(65, -1), wxALIGN_RIGHT);
    textCID = new wxTextCtrl(panel, 0x110, wxEmptyString, wxDefaultPosition, wxSize(150, -1), textFlags);
    
    labelSC = new wxStaticText(panel, 0x111, "SC:", wxDefaultPosition, wxSize(40, -1), wxALIGN_RIGHT);
    textSC = new wxTextCtrl(panel, 0x112, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    
    labelRSSI = new wxStaticText(panel, 0x113, "RSSI:", wxDefaultPosition, wxSize(50, -1));
    textRSSI = new wxTextCtrl(panel, 0x114, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    textRSSIAverage = new wxStaticText(panel, 0x130, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxALIGN_CENTRE_HORIZONTAL);
    gaugeRssi = new wxGauge(panel, 0x115, 65, wxDefaultPosition, wxSize(280, -1), wxGA_HORIZONTAL | wxGA_SMOOTH); //RSSI 4G 89
    
    labelRSCP = new wxStaticText(panel, 0x116, "RSCP:", wxDefaultPosition, wxSize(50, -1));
    textRSCP = new wxTextCtrl(panel, 0x117, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    textRSCPAverage = new wxStaticText(panel, 0x131, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxALIGN_CENTRE_HORIZONTAL);
    gaugeRscp = new wxGauge(panel, 0x118, 65, wxDefaultPosition, wxSize(280, -1), wxGA_HORIZONTAL | wxGA_SMOOTH);
    
    labelECIO = new wxStaticText(panel, 0x119, "Ec/Io:", wxDefaultPosition, wxSize(50, -1));
    textECIO = new wxTextCtrl(panel, 0x11A, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    textECIOAverage = new wxStaticText(panel, 0x132, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxALIGN_CENTRE_HORIZONTAL);
    gaugeEcio = new wxGauge(panel, 0x11B, 24, wxDefaultPosition, wxSize(280, -1), wxGA_HORIZONTAL | wxGA_SMOOTH);
    
    labelASU = new wxStaticText(panel, 0x11C, "ASU:", wxDefaultPosition, wxSize(50, -1));
    textASU = new wxTextCtrl(panel, 0x11D, wxEmptyString, wxDefaultPosition, wxSize(40, -1), textFlags);
    textASUAverage = new wxStaticText(panel, 0x133, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxALIGN_CENTRE_HORIZONTAL);
    gaugeAsu = new wxGauge(panel, 0x11E, 91, wxDefaultPosition, wxSize(280, -1), wxGA_HORIZONTAL | wxGA_SMOOTH);
    
    labelConnectionStatus = new wxStaticText(panel, 0x11F, "Status:", wxDefaultPosition, wxSize(70, -1));
	labelConnectionStatusLabel = new wxStaticText(panel, 0x120, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    
    labelModeLabel = new wxStaticText(panel, 0x121, wxEmptyString, wxDefaultPosition, wxSize(200, -1), wxALIGN_RIGHT);
    
    labelMonth = new wxStaticText(panel, 0x127, wxEmptyString, wxDefaultPosition, wxSize(120, -1), wxALIGN_RIGHT);
    
    labelSession = new wxStaticText(panel, 0x122, "Session:", wxDefaultPosition, wxSize(70, -1));
    labelSessionLabel = new wxStaticText(panel, 0x123, wxEmptyString, wxDefaultPosition, wxSize(200, -1));
    
    labelSpeed = new wxStaticText(panel, 0x124, "Speed:", wxDefaultPosition, wxSize(70, -1));
    labelSpeedLabel1 = new wxStaticText(panel, 0x125, wxEmptyString, wxDefaultPosition, wxSize(200, -1));
	labelSpeedLabel2 = new wxStaticText(panel, 0x127, wxEmptyString, wxDefaultPosition, wxSize(200, -1));
    
    textIpAddress = new wxTextCtrl(panel, 0x126, wxEmptyString, wxDefaultPosition, wxSize(120, -1), textFlags);
    
    labelConnectionStatusLabel->SetToolTip("Double click to toggle connection");
    labelConnectionStatusLabel->Bind(wxEVT_LEFT_DCLICK, [] (wxMouseEvent &event) { hilink->MobileToggleConnection(); });

	labelSpeed->SetToolTip("Double click to hop");
	labelSpeed->Bind(wxEVT_LEFT_DCLICK, [](wxMouseEvent& event) { hilink->MobileRenewIp(); });

    textRSSIAverage->Bind(wxEVT_ENTER_WINDOW, [this] (wxMouseEvent &event) { updateTooltips(); });
    textRSCPAverage->Bind(wxEVT_ENTER_WINDOW, [this] (wxMouseEvent &event) { updateTooltips(); });
    textECIOAverage->Bind(wxEVT_ENTER_WINDOW, [this] (wxMouseEvent &event) { updateTooltips(); });
    textASUAverage->Bind(wxEVT_ENTER_WINDOW, [this] (wxMouseEvent &event) { updateTooltips(); });
        
    textRSSIAverage->Bind(wxEVT_LEFT_DCLICK, [this] (wxMouseEvent &event) { resetAverage(); } );
    textRSCPAverage->Bind(wxEVT_LEFT_DCLICK, [this] (wxMouseEvent &event) { resetAverage(); } );
    textECIOAverage->Bind(wxEVT_LEFT_DCLICK, [this] (wxMouseEvent &event) { resetAverage(); } );
    textASUAverage->Bind(wxEVT_LEFT_DCLICK, [this] (wxMouseEvent &event) { resetAverage(); } );
    
    labelMonth->Bind(wxEVT_LEFT_DCLICK, [this] (wxMouseEvent &event) { setMonthData(); } );
}

void Status::updateControls()
{
    int selection = reinterpret_cast<wxNotebook*>(wxWindow::FindWindowById(0x150))->GetSelection();
    if(selection != 0)
        return;
    
    for(int i = 0x100; i < 0x127; i++)
        if(!wxWindow::FindWindowById(i))
            return;
    
    if(!hilink->IsDeviceOnline()) {
        wxPrintf("x");
        labelConnectionStatusLabel->SetLabel("Device NOT detected");
        labelConnectionStatusLabel->SetForegroundColour(wxColor(255, 0, 0));
        return;
    }

    wxString stringFullName = wxString(values.op_full);
    wxString stringShortName = wxString(values.op_short);
    
    wxString stringOperator = wxString::Format("%s / %s", stringFullName, stringShortName);
    if(stringFullName.length() == stringShortName.length())
        stringOperator = stringFullName;
    
    if(textOperator->GetValue() != stringOperator) textOperator->SetValue(stringOperator);
    
    wxString stringMCC = wxString::Format("%i", values.mcc);
    if(textMCC != stringMCC) textMCC->SetValue(stringMCC);
    
    wxString stringMNC = wxString::Format("%i", values.mnc);
    if(textMNC != stringMNC) textMNC->SetValue(stringMNC);
    
    wxString stringRAT = wxString::Format("%s", Hilink::GetNetworkModeText(values.rat));
    if(textRAT != stringRAT) textRAT->SetValue(stringRAT);

	wxString stringLAC = wxString::Format("%i", values.lac);
    if(textLAC->GetValue() != stringLAC) textLAC->SetValue(stringLAC);
    
    int lai = values.mcc + values.mnc + values.lac;
	wxString stringLAI = wxString::Format("%i", lai);
    if(textLAI->GetValue() != stringLAI) textLAI->SetValue(stringLAI);

    if(checkRoaming->GetValue() != values.roaming) checkRoaming->SetValue(values.roaming);
    if(checkRegistered->GetValue() != values.registered) checkRegistered->SetValue(values.registered);
    
    int rnc_id = int(values.cell_id / 65536);
    int cell_id_short = values.cell_id % 65536;
    
    wxString stringCID;
    if(values.rat == Hilink::NETWORK_3G)
        stringCID = wxString::Format("%i | %i-%i", int(values.cell_id), rnc_id, cell_id_short);
    else
        stringCID = wxString::Format("%i", int(values.cell_id));
    if(textCID->GetValue() != stringCID) textCID->SetValue(stringCID);
    
    int *sc = &values.sc;
    int *rscp_ = &values.rscp;
    int *ecio_ = &values.ecio;
    wxString strSC, strRSCP, strECIO, strASU;
    int asu = 0;
    int max = 116;
    
    switch(values.rat) {
        case Hilink::NETWORK_2G:
            asu = values.rssi + 116;
            strRSCP = "RSCP:";
            strECIO = "Ec/Io:";
            strASU = "ASU:";
            strSC = "SC:";
            if(gaugeRssi->GetRange() != 65) gaugeRssi->SetRange(65);
            if(gaugeRscp->GetRange() != 65) gaugeRscp->SetRange(65);
            if(gaugeEcio->GetRange() != 24) gaugeEcio->SetRange(24);
            if(gaugeAsu->GetRange() != 91) gaugeAsu->SetRange(91);
            
            values.rscp = 0;
            values.ecio = 0;
            if(gaugeRscp->GetValue() != 0) gaugeRscp->SetValue(0);
            if(gaugeEcio->GetValue() != 0) gaugeEcio->SetValue(0);
            rscp_ = &values.rscp;
            ecio_ = &values.ecio;
            break;
        case Hilink::NETWORK_3G:
            max = 116;
            sc = &values.sc;
            rscp_ = &values.rscp;
            ecio_ = &values.ecio;
            asu = values.rscp + 116;
            strSC = "SC:";
            strRSCP = "RSCP:";
            strECIO = "Ec/Io:";
            strASU = "ASU:";
            if(gaugeRssi->GetRange() != 65) gaugeRssi->SetRange(65);
            if(gaugeRscp->GetRange() != 65) gaugeRscp->SetRange(65);
            if(gaugeEcio->GetRange() != 24) gaugeEcio->SetRange(24);
            if(gaugeAsu->GetRange() != 91) gaugeAsu->SetRange(91);
            break;
        case Hilink::NETWORK_4G:
            max = 140;
            sc = &values.pci;
            rscp_ = &values.rsrp;
            ecio_ = &values.rsrq;
            asu = values.sinr;
            strSC = "PCI:";
            strRSCP = "RSRP:";
            strECIO = "RSRQ:";
            strASU = "SINR:";
            if(gaugeRssi->GetRange() != 96) gaugeRssi->SetRange(96);
            if(gaugeRscp->GetRange() != 96) gaugeRscp->SetRange(96);
            if(gaugeEcio->GetRange() != 17) gaugeEcio->SetRange(17);
            if(gaugeAsu->GetRange() != 40) gaugeAsu->SetRange(40);

            break;
    };
    
	wxString stringSC = wxString::Format("%i", *sc);
    if(textSC->GetValue() != stringSC) textSC->SetValue(stringSC);
    if(labelSC->GetLabel() != strSC) labelSC->SetLabel(strSC);
    
    wxString stringRSSI = wxString::Format("%i", values.rssi);
    if(textRSSI->GetLabel() != stringRSSI) textRSSI->SetLabel(stringRSSI);
    
    wxString stringRSSIAverage = wxString::Format("%.1f", average(vRssi));
    if(textRSSIAverage->GetLabel() != stringRSSIAverage) {
        wxString buffer = textRSSIAverage->GetLabel();
        textRSSIAverage->SetLabel(stringRSSIAverage);
        if(buffer.length()) {
            long value = 0;
            buffer.ToLong(&value);
            
            if(value < average(vRssi))
                textRSSIAverage->SetForegroundColour(wxColor(0, 100, 0));
            else
                textRSSIAverage->SetForegroundColour(wxColor(139, 0, 0));
        }
    }
    
    if(values.rat !=  Hilink::NETWORK_2G) {
        wxString stringRSCP = wxString::Format("%i", *rscp_);
        if(textRSCP->GetLabel() != stringRSCP) textRSCP->SetLabel(stringRSCP);
            
        wxString stringRSCPAverage = wxString::Format("%.1f", average(vRscp));
        if(textRSCPAverage->GetLabel() != stringRSCPAverage) {
            wxString buffer = textRSCPAverage->GetLabel();
            textRSCPAverage->SetLabel(stringRSCPAverage);
            if(buffer.length()) {
                long value = 0;
                buffer.ToLong(&value);
                
                if(value < average(vRscp))
                    textRSCPAverage->SetForegroundColour(wxColor(0, 100, 0));
                else
                    textRSCPAverage->SetForegroundColour(wxColor(139, 0, 0));
            }
        }
        if(labelRSCP->GetLabel() != strRSCP) labelRSCP->SetLabel(strRSCP);

        wxString stringECIO = wxString::Format("%i", *ecio_);
        if(textECIO->GetLabel() != stringECIO) textECIO->SetLabel(stringECIO);

        wxString stringECIOAverage = wxString::Format("%.1f", average(vEcio));
        if(textECIOAverage->GetLabel() != stringECIOAverage) {
            wxString buffer = textECIOAverage->GetLabel();
            
            textECIOAverage->SetLabel(stringECIOAverage);
            if(buffer.length()) {
                long value = 0;
                buffer.ToLong(&value);
                
                if(value < average(vEcio))
                    textECIOAverage->SetForegroundColour(wxColor(0, 100, 0));
                else
                    textECIOAverage->SetForegroundColour(wxColor(139, 0, 0));
            }
        }
        if(labelECIO->GetLabel() != strECIO) labelECIO->SetLabel(strECIO);
    }
    
    wxString stringASU = wxString::Format("%i", asu);
    if(textASU->GetLabel() != stringASU) textASU->SetLabel(stringASU);
    
    wxString stringASUAverage = wxString::Format("%.1f", average(vAsu));
    if(textASUAverage->GetLabel() != stringASUAverage) {
        wxString buffer = textASUAverage->GetLabel();
        
        textASUAverage->SetLabel(stringASUAverage);
        if(buffer.length()) {
            long value = 0;
            buffer.ToLong(&value);
            
            if(value < average(vAsu))
                textASUAverage->SetForegroundColour(wxColor(0, 100, 0));
            else
                textASUAverage->SetForegroundColour(wxColor(139, 0, 0));
        }
    }
    if(labelASU->GetLabel() != strASU) labelASU->SetLabel(strASU);
    
    int rssi = max + values.rssi;
    if(gaugeRssi->GetValue() != rssi) gaugeRssi->SetValue(rssi);
    
    if(values.rat !=  Hilink::NETWORK_2G) {
        int rscp = max + *rscp_;
        if(gaugeRscp->GetValue() != rscp) gaugeRscp->SetValue(rscp);

        int ecio = -*ecio_;
        if(gaugeEcio->GetValue() != ecio) gaugeEcio->SetValue(ecio);
    }
    
    if(gaugeAsu->GetValue() != asu) gaugeAsu->SetValue(asu);
    
	if (values.status == 900 || values.status == 901)
		labelConnectionStatusLabel->SetForegroundColour(wxColor(0, 100, 0));
	else
		labelConnectionStatusLabel->SetForegroundColour(wxColor(139, 0, 0));

    wxString stringConnectionStatus = wxString::Format("%s [%i]", Hilink::GetConnectionStatusText(values.status, values.servicestatus), values.status);
    if(labelConnectionStatusLabel->GetLabel() != stringConnectionStatus)
        labelConnectionStatusLabel->SetLabel(stringConnectionStatus);
       
    wxString stringMode = wxString::Format("%s | %s ", Hilink::GetModeText(values.mode), Hilink::GetNetworkTypeText(values.type));
    if(labelModeLabel->GetLabel() != stringMode) labelModeLabel->SetLabel(stringMode);

    double traffic = double(values.download_traffic + values.upload_traffic);
    
    int weeks = values.session_time / 604800;
    int days = (values.session_time / 86400) - (weeks * 7);
    int hours = (values.session_time / 3600) - (days * 24) - (weeks * 168);
    int minutes = (values.session_time / 60) - (hours * 60) - (days * 1440) - (weeks * 10080);
    int seconds = values.session_time - (minutes * 60) - (hours * 3600) - (days * 86400) - (weeks * 604800);
    
    
    double traffic_bytes = traffic;
    double traffic_kilobytes = traffic / 1000;
    double traffic_megabytes = traffic / 1000 / 1000;
    double traffic_gigabytes = traffic / 1000 / 1000 / 1000;

    wxString stringTraffic;
    if(traffic_gigabytes < 1000) stringTraffic = wxString::Format("%.2f GB", traffic_gigabytes);
    if(traffic_megabytes < 1000) stringTraffic = wxString::Format("%.2f MB", traffic_megabytes);
    if(traffic_kilobytes < 1000) stringTraffic = wxString::Format("%.2f KB", traffic_kilobytes);
    if(traffic_bytes < 1000) stringTraffic = wxString::Format("%.0f B", traffic_bytes);
  
    wxString stringSession;
    if(weeks) stringSession = wxString::Format("%02i:%02i:%02i:%02i:%02i, %s", weeks, days, hours, minutes, seconds, stringTraffic);
    else if(days) stringSession = wxString::Format("%02i:%02i:%02i:%02i, %s", days, hours, minutes, seconds, stringTraffic);
    else if(hours) stringSession = wxString::Format("%02i:%02i:%02i, %s", hours, minutes, seconds, stringTraffic);
    else stringSession = wxString::Format("%02i:%02i, %s", minutes, seconds, stringTraffic);
    if(labelSessionLabel->GetLabel() != stringSession) labelSessionLabel->SetLabel(stringSession);
    
    
    wxString stringMonthTraffic;
    
    if(values.month_data) {
        double month_cur_traffic = double(values.month_download + values.month_upload);
        double month_max_traffic = double(values.month_traffic_limit);
        //double month_traffic_left = month_max_traffic - month_cur_traffic;
        stringMonthTraffic = wxString::Format("%.2f of %.0f GB", month_cur_traffic / 1000 / 1000 / 1000,
                                                   month_max_traffic / 1000 / 1000 / 1000);
    }
    else {
        stringMonthTraffic = "+";
    }
    
    if(labelMonth->GetLabel() != stringMonthTraffic)
        labelMonth->SetLabel(stringMonthTraffic);
    
    wxString stringIpAddress = wxString::Format("%s", values.ip);
    if(textIpAddress->GetValue() != stringIpAddress) textIpAddress->SetValue(stringIpAddress);
    
    double downstream = double(values.download_speed) * 8 / 1000 / 1000;
    double upstream = double(values.upload_speed) * 8 / 1000 / 1000;

    double downstream_mb = double(values.download_speed) / 1000 / 1000;
    double upstream_mb = double(values.upload_speed)  / 1000 / 1000; 
    
    wxString stringSpeed1 = wxString::Format("v %2.1f [%1.2f]", downstream, downstream_mb);
	wxString stringSpeed2 = wxString::Format("^ %2.1f [%1.2f]", upstream, upstream_mb);

	if (labelSpeedLabel1->GetLabel() != stringSpeed1) labelSpeedLabel1->SetLabel(stringSpeed1);
	if (labelSpeedLabel2->GetLabel() != stringSpeed2) labelSpeedLabel2->SetLabel(stringSpeed2);
}

void Status::updateTooltips()
{
    wxToolTip *tooltip_rssi = new wxToolTip(wxString::Format("min: %i\navg: %.2f\nmax: %i", min(vRssi), average(vRssi), max(vRssi)));
    wxToolTip *tooltip_rscp = new wxToolTip(wxString::Format("min: %i\navg: %.2f\nmax: %i", min(vRscp), average(vRscp), max(vRscp)));
    wxToolTip *tooltip_ecio = new wxToolTip(wxString::Format("min: %i\navg: %.2f\nmax: %i", min(vEcio), average(vEcio), max(vEcio)));
    wxToolTip *tooltip_asu = new wxToolTip(wxString::Format("min: %i\navg: %.2f\nmax: %i", min(vAsu), average(vAsu), max(vAsu)));
    
    textRSSIAverage->SetToolTip(tooltip_rssi);
    textRSCPAverage->SetToolTip(tooltip_rscp);
    textECIOAverage->SetToolTip(tooltip_ecio);
    textASUAverage->SetToolTip(tooltip_asu);
}

void Status::setupSizer()
{
    wxStaticBoxSizer *sizer1 = new wxStaticBoxSizer(wxVERTICAL, panel, "Network");
    wxBoxSizer *sizer11 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer12 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer13 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer11->Add(labelOperator, 0, wxUP, 3);
    sizer11->AddSpacer(5);
    sizer11->Add(textOperator);
    sizer11->AddSpacer(35);
    sizer11->Add(labelRAT, 0, wxUP, 3);
    sizer11->AddSpacer(5);
    sizer11->Add(textRAT);
    sizer11->AddSpacer(5);
    sizer11->Add(labelSC, 0, wxUP, 3);
    sizer11->AddSpacer(5);
    sizer11->Add(textSC, 0);
    
    sizer12->Add(labelCID, 0, wxUP, 3);
    sizer12->AddSpacer(5);
    sizer12->Add(textCID, 0);
    sizer12->AddSpacer(35);
    sizer12->Add(labelMCC, 0, wxUP, 3);
    sizer12->AddSpacer(5);
    sizer12->Add(textMCC);
    sizer12->AddSpacer(5);
    sizer12->Add(labelMNC, 0, wxUP, 3);
    sizer12->AddSpacer(5);
    sizer12->Add(textMNC);
    
    sizer13->AddSpacer(20);
    sizer13->Add(checkRoaming, 0, wxUP, 3);
    sizer13->Add(checkRegistered, 0, wxUP, 3);
    sizer13->AddSpacer(35);
    sizer13->Add(labelLAC, 0, wxUP, 3);
    sizer13->AddSpacer(5);
    sizer13->Add(textLAC);
    sizer13->AddSpacer(5);
    sizer13->Add(labelLAI, 0, wxUP, 3);
    sizer13->AddSpacer(5);
    sizer13->Add(textLAI);

    sizer1->Add(sizer11);
    sizer1->Add(sizer12, 0, wxUP | wxDOWN, 5);
    sizer1->Add(sizer13);
    
    wxStaticBoxSizer *sizer2 = new wxStaticBoxSizer(wxVERTICAL, panel, "Signal");
    wxBoxSizer *sizer21 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer22 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer23 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer24 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer21->Add(labelRSSI);
    sizer21->Add(gaugeRssi, 3, wxLEFT | wxRIGHT, 10);
    sizer21->Add(textRSSI);
    sizer21->Add(textRSSIAverage, 0, wxUP, 2);
    
    sizer22->Add(labelRSCP);
    sizer22->Add(gaugeRscp, 3, wxLEFT | wxRIGHT, 10);
    sizer22->Add(textRSCP);
    sizer22->Add(textRSCPAverage, 0, wxUP, 2);
    
    sizer23->Add(labelECIO);
    sizer23->Add(gaugeEcio, 3, wxLEFT | wxRIGHT, 10);
    sizer23->Add(textECIO);
    sizer23->Add(textECIOAverage, 0, wxUP, 2);
    
    sizer24->Add(labelASU);
    sizer24->Add(gaugeAsu, 3, wxLEFT | wxRIGHT, 10);
    sizer24->Add(textASU);
    sizer24->Add(textASUAverage, 0, wxUP, 2);
    
    sizer2->Add(sizer21);
    sizer2->AddSpacer(5);
    sizer2->Add(sizer22);
    sizer2->AddSpacer(5);
    sizer2->Add(sizer23);
    sizer2->AddSpacer(5);
    sizer2->Add(sizer24);
    
    wxStaticBoxSizer *sizer3 = new wxStaticBoxSizer(wxVERTICAL, panel, "Connection");
    wxBoxSizer *sizer31 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer32 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer33 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer31->Add(labelConnectionStatus);
    sizer31->Add(labelConnectionStatusLabel);
    sizer31->AddSpacer(40);
    sizer31->Add(labelModeLabel);
    
	wxBoxSizer* sizer34 = new wxBoxSizer(wxVERTICAL);
	sizer34->Add(labelSpeedLabel1);
	sizer34->Add(labelSpeedLabel2);

    sizer32->Add(labelSpeed);
    sizer32->Add(sizer34);
    sizer32->AddSpacer(40);
    sizer32->Add(labelMonth);

    sizer33->Add(labelSession);
    sizer33->Add(labelSessionLabel);
    sizer33->AddSpacer(40);
    sizer33->Add(textIpAddress);
    
    sizer3->Add(sizer31);
    sizer3->Add(sizer32, 0, wxUP | wxDOWN, 5);
    sizer3->Add(sizer33);
    
    wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);
    mainSizer->AddSpacer(5);
    mainSizer->Add(sizer1);
    mainSizer->AddStretchSpacer();
    mainSizer->Add(sizer2, 0, wxUP | wxDOWN, 2);
    mainSizer->AddStretchSpacer();
    mainSizer->Add(sizer3);
    
    panel->SetSizerAndFit(mainSizer);
}

bool updateValues(const update_values &up)
{
    if(!status)
        return false;
    
    update_values null_array = { 0 };
    if(memcmp(&up, &null_array, sizeof(null_array)) == 0)
        return false;
    
    if(up.status == 0)
        return false;
    
    if(memcmp(&status->values, &up, sizeof(status->values)) != 0) {
        memcpy(&status->values, &up, sizeof(status->values));
        return true;
    }
    
    return false;
}

void Status::OnThread()
{
    myThreads->add();
    
    while(!status)
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    
    while(!myThreads->aborted()) {
        auto start = std::chrono::steady_clock::now();
        
        if(!status) {
            myThreads->remove();
            return;
        }
        
        int selection = reinterpret_cast<wxNotebook*>(wxWindow::FindWindowById(0x150))->GetSelection();
        if(selection == 0)
            networkUpdate();
		if (selection == 1)
			info->choice->SetFocus();
        
        if(!status) return;
        
        auto end = std::chrono::steady_clock::now();
        typedef std::chrono::duration<int,std::milli> millisecs_t ;
        millisecs_t duration( std::chrono::duration_cast<millisecs_t>(end-start) ) ;
        
        int buf = duration.count() - updateRate;
        if(buf > 0) buf = 0;
        else buf = -buf;
        
        std::this_thread::sleep_for(std::chrono::milliseconds(buf));
    }
    
    myThreads->remove();
}

void Status::networkUpdate()
{
    hilink->UpdateToken();
	hilink->api_device_information();
    hilink->api_device_signal(); //220ms | 120ms
    hilink->api_monitoring_status();//80ms | 30ms
    hilink->api_net_current_plmn(); //130ms | 60ms
    auto signal_para = hilink->api_net_signal_para(); //160ms | 100ms
    hilink->api_monitoring_traffic(); //80ms | 30ms
    hilink->api_net_netmode(); //110ms | 50ms
    hilink->api_monitoring_monthstatistics();
    auto start_date = hilink->api_monitoring_startdate();
    
    update_values tmp = { 0 };
    strcpy(tmp.op_full, hilink->GetCurrentPlmnFullName().c_str());
    strcpy(tmp.op_short, hilink->GetCurrentPlmnShortName().c_str());
    tmp.rat = hilink->GetCurrentPlmnRat();
    tmp.mcc = hilink->GetCurrentPlmnMcc();
    tmp.mnc = hilink->GetCurrentPlmnMnc();
    tmp.lac = hilink->GetLac();
    tmp.servicestatus = hilink->GetServiceStatus();
    tmp.registered = tmp.servicestatus == 2;
    tmp.roaming = hilink->GetRoamingStatus() == 1;
    tmp.status = hilink->GetConnectionStatus();
    tmp.mode = hilink->GetNetworkMode();
    tmp.type = hilink->GetCurrentNetworkTypeEx();
    tmp.download_speed = hilink->GetCurrentDownloadRate();
    tmp.upload_speed = hilink->GetCurrentUploadRate();
    tmp.download_traffic = hilink->GetCurrentDownload();
    tmp.upload_traffic = hilink->GetCurrentUpload();
    tmp.session_time = hilink->GetCurrentConnectTime();
    tmp.month_download = hilink->GetCurrentMonthDownload();
    tmp.month_upload = hilink->GetCurrentMonthUpload();
    tmp.month_traffic_limit = start_date.asUint64("trafficmaxlimit", 10);
    tmp.month_data = start_date.asInt("SetMonthData") == 1;
    strcpy(tmp.ip, hilink->GetWanIPAddress().c_str());
    
    
    switch(tmp.rat) {
        case Hilink::NETWORK_2G:
            tmp.rssi = signal_para.asInt("Rssi");
            tmp.rscp = signal_para.asInt("Rscp");
            tmp.ecio = signal_para.asInt("Ecio");
            tmp.cell_id = signal_para.asInt("CellID", 16);
            break;
        case Hilink::NETWORK_3G:
            tmp.rssi = hilink->GetRssi();
            tmp.rscp = hilink->GetRscp();
            tmp.ecio = hilink->GetEcIo();
            tmp.sc = hilink->GetSc();
            tmp.cell_id = hilink->GetCellId();
            break;
        case Hilink::NETWORK_4G:
            tmp.rssi = hilink->GetRssi();
            tmp.rsrp = hilink->GetRsrp();
            tmp.rsrq = hilink->GetRsrq();
            tmp.sinr = hilink->GetSinr();
            tmp.pci = hilink->GetPci();
            tmp.cell_id = hilink->GetCellId();
            break;
    }
    
    /*tmp.pci = 100;
    tmp.rsrp = tmp.rscp;
    tmp.rsrq =  tmp.ecio*2;
    tmp.sinr = -tmp.ecio;
    tmp.rat = 0;*/
    
    static bool test = false;
    
    wxString tmp2 = wxString(Hilink::GetConnectionStatusText(values.status, values.servicestatus));
    if(tmp2[0] == 'o' && tmp2[1] == 'n') //online
    {
        if(values.rat != tmp.rat || values.registered != tmp.registered || vRssi.size() > 172800) {
            vRssi.clear();
            vRscp.clear();
            vEcio.clear();
            vAsu.clear();
            wxPrintf("clear\n");
        }
        
        vRssi.push_back(tmp.rssi);
        switch(tmp.rat) {
            case Hilink::NETWORK_2G:
                vAsu.push_back(tmp.rssi + 116);
                break;
            case Hilink::NETWORK_3G:
                vRscp.push_back(tmp.rscp);
                vEcio.push_back(tmp.ecio);
                vAsu.push_back(tmp.rscp + 116);
                break;
            case Hilink::NETWORK_4G:
                vRscp.push_back(tmp.rsrp);
                vEcio.push_back(tmp.rsrq);
                vAsu.push_back(tmp.sinr);
                break;
        }
    }
    
    //g_mode = tmp.rat;
    
    updateValues(tmp);
}

void Status::resetAverage()
{
    vRssi.clear();
    vRscp.clear();
    vEcio.clear();
    vAsu.clear();
}

void Status::setMonthData()
{
    wxDialog *dialog = new wxDialog(panel, wxID_ANY, "Monthly data usage", wxDefaultPosition, wxSize(400, 300));
    dialog->SetFont(g_globalFont);
    
    auto checkboxMonthlytraffic = new wxCheckBox(dialog, wxID_ANY, "Monitor traffic and set limit", wxPoint(10, 10));
	bool cur = hilink->IsMonthlyMonitoringEnabled();
	checkboxMonthlytraffic->SetValue(cur);
    dialog->CenterOnScreen();
    dialog->ShowModal();
    
	if(cur != checkboxMonthlytraffic->GetValue())
		hilink->SetEnableMonthlyMonitoring(checkboxMonthlytraffic->GetValue());
	
	dialog->Destroy();
    
    
}