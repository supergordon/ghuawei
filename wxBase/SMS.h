//
//  SMS.h
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__SMS__
#define __wxBase__SMS__

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/spinctrl.h>

#include "Huawei.h"

extern wxString getItemColumnText(wxListCtrl *listCtrl, int item, int column);

class SMS {
public:
    SMS(wxWindow *win);
    ~SMS();
    wxPanel *getPanel();
    void createControls();
    void setupSizers();
    void setupControls();
    void updateControls();
    void updateSettings();
    void updateListSms();
	void refreshSms();
    void showContextMenu();
    
    void OnContextMenu(wxCommandEvent &event);
    void OnButtonSettingsApply(wxCommandEvent &event);
    void OnButtonSmsSend(wxCommandEvent &event);
    void OnListSmsItemActivated(wxListEvent &event);
    
    bool g_inbox, g_outbox, g_draft;
    
    wxListCtrl *listSms = nullptr;
    wxButton *buttonSmsRefresh = nullptr;
    wxCheckBox *checkInbox = nullptr, *checkOutbox = nullptr, *checkDraft = nullptr;
    wxButton *buttonSmsSend = nullptr;
    wxStaticText *labelSmsSc = nullptr, *labelValidity = nullptr;
    wxTextCtrl *textSmsSc = nullptr;
    wxSpinCtrl *spinValidity = nullptr;
    wxButton *buttonSmsSettingsApply = nullptr;
    wxCheckBox *checkSendReport = nullptr;
private:
    wxPanel *panel;
	std::list<Hilink::api_sms_list_phone_t> cur_sms_list;
};

#endif
