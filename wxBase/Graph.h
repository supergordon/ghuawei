//
//  Graph.h
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__Graph__
#define __wxBase__Graph__

#include <wx/wx.h>

class Graph {
public:
    Graph(wxWindow *win);
    wxPanel *getPanel();
private:
    wxPanel *panel;
};

#endif
