//
//  Huawei.cpp
//  wxBase
//
//  Created by Gordon Freeman on 03.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "Huawei.h"

Hilink *hilink = new Hilink();
std::map<int, CURL*> mapCurlThread;





/*
 TODO: http://192.168.8.1/config/global/net-type.xml parsen
 */


/*
 var G850G1900 = 0x280000;
 var GSM1900 = 0x200000;
 var GSM1800 = 0x80;
 var GSM900 = 0x300;
 var GSM850 = 0x80000;
 var W850W1900 = 0x4800000;
 var W2100 = 0x400000;
 var W1900 = 0x800000;
 var W850 = 0x4000000;
 var W900 = 0x2000000000000;
 
 GSM1800        0x80
 GSM900         0x300
 WCDMA2100      0x400000
 WCDMA900       0x2000000000000
 
 B1 0x1
 B3 0x4
 B7 0x40
 B8 0x80
 B20 0x80000
 All 0x800C5
 */



Hilink::Hilink()
{
    //wxPrintf("Hilink()\n");

    token = "";
	sessionid = "";
    SetIpAddress("192.168.8.1");
}

Hilink::~Hilink()
{
    //wxPrintf("~Hilink()\n");
}

void Hilink::Init()
{
   /* auto xmlGlobalConfig = hilink->getXml("config/global/config.xml");
    auto mapGlobalConfig = hilink->xmlToMap(xmlGlobalConfig);

    login_needed = static_cast<bool>(mapGlobalConfig.asLong("login"));*/
	login_needed = false;
  /*  auto xmlGlobalConfig = hilink->getXml("config/global/config.xml");
	wxString tmp = xmlGlobalConfig;
	wxMessageBox(tmp);
	if (tmp.Contains("<login>0</login>"))
		login_needed = false;
	else
		login_needed = true;*/
}

std::string Hilink::GetNetworkModeText(int mode)
{
    const char *g_pszNetworkMode[] = { "2G", "", "3G", "", "", "", "", "4G" };
    
    return std::string(g_pszNetworkMode[mode]);
}

std::string Hilink::GetModeText(int networkMode)
{
    switch(networkMode) {
        case Hilink::NETMODE_AUTO: return "auto";
        case Hilink::NETMODE_2G_ONLY: return "2G only";
        case Hilink::NETMODE_3G_ONLY: return "3G only";
        case Hilink::NETMODE_4G_ONLY: return "4G only";
        case Hilink::NETMODE_WCDMA_GSM: return "WCDMA -> GSM";
        case Hilink::NETMODE_LTE_WCDMA: return "LTE -> WCDMA";
        default: return "Unknown";
    }
    
    return "";
}

std::string Hilink::GetPlmnStateText(int state)
{
    const char *g_pszPlmnState[] = { "", "available", "registered", "forbidden" };
    
    return std::string(g_pszPlmnState[state]);
}

std::string Hilink::GetNetworkTypeText(int type)
{
    const char *g_pszNetworkType[] = { "No Service", "GSM", "GPRS", "EDGE", "WCDMA",
        "HSDPA", "HSUPA", "HSPA", "TDSCDMA", "HSPA+", "EVDO Rev0", "EVDO RevA",
        "EVDO RevB", "lxRTT", "UMB", "1xEVDV", "3xRTT", "HSPA+64QAM", "HSPA+MIMO",
        "LTE", "", "IS95A", "IS95B", "CDMA1x", "EVDO Rev0", "EVDO RevA", "EVDO RevB",
        "HYBRID_CDMA1x", "HYBRID_EVDO_REV0", "HYBRID_EVDO_REVA", "HYBRID_EVDO_REVB",
        "EHRPD_Rel0", "EHRPD_RelA", "EHRPD_RelB", "HYBRID_EHRPD_Rel0",
        "HYBRID_EHRPD_RelA", "HYBRID_EHRPD_RelB", "", "", "", "", "WCDMA", "HSDPA",
        "HSUPA", "HSPA", "HSPA+", "DC-HSPA+", "", "", "", "", "", "", "", "", "", "",
        "", "", "", "", "TD_SCDMA", "TD_HSDPA", "TD_HSUPA", "TD_HSPA", "TD_HSPA+", "",
        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "802.16E", "", "", "",
        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "LTE"};
    
    return std::string(g_pszNetworkType[type]);
}

void Hilink::SetIpAddress(std::string ip)
{
    ipAddress = ip;
}

const std::string &Hilink::GetIpAddress()
{
    return ipAddress;
}

void Hilink::test()
{
    //SetMonthlyMonitoring(23, "500GB", 90);
    //SetDisableMonthlyMonitoring();

   // SmsSendSms("31321", "GUT");
   /* auto sms = GetAllSms();
    for(auto it : sms)
        printf("-%i: %i | %s | %s | %s-\n", int(it.Index), int(it.Smstat), it.Date, it.Phone, it.Content);
    
    printf("%d SMS\n", int(sms.size()));
    */
    
    //SmsSetSendReport(false);
    //SmsSetSmsSc("+4366000660");
    
    //api/ussd/send
    //<request><content>*333#</content><codeType>CodeType</codeType></request>
    
    //wxPrintf(postXml("api/ussd/send", "<request><content>*06#</content><codeType>CodeType</codeType></request>"));
    //sleep(2);
   // wxPrintf("\n" + getXml("api/ussd/get")); //release
    //wxPrintf("\n" + getXml("api/ussd/status"));
    //wxPrintf("\n" + getXml("api/ussd/release"));
    //MobileToggleConnection();
    
    /*struct tm timeDate;
     strptime(s,"%y-%m-%d %H:%M", &timeDate);*/
    //wxPrintf("%d\n", (int)sizeof(time_t));
    
    /* int band = 20;
    
    for(int i = 1; i <= 20; i++) {
    unsigned long value = 1 << (i - 1);
    printf("%i: %X\n", i, unsigned(value));
    }*/
    
    /*
     <request>
     <Servers>
     <Server>
     <VirtualServerIPName>SSH</VirtualServerIPName>
     <VirtualServerStatus>0</VirtualServerStatus>
     <VirtualServerRemoteIP></VirtualServerRemoteIP>
     <VirtualServerWanPort>22</VirtualServerWanPort>
     <VirtualServerWanEndPort>22</VirtualServerWanEndPort>
     <VirtualServerLanPort>22</VirtualServerLanPort>
     <VirtualServerIPAddress>192.168.8.100</VirtualServerIPAddress>
     <VirtualServerProtocol>0</VirtualServerProtocol>
     </Server>
     </Servers>
     </request>
     
     <request><Servers><Server><VirtualServerIPName>SSH</VirtualServerIPName><VirtualServerStatus>0</VirtualServerStatus><VirtualServerRemoteIP></VirtualServerRemoteIP><VirtualServerWanPort>22</VirtualServerWanPort><VirtualServerWanEndPort>22</VirtualServerWanEndPort><VirtualServerLanPort>22</VirtualServerLanPort><VirtualServerIPAddress>192.168.8.100</VirtualServerIPAddress><VirtualServerProtocol>0</VirtualServerProtocol></Server></Servers></request>
     */
    
    return;
    
    wxString request = "<request><Servers><length>1</length><Server><VirtualServerIPName>SSH</VirtualServerIPName><VirtualServerStatus>0</VirtualServerStatus><VirtualServerRemoteIP>77.118.182.90</VirtualServerRemoteIP><VirtualServerWanPort>22</VirtualServerWanPort><VirtualServerWanEndPort></VirtualServerWanEndPort><VirtualServerLanPort>22</VirtualServerLanPort><VirtualServerIPAddress>192.168.8.100</VirtualServerIPAddress><VirtualServerProtocol>0</VirtualServerProtocol></Server></Servers></request>";
    
    wxString request2 = "<request><LanPorts><length>1</length><LanPort><SpecialApplicationTriggerName>SSH</SpecialApplicationTriggerName><SpecialApplicationTriggerStatus>0</SpecialApplicationTriggerStatus><SpecialApplicationTriggerPort>22</SpecialApplicationTriggerPort><SpecialApplicationTriggerProtocol>0</SpecialApplicationTriggerProtocol><SpecialApplicationOpenProtocol>0</SpecialApplicationOpenProtocol><SpecialApplicationStartOpenPort0>22</SpecialApplicationStartOpenPort0><SpecialApplicationEndOpenPort0>22</SpecialApplicationEndOpenPort0><SpecialApplicationStartOpenPort1></SpecialApplicationStartOpenPort1><SpecialApplicationEndOpenPort1></SpecialApplicationEndOpenPort1><SpecialApplicationStartOpenPort2></SpecialApplicationStartOpenPort2><SpecialApplicationEndOpenPort2></SpecialApplicationEndOpenPort2><SpecialApplicationStartOpenPort3></SpecialApplicationStartOpenPort3><SpecialApplicationEndOpenPort3></SpecialApplicationEndOpenPort3><SpecialApplicationStartOpenPort4></SpecialApplicationStartOpenPort4><SpecialApplicationEndOpenPort4></SpecialApplicationEndOpenPort4></LanPort></LanPorts></request>";
    
    /*
     
     <request><LanPorts><LanPort><SpecialApplicationTriggerName>SSH</SpecialApplicationTriggerName><SpecialApplicationTriggerStatus>0</SpecialApplicationTriggerStatus><SpecialApplicationTriggerPort>22</SpecialApplicationTriggerPort><SpecialApplicationTriggerProtocol>0</SpecialApplicationTriggerProtocol><SpecialApplicationOpenProtocol>0</SpecialApplicationOpenProtocol><SpecialApplicationStartOpenPort0>22</SpecialApplicationStartOpenPort0><SpecialApplicationEndOpenPort0>22</SpecialApplicationEndOpenPort0><SpecialApplicationStartOpenPort1></SpecialApplicationStartOpenPort1><SpecialApplicationEndOpenPort1></SpecialApplicationEndOpenPort1><SpecialApplicationStartOpenPort2></SpecialApplicationStartOpenPort2><SpecialApplicationEndOpenPort2></SpecialApplicationEndOpenPort2><SpecialApplicationStartOpenPort3></SpecialApplicationStartOpenPort3><SpecialApplicationEndOpenPort3></SpecialApplicationEndOpenPort3><SpecialApplicationStartOpenPort4></SpecialApplicationStartOpenPort4><SpecialApplicationEndOpenPort4></SpecialApplicationEndOpenPort4></LanPort></LanPorts></request>
     */
    
   // wxPrintf(postXml("api/security/virtual-servers", request));
   /// wxPrintf(postXml("api/security/special-applications", request2));
   // wxPrintf(getXml("api/security/virtual-servers"));
    //Name	WAN-Port	LAN-IP-Adresse	LAN-Port	Protokoll	Status	Optionen
}

size_t write_callback2(char *data, size_t size, size_t nmemb, std::string *userdata)
{
    if(userdata && size && nmemb)
        userdata->append(data, size * nmemb);
    
    return size * nmemb;
}

CURL *existsCurlThreadHandle()
{
    for(auto it: mapCurlThread)
        if(it.first == myThreads->id() && it.second != nullptr)
            return it.second;
    
    return nullptr;
}

CURL *getCurlThreadHandle()
{
    CURL *handle = existsCurlThreadHandle();
    if(handle)
        return handle;
    
    handle = curl_easy_init();
    if(!handle)
        wxPrintf("getCurlThreadHandle: curl handle is NULL");
    
    mapCurlThread.insert( { myThreads->id(), handle } );
    
    //wxPrintf("NEW curl handle for thread %i\n", myThreads->id());
    
    return handle;
}

void cleanupCurlThreadHandle()
{
    for(auto &it: mapCurlThread) {
        if(it.first == myThreads->id()) {
            if(it.second) {
                //wxPrintf("CLEANUP curl handle for thread %i\n", it.first);
                curl_easy_cleanup(it.second);
                it.second = nullptr;
                break;
            }
        }
    }
}

class CurlThreadHandle {
public:
    CURL *exists();
    CURL *handle();
    std::map<int, CURL*> &get();
private:
    std::map<int, CURL*> mapHandle;
};

CURL *CurlThreadHandle::exists()
{
    return nullptr;
}

CURL *CurlThreadHandle::handle()
{
    return nullptr;
}

std::map<int, CURL*> &CurlThreadHandle::get()
{
    return mapHandle;
}

bool Hilink::IsDeviceOnline()
{
    std::string ret = getXml("api/webserver/SesTokInfo");
    if(ret.length() && wxString(ret).Contains("<response>"))
        return true;

    return false;
}

std::string Hilink::UpdateToken()
{
    token = api_webserver_token(sessionid);
    return token;
}

std::string Hilink::GetToken()
{
    return token;
}

std::string Hilink::GetSessionId()
{
	return sessionid;
}


long Hilink::SetMonthlyMonitoring(int StartDay, const char *DataLimit, int MonthThreshold, int SetMonthData)
{
    map_str_str mapRequest;
    mapRequest.insert("StartDay", StartDay);
    mapRequest.insert("DataLimit", std::string(DataLimit));
    mapRequest.insert("MonthThreshold", MonthThreshold);
    mapRequest.insert("SetMonthData", SetMonthData);
    
    wxString response = postXml("api/monitoring/start_date", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetMonthlyMonitoring: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::IsMonthlyMonitoringEnabled() 
{
	auto res = api_monitoring_startdate();
	if (res.size() == 0)
		return false;

	return res.asInt("SetMonthData") == 1;
}

long Hilink::SetEnableMonthlyMonitoring(bool toggle)
{
    auto res = api_monitoring_startdate();
    if(res.size() == 0)
        return 1;
    
    SetMonthlyMonitoring(res.asInt("StartDay"), res["DataLimit"].c_str(), res.asInt("MonthThreshold"), int(toggle));
    
    return 0;
}

long Hilink::SetMonitoringClearTraffic()
{
    MyMap mapRequest("ClearTraffic", 1);
    std::string response = postXml("api/monitoring/clear-traffic", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetMonitoringClearTraffic: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SetAutoApn(bool toggle)
{
    wxString request = wxString::Format("<request><AutoAPN>%i</AutoAPN></request>", int(toggle));
    wxString response = postXml("api/dialup/auto-apn", std::string(request.char_str()));
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetAutoApn: error %d\n", int(error));
        return error;
    }
    
    return 0;
}

long Hilink::SetDataRoaming(bool toggle)
{
    auto res = api_dialup_connection();
    
    if(res.asInt("MaxIdelTime") == 0 || res.asInt("MTU") == 0) {
        wxPrintf("SetDataRoaming: invalid values\n");
        return 2;
    }
    
    if(bool(res.asInt("RoamAutoConnectEnable")) == toggle) {
        wxPrintf("SetDataRoaming: already set\n");
        return 0;
    }
    
    map_str_str mapRequest;
    mapRequest.insert("RoamAutoConnectEnable", int(toggle));
    mapRequest.insert("MaxIdelTime", res["MaxIdelTime"]);
    mapRequest.insert("ConnectMode", res["ConnectMode"]);
    mapRequest.insert("MTU", res["MTU"]);
    mapRequest.insert("auto_dial_switch", res["auto_dial_switch"]);
    
    std::string response = postXml("api/dialup/connection", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetDataRoaming: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::IsDataRoaming()
{
    auto res = api_dialup_connection();
    return res.asInt("RoamAutoConnectEnable") == 1;
}

long Hilink::MobileConnect(bool toggle)
{
    MyMap mapRequest("dataswitch", int(toggle));
    std::string response = postXml("api/dialup/mobile-dataswitch", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("MobileConnect: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::MobileReconnect()
{
    long ret1 = MobileConnect(false);
    long ret2 = MobileConnect(true);
    
    return ret1 == 0 && ret2 == 0;
}

bool Hilink::MobileToggleConnection()
{
    return MobileConnect(!IsMobileConnected());
}

bool Hilink::MobileRenewIp(int hops)
{
	bool success = false;

	for (int i = 0; i < hops; i++) {
		auto res = api_net_netmode();
		int netmode = res.asInt("NetworkMode");
		long ret = this->SetNetworkMode(netmode == 3 ? 302 : 3);
		if (ret == 0)
			success = true;
	}

	return success;
}

bool Hilink::IsMobileConnected()
{
    auto res = api_dialup_mobiledataswitch();
    if(res.size() == 0)
        return false;
    
    return res.asInt("dataswitch") == 1;
}

long Hilink::DeviceReboot()
{
    map_str_str mapRequest("Control", 1);
    std::string response = postXml("api/device/control", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("DeviceReboot: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::DeviceRestoreDefaults()
{
    map_str_str mapRequest("Control", 2);
    std::string response = postXml("api/device/control", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("DeviceRestoreDefaults: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::DeviceBackupSettings(wxString path)
{
    map_str_str mapRequest("Control", 3);
    std::string response = postXml("api/device/control", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("DeviceRestoreDefaults: %s\n", Errortext(error));
        return error;
    }
    
    wxString nvram = getXml("nvram.bak");
    
    FILE *file = fopen(path.c_str(), "w+");
    fwrite(nvram.c_str(), nvram.length(), 1, file);
    fclose(file);

    //save to file, optionally base64 decode it
    /*wxArrayString array = wxSplit(nvram, '\n');
    for(auto it : array) {
        wxMemoryBuffer buffer = wxBase64Decode(array);
        wxPrintf("%s\n", it);
    }*/
    
    return 0;
}

long Hilink::DeviceRestoreSettings(wxString path)
{
    wxMessageBox("not implemented yet");
    //configuration.html
    
    return 0;
}

long Hilink::DeviceSwitchToProjectMode()
{
    map_str_str mapRequest("mode", 0);
    std::string response = postXml("api/device/mode", mapRequest.toXmlRequest());
    
    return 0;
}

/*
 Normal Mode:
 Bus 001 Device 014: ID 12d1:14dc Huawei Technologies Co., Ltd.
 
 Project Mode:
 Bus 001 Device 015: ID 12d1:1442 Huawei Technologies Co., Ltd.
 
 Debug Mode:
 Bus 001 Device 018: ID 12d1:1566 Huawei Technologies Co., Ltd.
 */

long Hilink::DeviceSwitchToDebugMode()
{
    map_str_str mapRequest("mode", 0);
    std::string response = postXml("api/device/mode", mapRequest.toXmlRequest());
    
    return 0;
}

long Hilink::EnableUpnp(bool toggle)
{
    if(IsUpnpEnabled() == toggle) {
        wxPrintf("EnableUpnp: already set\n");
        return 0;
    }
    
    map_str_str mapRequest("UpnpStatus", int(toggle));
    
    std::string response = postXml("api/security/upnp", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("EnableUpnp: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::IsUpnpEnabled()
{
    auto res = api_security_upnp();
    return res["UpnpStatus"] == "1";
}

long Hilink::EnableDmz(bool toggle, std::string ip)
{
    auto res = api_security_dmz();
    if(res.size() == 0)
        return false;
    
    if(ip.length() == 0) {
        ip = res["DmzIPAddress"];
        if(ip.length() == 0)
            ip = "192.168.8.100";
    }
    
    map_str_str mapRequest;
    mapRequest.insert("DmzStatus", int(toggle));
    mapRequest.insert("DmzIPAddress", ip);
    
    std::string response = postXml("api/security/dmz", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("EnableDmz: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::IsDmzEnabled()
{
    auto res = api_security_dmz();
    return res["DmzStatus"] == "1";
}

long Hilink::EnableSipAlg(bool toggle)
{
    auto res = api_security_sip();
    return EnableSipAlg(toggle, res.asInt("SipPort"));
}

long Hilink::EnableSipAlg(bool toggle, int port)
{
    map_str_str mapRequest;
    mapRequest.insert("SipStatus", int(toggle));
    mapRequest.insert("SipPort", port);
    
    std::string response = postXml("api/security/sip", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("EnableSipAlg: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::IsSipAlgEnabled()
{
    auto res = api_security_sip();
    return res["SipStatus"] == "1";
}

long Hilink::EnableFirewall(bool toggle)
{
    auto res = api_security_firewallswitch();
    if(res.size() == 0)
        return 1;
    
    map_str_str mapRequest;
    mapRequest.insert("FirewallMainSwitch", int(toggle));
    mapRequest.insert("FirewallIPFilterSwitch", res["FirewallIPFilterSwitch"]);
    mapRequest.insert("FirewallWanPortPingSwitch", res["FirewallWanPortPingSwitch"]);
    
    std::string response = postXml("api/security/firewall-switch", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("EnableFirewall: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

bool Hilink::IsFirewallEnabled()
{
    auto res = api_security_firewallswitch();
    return res["FirewallMainSwitch"] == "1";
}

long Hilink::SetNetworkMode(int mode)
{
    auto res = api_net_netmode();
    
    map_str_str mapRequest;
    mapRequest.insert("NetworkMode", "0" + intToStr(mode));
    mapRequest.insert("NetworkBand", res["NetworkBand"]);
    mapRequest.insert("LTEBand", res["LTEBand"]);
    
    std::string response = postXml("api/net/net-mode", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetNetworkMode: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SetNetworkBand(uint64_t band)
{
    auto res = api_net_netmode();
    
    map_str_str mapRequest;
    mapRequest.insert("NetworkMode", res["NetworkMode"]);
    mapRequest.insert("NetworkBand", band);
    mapRequest.insert("LTEBand", res["LTEBand"]);
    
    std::string response = postXml("api/net/net-mode", mapRequest.toXmlRequest());
        
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetNetworkBand: %s\n", Errortext(error));
        return error;
    }
        
    return 0;
}

long Hilink::SetLteBand(uint64_t band)
{
    auto res = api_net_netmode();
    
    map_str_str mapRequest;
    mapRequest.insert("NetworkMode", res["NetworkMode"]);
    mapRequest.insert("NetworkBand", res["NetworkBand"]);
    mapRequest.insert("LTEBand", band);
    
    std::string response = postXml("api/net/net-mode", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetLteBand: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SetNetworkModeBand(int mode, uint64_t band, uint64_t lteband)
{
    map_str_str mapRequest;
    mapRequest.insert("NetworkMode", "0" + intToStr(mode));
    mapRequest.insert("NetworkBand", band);
    mapRequest.insert("LTEBand", lteband);

    std::string response = postXml("api/net/net-mode", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SetNetworkModeBand: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SmsSetSendReport(bool toggle)
{
    auto res = api_sms_config();
    if(res.size() == 0) {
        wxPrintf("SmsSetSendReport: Error api_sms_config()\n");
        return 1;
    }
    
    map_str_str mapRequest;
    mapRequest.insert("SaveMode", res["SaveMode"]);
    mapRequest.insert("Validity", res["Validity"]);
    mapRequest.insert("Sca", res["Sca"]);
    mapRequest.insert("UseSReport", int(toggle));
    mapRequest.insert("SendType", res["SendType"]);
    mapRequest.insert("Priority", res["Priority"]);
    
    std::string response = postXml("api/sms/config", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SmsSetSendReport: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SmsSetSmsSc(const char *smssc)
{
    if(strlen(smssc) == 0)
        return 1;
    
    auto res = api_sms_config();
    if(res.size() == 0) {
        wxPrintf("SmsSetSmsSc: Error api_sms_config()\n");
        return 2;
    }
    
    if(!strcmp(smssc, res["Sca"].c_str()))
        return 0;
    
    map_str_str mapRequest;
    mapRequest.insert("SaveMode", res["SaveMode"]);
    mapRequest.insert("Validity", res["Validity"]);
    mapRequest.insert("Sca", std::string(smssc));
    mapRequest.insert("UseSReport", res["UseSReport"]);
    mapRequest.insert("SendType", res["SendType"]);
    mapRequest.insert("Priority", res["Priority"]);
    
    std::string response = postXml("api/sms/config", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SmsSetSmsSc: %s\n", Errortext(error));
        return error;
    }

    return 0;
}

long Hilink::SmsSetSmsValidity(int validity)
{
    if(validity == 0)
        return 1;
    
    auto res = api_sms_config();
    if(res.size() == 0) {
        wxPrintf("SmsSetSmsValidity: Error api_sms_config()\n");
        return 2;
    }

    map_str_str mapRequest;
    mapRequest.insert("SaveMode", res["SaveMode"]);
    mapRequest.insert("Validity", validity);
    mapRequest.insert("Sca", res["Sca"]);
    mapRequest.insert("UseSReport", res["UseSReport"]);
    mapRequest.insert("SendType", res["SendType"]);
    mapRequest.insert("Priority", res["Priority"]);
    
    std::string response = postXml("api/sms/config", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SmsSetSmsValidity: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SmsSendSms(const char *number, const char *content)
{
    if(!strlen(number) || !strlen(content))
        return 1;
    
    wxPrintf("Sending SMS to %s with the content [%s]\n", number, content);
    //return 0;
    
    time_t now = time(NULL);
    char time[32] = { 0 };
    strftime(time, 32, "%Y-%m-%d %H:%M:%S", localtime(&now));
    
    map_str_str map1("Index", -1);
    map_str_str map2("Phone", std::string(number));
    
    map_str_str map3;
    map3.insert("Sca", "");
    map3.insert("Content", std::string(content));
    map3.insert("Length", int(strlen(content)));
    map3.insert("Reserved", 1);
    map3.insert("Date", std::string(time));
    
    std::string request = std::string(wxString::Format("<request>%s<Phones>%s</Phones>%s</request>", map1.toXml(), map2.toXml(), map3.toXml()).c_str());
    std::string response = postXml("api/sms/send-sms", request);
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SmsSendSms: error %d\n", int(error));
        return error;
    }
    
    auto res = api_sms_sendstatus();
    if(res.size() == 0) {
        wxPrintf("SmsSendSms: error api_sms_sendstatus()\n");
        return 6;
    }
    /*
     struct api_sms_smsstatus_t {
        char Phone[32];
        char SucPhone[32];
        char FailPhone[32];
        long TotalCount;
        long CurIndex;
     };
     */
    if(res["Phone"] != std::string(number)) {
        wxPrintf("SmsSendSms: error sendstatus - number mismatch %d\n", int(Errorcode(response)));
        return 4;
    }
    
    if(res.asInt("TotalCount") != 1) {
        wxPrintf("SmsSendSms: error sendstatus - sms sentcount mismatch %d\n", int(Errorcode(response)));
        return 5;
    }
    
    return 0;
}


long Hilink::SmsDeleteSms(int index)
{
    map_str_str mapRequest("Index", index);
    std::string response = postXml("api/sms/delete-sms", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SmsDeleteSms: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::SmsSetReadSms(int index)
{
    map_str_str mapRequest("Index", index);
    std::string response = postXml("api/sms/set-read", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("SmsSetReadSms: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

/*
 http://192.168.8.1/api/net/register
 manual: <request><Mode>1</Mode><Plmn>23210</Plmn><Rat>2</Rat></request>
 auto: <request><Mode>0</Mode><Plmn></Plmn><Rat></Rat></request>
 */

long Hilink::RegisterNetwork(bool automatic, std::string numeric, int rat)
{
    map_str_str mapRequest;
    
    if(automatic) {
        mapRequest.insert("Mode", 0);
        mapRequest.insert("Plmn", "");
        mapRequest.insert("Rat", "");
    }
    else {
        mapRequest.insert("Mode", 1);
        mapRequest.insert("Plmn", numeric);
        mapRequest.insert("Rat", rat);
    }
    
    std::string response = postXml("api/net/register", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("RegisterNetwork: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

std::list<Hilink::api_sms_list_contact_t> Hilink::GetSmsContacts()
{
    std::list<Hilink::api_sms_list_contact_t> ret;
    int PageIndex = 1, ReadCount = 50;
    wxString response;
    
    do {
        wxString request = wxString::Format("<request><PageIndex>%i</PageIndex><ReadCount>%i</ReadCount></request>", PageIndex, ReadCount);
        response = postXml("api/sms/sms-list-contact", std::string(request.char_str()));
    
        int error = Errorcode(response);
        if(error != 0) {
            wxPrintf("GetSmsContacts: error %d\n", int(error));
            return ret;
        }
        
        auto buf = this->api_sms_listcontact(response.char_str());
        for(auto it : buf)
            ret.push_back(it);
        
        PageIndex++;
        
    } while(response.Contains("<Count>50</Count>"));
    
    ret.sort([] (const api_sms_list_contact_t first, const api_sms_list_contact_t second) {
        return first.time > second.time;
    });
    
    return ret;
}

std::list<Hilink::api_sms_list_phone_t> Hilink::GetAllSmsFromNumber(const std::string &number)
{
    std::list<Hilink::api_sms_list_phone_t> ret;
    int PageIndex = 1, ReadCount = 50;
    wxString response;
    
    do {
        wxString request = wxString::Format("<request><Phone>%s</Phone><PageIndex>%i</PageIndex><ReadCount>%i</ReadCount></request>", number, PageIndex, ReadCount);
        response = postXml("api/sms/sms-list-phone", std::string(request.char_str()));
        
        int error = Errorcode(response);
        if(error != 0) {
            wxPrintf("GetAllSmsFromNumber: error %d\n", int(error));
            return ret;
        }
    
        auto buf = this->api_sms_listphone(response.char_str());
        for(auto it : buf)
            ret.push_back(it);
        
        PageIndex++;
        
    } while(response.Contains("<Count>50</Count>"));

    ret.sort([] (const api_sms_list_phone_t first, const api_sms_list_phone_t second) {
        return first.time > second.time;
    });
    
    
    return ret;
}

std::list<Hilink::api_sms_list_phone_t> Hilink::GetAllSms()
{
    std::list<api_sms_list_phone_t> ret;
    
    //devices updates the sms list only with this call
    api_sms_smscount();
    
    for(auto &it : GetSmsContacts()) {
        for(auto &it2 : GetAllSmsFromNumber(it.Phone))
            ret.push_back(it2);
    }
    
    ret.sort([] (const api_sms_list_phone_t first, const api_sms_list_phone_t second) {
        return first.time > second.time;
    });
    
    return ret;
}

std::list<Hilink::api_net_plmnlist_t> Hilink::GetPlmnList()
{
    UpdateToken();
    std::list<api_net_plmnlist_t> ret;
    std::string response = getXml("api/net/plmn-list");
   // std::string response = "<response><Networks><Network><Index>0</Index><State>1</State><FullName>3 AT</FullName><ShortName>3 AT</ShortName><Numeric>23210</Numeric><Rat>7</Rat></Network><Network><Index>1</Index><State>1</State><FullName>3 AT</FullName><ShortName>3 AT</ShortName><Numeric>23210</Numeric><Rat>2</Rat></Network><Network><Index>2</Index><State>2</State><FullName>3 AT</FullName><ShortName>3 AT</ShortName><Numeric>23205</Numeric><Rat>2</Rat></Network><Network><Index>3</Index><State>1</State><FullName>T-Mobile A</FullName><ShortName>TMA</ShortName><Numeric>23203</Numeric><Rat>0</Rat></Network><Network><Index>4</Index><State>1</State><FullName>T-Mobile A</FullName><ShortName>TMA</ShortName><Numeric>23203</Numeric><Rat>7</Rat></Network><Network><Index>5</Index><State>1</State><FullName>A1</FullName><ShortName>A1</ShortName><Numeric>23201</Numeric><Rat>7</Rat></Network><Network><Index>6</Index><State>1</State><FullName>3 AT</FullName><ShortName>3 AT</ShortName><Numeric>23205</Numeric><Rat>7</Rat></Network><Network><Index>7</Index><State>1</State><FullName>T-Mobile A</FullName><ShortName>TMA</ShortName><Numeric>23203</Numeric><Rat>2</Rat></Network><Network><Index>8</Index><State>1</State><FullName>A1</FullName><ShortName>A1</ShortName><Numeric>23201</Numeric><Rat>2</Rat></Network><Network><Index>9</Index><State>1</State><FullName>3 AT</FullName><ShortName>3 AT</ShortName><Numeric>23205</Numeric><Rat>0</Rat></Network><Network><Index>10</Index><State>1</State><FullName>A1</FullName><ShortName>A1</ShortName><Numeric>23201</Numeric><Rat>0</Rat></Network></Networks></response>";

    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("GetPlmnList: %s\n", Errortext(error));
        return ret;
    }
    
    wxStringInputStream stream(response);
    wxXmlDocument doc(stream);
    
    if(!doc.IsOk())
        return ret;
    
    wxXmlNode *node = doc.GetRoot()->GetChildren()->GetChildren();
    
    while(node) {
        if(node->GetName() == "Network") {
            wxXmlNode *nodeNetwork = node->GetChildren();
            api_net_plmnlist_t tmp = { 0 };
            while(nodeNetwork) {  
                std::string name = std::string(nodeNetwork->GetName().c_str());
                std::string content = std::string(nodeNetwork->GetNodeContent().c_str());
                
                if(name == "Index") tmp.Index = strToInt(content);
                else if(name == "State") tmp.State = strToInt(content);
                else if(name == "FullName") tmp.FullName = content;
                else if(name == "ShortName") tmp.ShortName = content;
                else if(name == "Numeric") tmp.Numeric = content;
                else if(name == "Rat") tmp.Rat = strToInt(content);
                
                nodeNetwork = nodeNetwork->GetNext();
            }
            ret.push_back(tmp);
        }
        
        node = node->GetNext();
    }
    
    return ret;
}

Hilink::api_dialup_profiles_t Hilink::GetProfiles()
{
    UpdateToken();
    api_dialup_profiles_t ret;
    std::string response = getXml("api/dialup/profiles");

    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("GetProfiles: %s\n", Errortext(error));
        return ret;
    }
    
    wxStringInputStream stream(response);
    wxXmlDocument doc(stream);
    
    if(!doc.IsOk())
        return ret;
    
    wxXmlNode *node = doc.GetRoot()->GetChildren();
    node->GetNodeContent().ToLong((long*)&ret.CurrentProfile);
    
    node = node->GetNext()->GetChildren();
    while(node) {
        if(node->GetName() == "Profile") {
            wxXmlNode *nodeProfiles = node->GetChildren();
            api_dialup_profile_t tmp;
            
            while(nodeProfiles) {
                std::string name = std::string(nodeProfiles->GetName().c_str());
                std::string content = std::string(nodeProfiles->GetNodeContent().c_str());

                if(name == "Index") tmp.Index = strToInt(content);
                else if(name == "IsValid") tmp.IsValid = strToInt(content);
                else if(name == "Name") tmp.Name = content;
                else if(name == "ApnIsStatic") tmp.ApnIsStatic = strToInt(content);
                else if(name == "ApnName") tmp.ApnName = content;
                else if(name == "DialupNum") tmp.DialupNum = content;
                else if(name == "Username") tmp.Username = content;
                else if(name == "Password") tmp.Password = content;
                else if(name == "AuthMode") tmp.AuthMode = strToInt(content);
                else if(name == "IpIsStatic") tmp.IpIsStatic = strToInt(content);
                else if(name == "IpAddress") tmp.IpAddress = content;
                else if(name == "Ipv6Address") tmp.Ipv6Address = content;
                else if(name == "DnsIsStatic") tmp.DnsIsStatic = strToInt(content);
                else if(name == "PrimaryDns") tmp.PrimaryDns = content;
                else if(name == "SecondaryDns") tmp.SecondaryDns = content;
                else if(name == "PrimaryIpv6Dns") tmp.PrimaryIpv6Dns = content;
                else if(name == "SecondaryIpv6Dns") tmp.SecondaryIpv6Dns = content;
                else if(name == "ReadOnly") tmp.ReadOnly = strToInt(content);
                else if(name == "iptype") tmp.iptype = strToInt(content);
            
                nodeProfiles = nodeProfiles->GetNext();
            }
            ret.profiles.push_back(tmp);
        }
        node = node->GetNext();
    }
    
    return ret;
}

Hilink::api_dialup_profile_t Hilink::GetProfileByIndex(int index)
{
    api_dialup_profile_t ret = { 0 };
    
    auto profiles = GetProfiles();
    for(auto profile : profiles.profiles) {
        if(profile.Index == index) {
            ret = profile;
            break;
        }
    }
    
    return ret;
}

long Hilink::ProfileDelete(int index)
{
    if(index == 1) {
        wxPrintf("we dont want to delete our first profile\n");
        return 1;
    }
    
    map_str_str mapRequest;
    mapRequest.insert("Delete", index);
    mapRequest.insert("SetDefault", 1);
    mapRequest.insert("Modify", 0);
    
    std::string response = postXml("api/dialup/profiles", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("ProfileDelete: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::ProfileSetDefault(int index)
{
    if(index == 0)
        index = 1;
    
    map_str_str mapRequest;
    mapRequest.insert("Delete", 0);
    mapRequest.insert("SetDefault", index);
    mapRequest.insert("Modify", 0);
    
    std::string response = postXml("api/dialup/profiles", mapRequest.toXmlRequest());
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("ProfileSetDefault: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::ProfileAdd(std::string name, std::string apn, std::string user, std::string password, int IsValid, int ApnIsStatic, std::string DialupNum, int AuthMode, int IpIsStatic, std::string IpAddress, int DnsIsStatic, std::string PrimaryDns, std::string SecondaryDns, int ReadOnly, int iptype)
{
    map_str_str map1;
    map1.insert("Delete", 0);
    map1.insert("SetDefault", 0);
    map1.insert("Modify", 1);
    
    map_str_str map2;
    map2.insert("Index", "");
    map2.insert("IsValid", IsValid);
    map2.insert("Name", name);
    map2.insert("ApnIsStatic", ApnIsStatic);
    map2.insert("ApnName", apn);
    map2.insert("DialupNum", DialupNum);
    map2.insert("Username", user);
    map2.insert("Password", password);
    map2.insert("AuthMode", AuthMode);
    map2.insert("IpIsStatic", IpIsStatic);
    map2.insert("IpAddress", IpAddress);
    map2.insert("DnsIsStatic", DnsIsStatic);
    map2.insert("PrimaryDns", PrimaryDns);
    map2.insert("SecondaryDns", SecondaryDns);
    map2.insert("ReadOnly", ReadOnly);
    map2.insert("iptype", iptype);
    
    std::string request = std::string(wxString::Format("<request>%s<Profile>%s</Profile></request>", map1.toXml(), map2.toXml()).c_str());
    std::string response = postXml("api/dialup/profiles", request);
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("ProfileAdd: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::ProfileModify(int Index, std::string name, std::string apn, std::string user, std::string password, int IsValid, int ApnIsStatic, std::string DialupNum, int AuthMode, int IpIsStatic, std::string IpAddress, int DnsIsStatic, std::string PrimaryDns, std::string SecondaryDns, int ReadOnly, int iptype)
{
    map_str_str map1;
    map1.insert("Delete", 0);
    map1.insert("SetDefault", 0);
    map1.insert("Modify", 2);
    
    map_str_str map2;
    map2.insert("Index", Index);
    map2.insert("IsValid", IsValid);
    map2.insert("Name", name);
    map2.insert("ApnIsStatic", ApnIsStatic);
    map2.insert("ApnName", apn);
    map2.insert("DialupNum", DialupNum);
    map2.insert("Username", user);
    map2.insert("Password", password);
    map2.insert("AuthMode", AuthMode);
    map2.insert("IpIsStatic", IpIsStatic);
    map2.insert("IpAddress", IpAddress);
    map2.insert("DnsIsStatic", DnsIsStatic);
    map2.insert("PrimaryDns", PrimaryDns);
    map2.insert("SecondaryDns", SecondaryDns);
    map2.insert("ReadOnly", ReadOnly);
    map2.insert("iptype", iptype);
    
    std::string request = std::string(wxString::Format("<request>%s<Profile>%s</Profile></request>", map1.toXml(), map2.toXml()).c_str());
    std::string response = postXml("api/dialup/profiles", request);
    
    int error = Errorcode(response);
    if(error != 0) {
        wxPrintf("ProfileModify: %s\n", Errortext(error));
        return error;
    }
    
    return 0;
}

long Hilink::ProfileSetReadOnly(int index, bool readOnly)
{
    auto profile = GetProfileByIndex(index);
    if(profile.Index == 0) {
        wxPrintf("ProfileSetReadOnly: profile not found\n");
        return 1;
    }
    
    return ProfileModify(index, profile.Name, profile.ApnName, profile.Username, profile.Password, profile.IsValid, profile.ApnIsStatic, profile.DialupNum, profile.AuthMode, profile.IpIsStatic, profile.IpAddress, profile.DnsIsStatic, profile.PrimaryDns, profile.SecondaryDns, readOnly ? 2 : 0, profile.iptype);
}

size_t Hilink::write_callback(char *data, size_t size, size_t nmemb, std::string *userdata)
{
	if (userdata && size && nmemb) {
		wxString conv = wxString(data, wxConvUTF8);
		//userdata->append(conv.c_str(), conv.length());
		userdata->append(data, size * nmemb);
		//wxMessageBox(wxString(data, wxConvUTF8).c_str());
	}
    
    return size * nmemb;
}

std::string Hilink::getXml(std::string path)
{
    std::string result;
    CURL *curl = getCurlThreadHandle();
    if(!curl)
        return "";
    
    std::string buf = "http://" + GetIpAddress() + "/" + path;
    curl_easy_setopt(curl, CURLOPT_URL, buf.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
    //curl_easy_setopt(curl, CURLOPT_EXPECT_100_TIMEOUT_MS, 100);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 150000);
    
	char header1[512] = "", header2[512] = "";
    sprintf(header1, "__RequestVerificationToken: %s", GetToken().c_str());
	sprintf(header2, "Cookie: SessionID=%s", GetSessionId().c_str());
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, header2);
    
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_perform(curl);

    curl_slist_free_all(headers);
    
    return result;
}

std::string Hilink::postXml(std::string path, std::string post)
{
    std::string ret;
    
    if(!IsDeviceOnline())
        return ret;
    
    CURL *curl = curl_easy_init();
    if(curl) {
        std::string buf = "http://" + GetIpAddress() + "/" + path;
        curl_easy_setopt(curl, CURLOPT_URL, buf.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ret);
        
		char header1[512] = "", header2[512] = "";
		sprintf(header1, "__RequestVerificationToken: %s", UpdateToken().c_str());
		sprintf(header2, "Cookie: SessionID=%s", GetSessionId().c_str());
		struct curl_slist* headers = NULL;
		headers = curl_slist_append(headers, header1);
		headers = curl_slist_append(headers, header2);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
       
        char post_[10240] = "";
        strcpy(post_, post.c_str());
        
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(post_));
        
        curl_easy_perform(curl);
        curl_slist_free_all(headers);
        curl_easy_cleanup(curl);
    }
    
    return ret;
}

long Hilink::Errorcode(wxString xml_result)
{
    if(xml_result.length() == 0)
        return 3;
    
    wxStringInputStream stream(xml_result);
    wxXmlDocument doc(stream);
    
    if(!doc.IsOk())
        return 1;
    
    if(!doc.GetRoot())
        return 2;
    
    if(doc.GetRoot()->GetName() == "response" &&
       doc.GetRoot()->GetNodeContent() == "OK") {
        return 0;
    }
    
    if(doc.GetRoot()->GetName() == "error") {
        if(doc.GetRoot()->GetChildren()->GetName() == "code") {
            long buf = 1;
            doc.GetRoot()->GetChildren()->GetNodeContent().ToLong(&buf);
            return buf;
        }
    }
    
    return 0;
}

std::string Hilink::Errortext(long errorcode)
{
    std::string error;
    
    switch(errorcode) {
        case 000003: error = "ERROR_NO_RESPONSE (3): The system did not response."; break;
        case 100002: error = "ERROR_SYSTEM_NO_SUPPORT (100002): The system does not support this operation."; break;
        case 100003: error = "ERROR_SYSTEM_NO_RIGHTS (100003): No rights to perform this operation."; break;
        case 100004: error = "ERROR_SYSTEM_BUSY (100004): The system is busy."; break;
        case 100005: error = "ERROR_SYSTEM_WRONG_PARAMETER (100005): Wrong parameters were passed into the HTML header."; break;
        case 107723: error = "ERROR_PROFILE_STATIC_APN: Can not use no static APN."; break;
        case 107724: error = "ERROR_WRONG_PROFILE_INDEX (107724): Wrong profile index was specified."; break;
        case 107725: error = "ERROR_WRONG_PROFILE_INDEX (107725): Wrong profile index was specified."; break;
        case 107726: error = "ERROR_WRONG_PROFILE_INDEX (107726): Wrong profile index was specified."; break;
        case 112003: error = "ERROR_NETMODE_BAD_PARAMETER (112003): NetworkBand and/or LTEBand do not work with the chosen NetMode."; break;
        case 112005: error = "ERROR_REGISTER_NETWORK_NOT_POSSIBLE: Could not register to the network."; break;
        case 113055: error = "ERROR_SMS_ALREADY_READ (113055): SMS was already set as read."; break;
        case 113114: error = "ERROR_SMS_BAD_INDEX (113114): Bad index for sms was passed."; break;
        case 125001: error = "ERROR_WRONG_TOKEN (125001): Wrong or outdated token was passed into the HTML header."; break;
        case 125002: error = "ERROR_WRONG_SESSION (125002): No session running."; break;
        case 125003: error = "ERROR_WRONG_SESSION_TOKEN (125003): Wrong or outdated session key was passed into the HTML header."; break;
        default: error = wxString::Format("ERROR_UNKNOWN (%i): The error is unknown.", int(errorcode)); break;
    }
    
    //error += '\n';
    
    return error;
}

map_str_str Hilink::xmlToMap(const std::string &xml)
{
    map_str_str result;
    if(xml.length() == 0)
        return result;
    
    wxStringInputStream stream(xml);
    wxXmlDocument doc(stream);
    
    if(!doc.IsOk())
        return result;
    
    wxXmlNode *node = doc.GetRoot()->GetChildren();
    
    while(node) {
	/*	auto name = std::string(node->GetName());
		if (name == "cell_id" || name == "CellID" || name == "Lac" || name == "SerialNumber" || name == "Imei" || name == "Iccid" || name == "Imsi"
			|| name == "MacAddress1" || name == "sc")
			result.insert(name, "*censored*");
		else if (name == "WanIPAddress" && node->GetNodeContent().length())
			result.insert(name, "***" + std::string(node->GetNodeContent()).substr(6));
		else*/
			result.insert(std::string(node->GetName()), std::string(node->GetNodeContent()));

        node = node->GetNext();
    }
    
    return result;
}

std::string Hilink::mapToXml(const map_str_str &map)
{
    std::string result;
    
    for(auto it : map.get())
        result += "<" + it.first + ">" + it.second + "</" + it.first + ">";
    
    return result;
}

std::string Hilink::mapToXmlRequest(const map_str_str &map)
{
    std::string request = "<request>" + mapToXml(map) +  "</request>";
    return request;
}

const map_str_str &Hilink::api_device_signal()
{
    std::string xml = getXml("api/device/signal");
    if(Errorcode(xml) != 0)
        return deviceSignal;
    
    deviceSignal = xmlToMap(xml);
    
    return deviceSignal;
}

const map_str_str &Hilink::api_monitoring_checknotifications()
{
    std::string xml = getXml("api/monitoring/check-notifications");
    if(Errorcode(xml) != 0)
        return monitoringCheckNotifications;
    
    monitoringCheckNotifications = xmlToMap(xml);
    
    return monitoringCheckNotifications;
}

const map_str_str &Hilink::api_monitoring_convergedstatus()
{
    std::string xml = getXml("api/monitoring/converged-status");
    if(Errorcode(xml) != 0)
        return monitoringConvergedStatus;
    
    monitoringConvergedStatus = xmlToMap(xml);
    
    return monitoringConvergedStatus;
}

const map_str_str &Hilink::api_monitoring_monthstatistics()
{
    std::string xml = getXml("api/monitoring/month_statistics");
    if(Errorcode(xml) != 0)
        return monitoringMonthStatistics;
    
    monitoringMonthStatistics = xmlToMap(xml);
    
    return monitoringMonthStatistics;
}

const map_str_str &Hilink::api_monitoring_startdate()
{
    std::string xml = getXml("api/monitoring/start_date");
    if(Errorcode(xml) != 0)
        return monitoringStartDate;
    
    monitoringStartDate = xmlToMap(xml);
    
    return monitoringStartDate;
}

const map_str_str &Hilink::api_monitoring_status()
{
    std::string xml = getXml("api/monitoring/status");
    if(Errorcode(xml) != 0)
        return monitoringStatus;
    
    monitoringStatus = xmlToMap(xml);
    
    return monitoringStatus;
}

const map_str_str &Hilink::api_monitoring_traffic()
{
    std::string xml = getXml("api/monitoring/traffic-statistics");
    if(Errorcode(xml) != 0)
        return monitoringTraffic;
    
    monitoringTraffic = xmlToMap(xml);
    
    return monitoringTraffic;
}

const map_str_str &Hilink::api_pin_status()
{
    std::string xml = getXml("api/pin/status");
    if(Errorcode(xml) != 0)
        return pinStatus;
    
    pinStatus = xmlToMap(xml);
    
    return pinStatus;
}

const map_str_str &Hilink::api_pin_simlock()
{
    std::string xml = getXml("api/pin/simlock");
    if(Errorcode(xml) != 0)
        return pinSimlock;
    
    pinSimlock = xmlToMap(xml);
    
    return pinSimlock;
}

const map_str_str &Hilink::api_dhcp_settings()
{
    std::string xml = getXml("api/dhcp/settings");
    if(Errorcode(xml) != 0)
        return dhcpSettings;
    
    dhcpSettings = xmlToMap(xml);
    
    return dhcpSettings;
}

const map_str_str &Hilink::api_net_current_plmn()
{
    std::string xml = getXml("api/net/current-plmn");
    if(Errorcode(xml) != 0)
        return netCurrentPlmn;
    
    netCurrentPlmn = xmlToMap(xml);
    
    return netCurrentPlmn;
}

const map_str_str &Hilink::api_net_signal_para()
{
    std::string xml = getXml("api/net/signal-para");
    if(Errorcode(xml) != 0)
        return netSignalPara;
    
    netSignalPara = xmlToMap(xml);
    
    return netSignalPara;
}

const map_str_str &Hilink::api_net_netmode()
{
    std::string xml = getXml("api/net/net-mode");
    if(Errorcode(xml) != 0)
        return netNetmode;
    
    netNetmode = xmlToMap(xml);
    
    return netNetmode;
}

const map_str_str &Hilink::api_net_network()
{
    std::string xml = getXml("api/net/network");
    if(Errorcode(xml) != 0)
        return netNetwork;
    
    netNetwork = xmlToMap(xml);
    
    return netNetwork;
}

std::string Hilink::api_webserver_token(std::string &sessid)
{
    std::string xml = getXml("api/webserver/SesTokInfo");
    if(Errorcode(xml) != 0)
        return 0;
    
    auto webserverToken = xmlToMap(xml);
	sessid = webserverToken["SesInfo"];
	sessid = sessid.substr(strlen("SessionID="), std::string::npos);
    
    return webserverToken["TokInfo"];
}

const map_str_str &Hilink::api_device_information()
{
    UpdateToken();
    
    std::string xml = getXml("api/device/information");
    if(Errorcode(xml) != 0)
        return deviceInformation;
    
    deviceInformation = xmlToMap(xml);
    
    return deviceInformation;
}

const map_str_str &Hilink::api_device_basic_information()
{
    std::string xml = getXml("api/device/basic_information");
    if(Errorcode(xml) != 0)
        return deviceBasicInformation;
    
    deviceBasicInformation = xmlToMap(xml);
    
    return deviceBasicInformation;
}

const map_str_str &Hilink::api_dialup_connection()
{
    std::string xml = getXml("api/dialup/connection");
    if(Errorcode(xml) != 0)
        return dialupConnection;
    
    dialupConnection = xmlToMap(xml);
    
    return dialupConnection;
}

const map_str_str &Hilink::api_dialup_mobiledataswitch()
{
    std::string xml = getXml("api/dialup/mobile-dataswitch");
    if(Errorcode(xml) != 0)
        return dialupMobileDataswitch;
    
    dialupMobileDataswitch = xmlToMap(xml);
    
    return dialupMobileDataswitch;
}

const map_str_str &Hilink::api_sms_config()
{
    UpdateToken();
    
    std::string xml = getXml("api/sms/config");
    if(Errorcode(xml) != 0)
        return smsConfig;
    
    smsConfig = xmlToMap(xml);
    
    return smsConfig;
}

const map_str_str &Hilink::api_sms_sendstatus()
{
    UpdateToken();
    
    std::string xml = getXml("api/sms/send-status");
    if(Errorcode(xml) != 0)
        return smsSendStatus;
    
    smsSendStatus = xmlToMap(xml);
    
    return smsSendStatus;
}

const map_str_str &Hilink::api_sms_smscount()
{
    std::string xml = getXml("api/sms/sms-count");
    if(Errorcode(xml) != 0)
        return smsSendStatus;
    
    smsSendStatus = xmlToMap(xml);
    
    return smsSendStatus;
}

const map_str_str &Hilink::api_security_dmz()
{
    UpdateToken();
    
    std::string xml = getXml("api/security/dmz");
    if(Errorcode(xml) != 0)
        return securityDmz;
    
    securityDmz = xmlToMap(xml);
    
    return securityDmz;
}

const map_str_str &Hilink::api_security_upnp()
{
    UpdateToken();
    
    std::string xml = getXml("api/security/upnp");
    if(Errorcode(xml) != 0)
        return securityUpnp;
    
    securityUpnp = xmlToMap(xml);
    
    return securityUpnp;
}

const map_str_str &Hilink::api_security_sip()
{
    UpdateToken();
    
    std::string xml = getXml("api/security/sip");
    if(Errorcode(xml) != 0)
        return securitySip;
    
    securitySip = xmlToMap(xml);
    
    return securitySip;
}

const map_str_str &Hilink::api_security_firewallswitch()
{
    UpdateToken();
    
    std::string xml = getXml("api/security/firewall-switch");
    if(Errorcode(xml) != 0)
        return securityFirewallswitch;
    
    securityFirewallswitch = xmlToMap(xml);
    
    return securityFirewallswitch;
}

const map_str_str &Hilink::api_global_moduleswitch()
{
    UpdateToken();
    
    std::string xml = getXml("api/global/module-switch");
    if(Errorcode(xml) != 0)
        return globalModuleSwitch;
    
    globalModuleSwitch = xmlToMap(xml);
    
    return globalModuleSwitch;
}

std::list<Hilink::api_sms_list_contact_t> Hilink::api_sms_listcontact(char *xml)
{
    std::list<api_sms_list_contact_t> ret;
    
    if(!xml || strlen(xml) == 0)
        return ret;
    
    ret.clear();
    
    wxStringInputStream stream(xml);
    wxXmlDocument doc(stream);
    wxXmlNode *node = doc.GetRoot()->GetChildren()->GetNext()->GetChildren();
    
    long count = 0;
    
    if(doc.GetRoot()->GetChildren()->GetName() == "Count") {
        doc.GetRoot()->GetChildren()->GetNodeContent().ToLong(&count);
    }
    
    while(node) {
        
        if(node->GetName() == "Message") {
            
            wxXmlNode *node2 = node->GetChildren();
            api_sms_list_contact_t buf = { 0 };
            
            while(node2) {
                wxString tag = node2->GetName();
                wxString content = node2->GetNodeContent();
                
                node2 = node2->GetNext();
                
                if(content == "")
                    continue;
                
                if(tag == "Smstat") content.ToLong(&buf.Smstat);
                if(tag == "Index") content.ToLong(&buf.Index);
                if(tag == "Phone") buf.Phone = content.ToStdString();
                if(tag == "Content") buf.Content = content.ToStdString();
                if(tag == "Date") {
                    buf.Date = content.ToStdString();
                    char buf_date[32] = { 0 };
 
                    tm tm_;
                    strptime(buf.Date.c_str(), "%Y-%m-%d %H:%M:%S", &tm_);
                    buf.time = (uint64_t)mktime(&tm_);
                    
                    time_t time_ = (time_t)buf.time;
                    strftime(buf_date, 32, "%d.%m.%y %H:%M:%S", localtime(&time_));
                    buf.Date = std::string(buf_date);
                }
                if(tag == "Sca") buf.Sca = content.ToStdString();
                if(tag == "SaveType") content.ToLong(&buf.SaveType);
                if(tag == "Priority") content.ToLong(&buf.Priority);
                if(tag == "SmsType") content.ToLong(&buf.SmsType);
                if(tag == "UnreadCount") content.ToLong(&buf.UnreadCount);
            }
            
            if(buf.Index != 0) {
                ret.push_back(buf);
            }
        }
        node = node->GetNext();
    }
    
    if(long(ret.size()) != count)
        wxMessageBox("error in method api_sms_listcontact");
    
    return ret;
}

std::list<Hilink::api_sms_list_phone_t> Hilink::api_sms_listphone(char *xml)
{
    std::list<api_sms_list_phone_t> ret;
    
    if(!xml || strlen(xml) == 0)
        return ret;

    ret.clear();
    wxStringInputStream stream(xml);
    wxXmlDocument doc(stream);
    wxXmlNode *node = doc.GetRoot()->GetChildren()->GetNext()->GetChildren();
    
    long count = 0;
    
    if(doc.GetRoot()->GetChildren()->GetName() == "Count") {
        doc.GetRoot()->GetChildren()->GetNodeContent().ToLong(&count);
    }
    
    while(node) {
        
        if(node->GetName() == "Message") {
            
            wxXmlNode *node2 = node->GetChildren();
            api_sms_list_phone_t buf = { 0 };
            
            while(node2) {
                wxString tag = node2->GetName();
                wxString content = node2->GetNodeContent();
                
#ifdef _MSC_VER
               // content = content.ToAscii();
#endif
                
                node2 = node2->GetNext();
                
                if(content == "")
                    continue;
                
                if(tag == "Smstat") content.ToLong(&buf.Smstat);
                if(tag == "Index") content.ToLong(&buf.Index);
                if(tag == "Phone") buf.Phone = content.ToStdString();
                if(tag == "Content") buf.Content = content.ToStdString();
                if(tag == "Date") {
                    buf.Date = content.ToStdString();
                    char buf_date[32] = { 0 };
         
                    tm tm_;
                    strptime(buf.Date.c_str(), "%Y-%m-%d %H:%M:%S", &tm_);
                    buf.time = (uint64_t)mktime(&tm_);
                    
                    time_t time_ = (time_t)buf.time;
                    strftime(buf_date, 32, "%d.%m.%y %H:%M:%S", localtime(&time_));
                    buf.Date = std::string(buf_date);
                }
                if(tag == "Sca") buf.Sca = content.ToStdString();
                if(tag == "CurBox") content.ToLong(&buf.CurBox);
                if(tag == "SaveType") content.ToLong(&buf.SaveType);
                if(tag == "Priority") content.ToLong(&buf.Priority);
                if(tag == "SmsType") content.ToLong(&buf.SmsType);
            }
            
            if(buf.Index != 0) {
                ret.push_back(buf);
            }
        }
        node = node->GetNext();
    }
    
    if(long(ret.size()) != count)
        wxMessageBox("error in method api_sms_list_phone");
    
    return ret;
}
