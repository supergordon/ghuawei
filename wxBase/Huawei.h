//
//  Huawei.h
//  wxBase
//
//  Created by Gordon Freeman on 03.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __HUAWEI__
#define __HUAWEI__

#include <inttypes.h>
#include "utils.h"
#include <map>
#include <string>
#include <time.h>
#include <stdio.h>
//#include <pthread.h>
//#include <unistd.h>
#include <string.h>
#include <list>
#include <curl/curl.h>

#include <wx/wx.h>
#include <wx/xml/xml.h>
#include <wx/sstream.h>
#include <wx/notebook.h>
#include <wx/listctrl.h>
#include <wx/protocol/http.h>
#include <wx/arrstr.h> 
#include <wx/base64.h> 

//typedef std::map<std::string, std::string> map_str_str;
typedef MyMap map_str_str;

extern void cleanupCurlThreadHandle();
extern CURL *getCurlThreadHandle();
extern CURL *existsCurlThreadHandle();

class Hilink {
public:
    
    enum SMSBOX {
        BOX_INBOX = 0,
        BOX_OUTBOX = 1,
        BOX_DRAFT = 2
    };
    
    enum SMSTAT {
        SMSTAT_UNREAD = 0,
        SMSTAT_READ = 1,
        SMSTAT_SENT = 2
    };
    
    enum PINSTATUS {
        PIN_NO_SIM_CARD = 255,
        PIN_CPIN_FAIL = 256, //PinOptState
        PIN_READY = 257,
        PIN_DISABLE = 258, //PinOptState
        PIN_VALIDATE = 259, //PinOptState
        PIN_PIN_REQUIRED = 260,
        PIN_PUK_REQUIRED = 261,
    };

    enum NETWORKMODE {
        NETWORKMODE_AUTO = 0,
        NETWORKMODE_2G_ONLY = 1,
        NETWORKMODE_3G_ONLY = 2,
        NETWORKMODE_2G_PREFERED = 3,
        NETWORKMODE_3G_PREFERED = 4,
        NETWORKMODE_4G_ONLY = 5,
        NETWORKMODE_4G_PREFERED = 6
    };
    
    enum NETWORKMODE2 {
        NETMODE_AUTO = 0,
        NETMODE_2G_ONLY = 1,
        NETMODE_3G_ONLY = 2,
        NETMODE_4G_ONLY = 3,
        NETMODE_WCDMA_GSM = 201,
        NETMODE_LTE_WCDMA = 302
    };
    
    enum CONNECTIONSTATUS {
        CONNECTION_CONNECTING = 900,
        CONNECTION_CONNECTED = 901,
        CONNECTION_DISCONNECTED = 902,
        CONNECTION_DISCONNECTING = 903,
        CONNECT_FAILED = 904,
		CONNECT_STATUS_NULL = 905,
		CONNECT_STATUS_ERRO = 906
    };
    
    enum CURRENTNETWORK {
        NETWORK_2G = 0,
        NETWORK_3G = 2,
        NETWORK_H = 5,
        NETWORK_4G = 7,
    };
    
    enum SYSTEMERROR {
        ERROR_SYSTEM_NO_SUPPORT = 100002,
        ERROR_SYSTEM_NO_RIGHTS = 100003,
        ERROR_SYSTEM_BUSY = 100004,
        ERROR_SYSTEM_WRONG_PARAMETER = 100005,
        ERROR_LOGIN_USERNAME_WRONG = 108001,
        ERROR_LOGIN_PASSWORD_WRONG = 108002,
        ERROR_LOGIN_ALREADY_LOGIN = 108003,
        ERROR_LOGIN_USERNAME_PWD_WRONG = 108006,
        ERROR_LOGIN_USERNAME_PWD_ORERRUN = 108007,
        ERROR_VOICE_BUSY = 120001,
        ERROR_WRONG_TOKEN = 125001,
        ERROR_WRONG_SESSION = 125002,
        ERROR_WRONG_SESSION_TOKEN = 125003,
        ERROR_SMS_ALREADY_READ = 113055,
        ERROR_NETWORK_NON_SUPPORT = 111022,
        ERROR_NETWORK_NO_RESPONSE = 111019,
        ERROR_NETWORK_TIMEOUT_ERROR = 111020
    };
    
    enum {
        MODE_INACTIVE = 0,
        MODE_ACTIVE = 1,
        SIM_STATUS_LOCKED = 1,
        SIM_STATUS_OK = 1,
        SERVICE_STATUS_AVAILABLE = 2,
        PLMN_USAGE = 1,
        PLMN_REGISTERED = 2,
        PLMN_FORBIDDEN = 3,
        DIALUP_AUTO_SEARCH_NET = 0,
        DIALUP_MANUAL_SEARCH_NET = 1,
        VIRTUAL_SERVER_NUM = 16
    };
    
    enum PROTOCOL {
        PROTOCOLL_BOTH = 0,
        PROTOCOL_IMCP = 1,
        PROTOCOL_TCP = 6,
        PROTOCOL_UDP = 17
    };
    
    enum FILTER {
        FILTER_DISABLED = 0,
        FILTER_ENABLED = 1
    };
    
    enum PORT {
        MIN_PORT_RANGE = 1,
        MAX_PORT_RANGE = 0xFFFF
    };
    
    static const uint64_t BAND_NONE = 0x00;
    static const uint64_t BAND_GSM1800 = 0x80;
    static const uint64_t BAND_GSM900 = 0x300;
	static const uint64_t BAND_GSM850 = 0x80000;
	static const uint64_t BAND_GSM1900 = 0x200000;
    static const uint64_t BAND_WCDMA2100 = 0x400000;
	static const uint64_t BAND_WCDMA1900 = 0x800000;
	static const uint64_t BAND_WCDMA850 = 0x4000000;
    static const uint64_t BAND_WCDMA900 = 0x2000000000000;
	static const uint64_t BAND_ALL = 0x2000004600380;
    
	enum BANDS2 {
		LTEBAND_NONE = 0x0,
		LTEBAND_B1 = 0x1,
		LTEBAND_B3 = 0x4,
		LTEBAND_B5 = 0x10,
		LTEBAND_B7 = 0x40,
		LTEBAND_B8 = 0x80,
		LTEBAND_B20 = 0x80000
    };

	static const uint64_t LTEBAND_B38 = 0x2000000000;
	static const uint64_t LTEBAND_B40 = 0x8000000000;
	static const uint64_t LTEBAND_ALL = 0xA0000800C5;

    long ToLong(std::string input, int base = 10)
    {
        if(input.length() == 0) return 0;
        return strtol(input.c_str(), 0, base);
    }
    
    unsigned long long ToULongLong(std::string input, int base = 10)
    {
        if(input.length() == 0) return 0;
        unsigned long long buf = 0;
        wxString(input.c_str()).ToULongLong(&buf, base);
        
        return buf;
    }
    
    uint64_t ToUInt64(std::string input, int base = 10)
    {
        return static_cast<uint64_t>(ToULongLong(input, base));
    }
    
    long GetPci() { return deviceSignal.asLong("pci"); }
    long GetSc() { return deviceSignal.asLong("sc"); }
    long GetCellId() { return deviceSignal.asLong("cell_id"); }
    long GetRsrq() { return deviceSignal.asLong("rsrq"); }
    long GetRsrp() { return deviceSignal.asLong("rsrp"); }
    long GetRssi() { return deviceSignal.asLong("rssi"); }
    long GetSinr() { return deviceSignal.asLong("sinr"); }
    long GetRscp() { return deviceSignal.asLong("rscp"); }
    long GetEcIo() { return deviceSignal.asLong("ecio"); }
    long GetMode() { return deviceSignal.asLong("mode"); }
    long GetAsu() {
        int asu = 0;
        float asu_percentage = 0;
        
        if(GetCurrentPlmnRat() == NETWORK_2G) {
            asu = (GetRssi() + 113) / 2; //RSSI [dBm] = (2x ASU) – 113
            asu_percentage = float(asu) / 31 * 100; //ASU: 0 - 31
        }
        else if(GetCurrentPlmnRat() == NETWORK_3G) {
            asu = GetRscp() + 116; //RSCP [dBm] = ASU – 116
            asu_percentage = float(asu) / 91 * 100; //ASU: 0 - 91
        }
        else if(GetCurrentPlmnRat() == NETWORK_4G) {
            asu = GetRsrp() + 140; //RSRP [dBm] = ASU – 140
            asu_percentage = float(asu) / 95 * 100; //ASU: 0 - 95
        }

        return asu;
    }
    
    long GetCurrentConnectTime() { return ToLong(monitoringTraffic["CurrentConnectTime"]); }
    uint64_t GetCurrentUpload() { return ToUInt64(monitoringTraffic["CurrentUpload"]); }
    uint64_t GetCurrentDownload() { return ToUInt64(monitoringTraffic["CurrentDownload"]); }
    uint64_t GetCurrentDownloadRate() { return ToUInt64(monitoringTraffic["CurrentDownloadRate"]); }
    uint64_t GetCurrentUploadRate() { return ToUInt64(monitoringTraffic["CurrentUploadRate"]); }
    uint64_t GetTotalUpload() { return ToUInt64(monitoringTraffic["TotalUpload"]); }
    uint64_t GetTotalDownload() { return ToUInt64(monitoringTraffic["TotalDownload"]); }
    long GetTotalConnectTime() { return ToLong(monitoringTraffic["TotalConnectTime"]); }
    long GetShowTraffic() { return ToLong(monitoringTraffic["showtraffic"]); }
    long GetConnectionStatus() { return strtol(monitoringStatus["ConnectionStatus"].c_str(), 0, 10); }
    static wxString GetConnectionStatusText(int s, int ss = 2) {
        
        if(ss == 0) return "No service";
        if(ss == 1) return "No service (no network)";
        
        switch(s) {
            case 000: return "Undefined";
			case 32: return "Wrong APN/DFU settings";
            case 900: return "going online...";
            case 901: return "online";
            case 902: return "offline";
            case 903: return "going offline...";
            case 904: return "offline";
            case 905: return "offline (connection failed)";
            case 112: return "No autoconnect";
            case 113: return "No autoconnect (roaming)";
            case 114: return "No reconnect on timeout";
            case 115: return "No reconnect on timeout (roaming)";
            default: return "Unknown";
        }
    }
    long GetWifiConnectionStatus() { return ToLong(monitoringStatus["WifiConnectionStatus"]); }
    long GetSignalStrength() { return ToLong(monitoringStatus["SignalStrength"]); }
    long GetSignalIcon() { return ToLong(monitoringStatus["SignalIcon"]); }
    long GetCurrentNetworkType() { return ToLong(monitoringStatus["CurrentNetworkType"]); }
    long GetCurrentServiceDomain() { return ToLong(monitoringStatus["CurrentServiceDomain"]); }
    long GetRoamingStatus() { return ToLong(monitoringStatus["RoamingStatus"]); }
    long GetBatteryStatus() { return ToLong(monitoringStatus["BatteryStatus"]); }
    long GetBatteryLevel() { return ToLong(monitoringStatus["BatteryLevel"]); }
    long GetBatteryPercent() { return ToLong(monitoringStatus["BatteryPercent"]); }
    long GetSimlockStatus() { return ToLong(monitoringStatus["simlockStatus"]); }
    std::string GetWanIPAddress() { return deviceInformation["WanIPAddress"]; }
    std::string GetWanIPv6Address() { return deviceInformation["WanIPv6Address"]; }
    std::string GetPrimaryDns() { return monitoringStatus["PrimaryDns"]; }
    std::string GetSecondaryDns() { return monitoringStatus["SecondaryDns"]; }
    std::string GetPrimaryIPv6Dns() { return monitoringStatus["PrimaryIPv6Dns"]; }
    std::string GetSecondaryIPv6Dns() { return monitoringStatus["SecondaryIPv6Dns"]; }
    long GetCurrentWifiUser() { return ToLong(monitoringStatus["CurrentWifiUser"]); }
    long GetTotalWifiUser() { return ToLong(monitoringStatus["TotalWifiUser"]); }
    long GetCurrentTotalWifiUser() { return ToLong(monitoringStatus["currenttotalwifiuser"]); }
    long GetServiceStatus() { return ToLong(monitoringStatus["ServiceStatus"]); }
    long GetSimStatus() { return ToLong(monitoringStatus["SimStatus"]); }
    long GetWifiStatus() { return ToLong(monitoringStatus["WifiStatus"]); }
    long GetCurrentNetworkTypeEx() { return ToLong(monitoringStatus["CurrentNetworkTypeEx"]); }
    long GetMaxSignal() { return ToLong(monitoringStatus["maxsignal"]); }
    long GetWifiIndoorOnly() { return ToLong(monitoringStatus["wifiindooronly"]); }
    long GetWifiFrequence() { return ToLong(monitoringStatus["wififrequence"]); }
    long GetMsisdn() { return ToLong(monitoringStatus["msisdn"]); }
    std::string GetClassify() { return monitoringStatus["classify"]; }
    long GetFlymode() { return ToLong(monitoringStatus["flymode"]); }
    
    
    long GetCurrentPlmnState() { return strtol(netCurrentPlmn["State"].c_str(), 0, 10); }
    std::string GetCurrentPlmnFullName() { return netCurrentPlmn["FullName"]; }
    std::string GetCurrentPlmnShortName() { return netCurrentPlmn["ShortName"]; }
    std::string GetCurrentPlmnNumeric() { return netCurrentPlmn["Numeric"]; }
    long GetCurrentPlmnMcc()
    {
        std::string buf = netCurrentPlmn["Numeric"];
        if(buf == "") return 0;
        
        buf.erase(3);
        return strtol(buf.c_str(), 0, 10);
    }
    long GetCurrentPlmnMnc()
    {
        std::string buf = netCurrentPlmn["Numeric"];
        if(buf == "") return 0;
        
        buf = buf.substr(3, buf.length());
        return strtol(buf.c_str(), 0, 10);
    }
    long GetCurrentPlmnRat() { return strtol(netCurrentPlmn["Rat"].c_str(), 0, 10); }
    
    struct api_net_signal_para_t {
        long Rscp;
        long Ecio;
        long Rssi;
        long Lac;
        long CellID;
    };
    
    long GetLac() { return strtol(netSignalPara["Lac"].c_str(), 0, 16); }
    long GetCellIdPara() { return strtol(netSignalPara["CellID"].c_str(), 0, 16); }
    
    std::string GetDeviceName() { return deviceInformation["DeviceName"]; }
    std::string GetSerialNumber() { return deviceInformation["SerialNumber"]; }
    std::string GetImei() { return deviceInformation["Imei"]; }
    std::string GetImsi() { return deviceInformation["Imsi"]; }
    std::string GetIccid() { return deviceInformation["Iccid"]; }
    std::string GetTelephoneNumber() { return deviceInformation["Msisdn"]; }
    std::string GetHardwareVersion() { return deviceInformation["HardwareVersion"]; }
    std::string GetSoftwareVersion() { return deviceInformation["SoftwareVersion"]; }
    std::string GetWebUIVersion() { return deviceInformation["WebUIVersion"]; }
    std::string GetMacAddress1() { return deviceInformation["MacAddress1"]; }
    std::string GetMacAddress2() { return deviceInformation["MacAddress2"]; }
    std::string GetProductFamily() { return deviceInformation["ProductFamily"]; }
    //char *Classify() { return adi.Classify; }
    std::string GetSupportedModes() { return deviceInformation["supportmode"]; }
    std::string GetWorkMode() { return deviceInformation["workmode"]; }
    
    std::string GetDhcpIPAddress() { return dhcpSettings["DhcpIPAddress"]; }
    std::string GetDhcpLanNetmask() { return dhcpSettings["DhcpLanNetmask"]; }
    long GetDhcpStatus() { return ToLong(dhcpSettings["DhcpStatus"]);}
    std::string GetDhcpStartIPAddress() { return dhcpSettings["DhcpStartIPAddress"]; }
    std::string GetDhcpEndIPAddress() { return dhcpSettings["DhcpEndIPAddress"]; }
    long GetDhcpLeaseTime() { return ToLong(dhcpSettings["DhcpLeaseTime"]);}
    long GetDnsStatus() { return ToLong(dhcpSettings["DnsStatus"]);}
    std::string GetDhcpPrimaryDns() { return dhcpSettings["PrimaryDns"]; }
    std::string GetDhcpSecondaryDns() { return dhcpSettings["SecondaryDns"]; }
    
    long GetSimState() { return ToLong(pinStatus["SimState"]); }
    long GetPinOptState() { return ToLong(pinStatus["PinOptState"]); }
    long GetSimPinTimes() { return ToLong(pinStatus["SimPinTimes"]); }
    long GetSimPukTimes() { return ToLong(pinStatus["SimPukTimes"]); }
    
    long GetSimLockEnable() { return ToLong(pinSimlock["SimLockEnable"]); }
    long GetSimLockRemainTimes() { return ToLong(pinSimlock["SimLockRemainTimes"]); }
    long GetpSimLockEnable() { return ToLong(pinSimlock["pSimLockEnable"]); }
    long GetpSimLockRemainTimes() { return ToLong(pinSimlock["pSimLockRemainTimes"]); }
    
    long GetRoamAutoConnectEnable() { return ToLong(dialupConnection["RoamAutoConnectEnable"]); }
    long GetMaxIdleTime() { return ToLong(dialupConnection["MaxIdelTime"]); }
    long GetConnectMode() { return ToLong(dialupConnection["ConnectMode"]); }
    long GetMtu() { return ToLong(dialupConnection["MTU"]); }
    long GetAutoDialSwitch() { return ToLong(dialupConnection["auto_dial_switch"]); }
    
    
    long GetNetworkMode() { return ToLong(netNetmode["NetworkMode"]); }
    uint64_t GetNetworkBand() { return ToULongLong(netNetmode["NetworkBand"], 16); }
    uint64_t GetLteBand() { return ToULongLong(netNetmode["LTEBand"], 16); }

    long GetSmsConfigSaveMode() { return ToLong(smsConfig["SaveMode"]); }
    long GetSmsConfigValidity() { return ToLong(smsConfig["Validity"]); }
    std::string GetSmsConfigSca() { return smsConfig["Sca"]; }
    long GetSmsConfigSendReport() { return ToLong(smsConfig["UseSReport"]); }
    long GetSmsConfigSendType() { return ToLong(smsConfig["SendType"]); }
    
    uint64_t GetCurrentMonthDownload() { return ToULongLong(monitoringMonthStatistics["CurrentMonthDownload"]); }
    uint64_t GetCurrentMonthUpload() { return ToULongLong(monitoringMonthStatistics["CurrentMonthUpload"]); }
    uint64_t GetMonthDuration() { return ToULongLong(monitoringMonthStatistics["MonthDuration"]); }
    std::string GetMonthLastClearTime() { return monitoringMonthStatistics["MonthLastClearTime"]; }
    
    struct api_sms_list_contact_t {
        long Smstat;
        long Index;
        std::string Phone;
        std::string Content;
        std::string Date;
        std::string Sca;
        long SaveType;
        long Priority;
        long SmsType;
        long UnreadCount;
        uint64_t time;
    };
    
    struct api_sms_list_phone_t {
        long Smstat;
        long Index;
        std::string Phone;
        std::string Content;
        std::string Date;
        std::string Sca;
        long CurBox;
        long SaveType;
        long Priority;
        long SmsType;
        uint64_t time;
    };
    
    struct api_net_plmnlist_t {
        int Index;
        int State;
        std::string FullName;
        std::string ShortName;
        std::string Numeric;
        int Rat;
    };
    
    struct api_dialup_profile_t {
        int Index;
        int IsValid;
        std::string Name;
        int ApnIsStatic;
        std::string ApnName;
        std::string DialupNum;
        std::string Username;
        std::string Password;
        int AuthMode;
        int IpIsStatic;
        std::string IpAddress;
        std::string Ipv6Address;
        int DnsIsStatic;
        std::string PrimaryDns;
        std::string SecondaryDns;
        std::string PrimaryIpv6Dns;
        std::string SecondaryIpv6Dns;
        int ReadOnly;
        int iptype;
    };
    
    struct api_dialup_profiles_t {
        int CurrentProfile;
        std::vector<api_dialup_profile_t> profiles;
    };
    
    Hilink();
    ~Hilink();
    
    void Init();
    
    bool loginNeeded() { return login_needed; };
    
    static std::string GetNetworkModeText(int mode);
    static std::string GetModeText(int networkMode);
    static std::string GetPlmnStateText(int state);
    static std::string GetNetworkTypeText(int type);
    
    void SetIpAddress(std::string ip);
    const std::string &GetIpAddress();
    
    bool IsDeviceOnline();
	std::string UpdateToken();
    std::string GetToken();
	std::string GetSessionId();
	void GetTokenSupport();

	bool IsMonthlyMonitoringEnabled();
    long SetMonthlyMonitoring(int StartDay = 1, const char *DataLimit = "1000GB", int MonthThreshold = 90, int SetMonthData = MODE_ACTIVE);
    long SetEnableMonthlyMonitoring(bool toggle = true);
    long SetMonitoringClearTraffic();
    long SetAutoApn(bool toggle = false);
    long SetDataRoaming(bool toggle = false);
    bool IsDataRoaming();
    long MobileConnect(bool toggle);
    bool MobileReconnect();
    bool MobileToggleConnection();
	bool MobileRenewIp(int hops = 1);
    bool IsMobileConnected();
    long DeviceReboot();
    long DeviceBackupSettings(wxString path);
    long DeviceRestoreSettings(wxString path);
    long DeviceRestoreDefaults();
    long DeviceSwitchToProjectMode();
    long DeviceSwitchToDebugMode();
    long EnableUpnp(bool toggle);
    bool IsUpnpEnabled();
    long EnableDmz(bool toggle, std::string ip = "192.168.8.100");
    bool IsDmzEnabled();
    long EnableSipAlg(bool toggle);
    long EnableSipAlg(bool toggle, int port);
    bool IsSipAlgEnabled();
    long EnableFirewall(bool toggle);
    bool IsFirewallEnabled();
    
    long SetNetworkMode(int mode);
    long SetNetworkBand(uint64_t band);
    long SetNetworkModeBand(int mode, uint64_t band, uint64_t lteband);
    long SetLteBand(uint64_t band);

    long SmsSetSendReport(bool toggle = false);
    long SmsSetSmsSc(const char *smssc);
    long SmsSetSmsValidity(int validity);
    long SmsSendSms(const char *number, const char *content);
    long SmsDeleteSms(int index);
    long SmsSetReadSms(int index);
    
    long RegisterNetwork(bool automatic = true, std::string numeric = "", int rat = 2);
    
    std::list<api_sms_list_contact_t> GetSmsContacts();
    std::list<api_sms_list_phone_t> GetAllSmsFromNumber(const std::string &number);
    std::list<api_sms_list_phone_t> GetAllSms();
    std::list<api_net_plmnlist_t> GetPlmnList();
    api_dialup_profiles_t GetProfiles();
    api_dialup_profile_t GetProfileByIndex(int index);
    
    long ProfileDelete(int index);
    long ProfileSetDefault(int index);
    long ProfileAdd(std::string name, std::string apn, std::string user = "", std::string password = "", int IsValid = 1, int ApnIsStatic = 1, std::string DialupNum = "*99#", int AuthMode = 0, int IpIsStatic = 0, std::string IpAddress = "", int DnsIsStatic = 0, std::string PrimaryDns = "", std::string SecondaryDns = "", int ReadOnly = 0, int iptype = 2);
    
    long ProfileModify(int index, std::string name, std::string apn, std::string user = "", std::string password = "", int IsValid = 1, int ApnIsStatic = 1, std::string DialupNum = "*99#", int AuthMode = 0, int IpIsStatic = 0, std::string IpAddress = "", int DnsIsStatic = 0, std::string PrimaryDns = "", std::string SecondaryDns = "", int ReadOnly = 0, int iptype = 2);
    
    long ProfileSetReadOnly(int index, bool readOnly = true);
    
    static size_t write_callback(char *data, size_t size, size_t nmemb, std::string *userdata);
    std::string getXml(std::string path);
    std::string postXml(std::string path, std::string post);
    
    long Errorcode(wxString xml_result);
    std::string Errortext(long errorcode);
    map_str_str xmlToMap(const std::string &xml);
    std::string mapToXml(const map_str_str &map);
    std::string mapToXmlRequest(const map_str_str &map);
    
    void test();
    
	std::string api_webserver_token(std::string &sessid);
    const map_str_str &api_device_information();
    const map_str_str &api_device_basic_information();
    const map_str_str &api_device_signal();
    const map_str_str &api_net_current_plmn();
    const map_str_str &api_net_signal_para();
    const map_str_str &api_net_netmode();
    const map_str_str &api_net_network();
    const map_str_str &api_monitoring_checknotifications();
    const map_str_str &api_monitoring_convergedstatus();
    const map_str_str &api_monitoring_monthstatistics();
    const map_str_str &api_monitoring_startdate();
    const map_str_str &api_monitoring_status();
    const map_str_str &api_monitoring_traffic();
    const map_str_str &api_pin_status();
    const map_str_str &api_pin_simlock();
    const map_str_str &api_dhcp_settings();
    const map_str_str &api_dialup_connection();
    const map_str_str &api_dialup_mobiledataswitch();
    const map_str_str &api_sms_config();
    const map_str_str &api_sms_sendstatus();
    const map_str_str &api_sms_smscount();
    const map_str_str &api_security_dmz();
    const map_str_str &api_security_upnp();
    const map_str_str &api_security_sip();
    const map_str_str &api_security_firewallswitch();
    const map_str_str &api_global_moduleswitch();
    
    std::list<api_sms_list_contact_t> api_sms_listcontact(char *xml = NULL);
    std::list<api_sms_list_phone_t> api_sms_listphone(char *xml = NULL);
private:
    map_str_str dialupConnection;
    map_str_str dialupMobileDataswitch;
    map_str_str deviceInformation;
    map_str_str deviceBasicInformation;
    map_str_str deviceSignal;
    map_str_str netCurrentPlmn;
    map_str_str netSignalPara;
    map_str_str netNetmode;
    map_str_str netNetwork;
    map_str_str monitoringStatus;
    map_str_str monitoringTraffic;
    map_str_str monitoringMonthStatistics;
    map_str_str monitoringStartDate;
    map_str_str monitoringCheckNotifications;
    map_str_str monitoringConvergedStatus;
    map_str_str pinStatus;
    map_str_str pinSimlock;
    map_str_str dhcpSettings;
    map_str_str smsConfig;
    map_str_str smsSendStatus;
    map_str_str smsSmsCount;
    map_str_str globalModuleSwitch;
    map_str_str securityDmz;
    map_str_str securityUpnp;
    map_str_str securitySip;
    map_str_str securityFirewallswitch;
    
    std::string ipAddress;
	std::string token;
	std::string sessionid;
    bool login_needed;
    bool token_support;
};

extern Hilink *hilink;

#endif 
