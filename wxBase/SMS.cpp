//
//  SMS.cpp
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "MainDialog.h"
#include "SMS.h"

SMS::SMS(wxWindow *win)
{
   // wxPrintf("SMS()\n");
    panel = new wxPanel(win, wxID_ANY);
    
    createControls();
    setupControls();
    updateControls();
    setupSizers();
}

SMS::~SMS()
{
    //wxPrintf("~SMS()\n");
}

wxPanel *SMS::getPanel()
{
    return panel;
}

void SMS::createControls()
{
    listSms = new wxListCtrl(panel, wxID_ANY, wxDefaultPosition, wxSize(-1, 220), wxLC_REPORT | wxLC_VRULES | wxLC_HRULES);
    buttonSmsRefresh = new wxButton(panel, wxID_ANY, "Refresh", wxDefaultPosition, wxDefaultSize);
    checkInbox = new wxCheckBox(panel, wxID_ANY, "Inbox", wxDefaultPosition, wxDefaultSize);
    checkOutbox = new wxCheckBox(panel, wxID_ANY, "Outbox", wxDefaultPosition, wxDefaultSize);
    checkDraft = new wxCheckBox(panel, wxID_ANY, "Draft", wxDefaultPosition, wxDefaultSize);
    buttonSmsSend = new wxButton(panel, wxID_ANY, "Send Message...", wxDefaultPosition, wxDefaultSize);
    
    labelSmsSc = new wxStaticText(panel, wxID_ANY, "SMS Center", wxDefaultPosition, wxSize(100, -1));
    labelValidity = new wxStaticText(panel, wxID_ANY, "Validity", wxDefaultPosition, wxSize(100, -1));
    textSmsSc = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(150, -1), wxTE_CENTRE);
    spinValidity = new wxSpinCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(150, -1), wxSP_ARROW_KEYS | wxALIGN_CENTRE_HORIZONTAL, 0, 102400);
    buttonSmsSettingsApply = new wxButton(panel, wxID_ANY, "Apply", wxDefaultPosition, wxDefaultSize);
    checkSendReport = new wxCheckBox(panel, wxID_ANY, "Send Report", wxDefaultPosition, wxSize(100, -1));
}

void SMS::setupSizers()
{
    wxBoxSizer *sizerMain = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticBoxSizer *sizer3 = new wxStaticBoxSizer(wxVERTICAL, panel, "Settings");
    wxBoxSizer *sizer31 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer32 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer33 = new wxBoxSizer(wxHORIZONTAL);

    sizer2->Add(buttonSmsRefresh, 0);
    sizer2->AddSpacer(10);
    sizer2->Add(checkInbox);
    sizer2->AddSpacer(5);
    sizer2->Add(checkOutbox);
    sizer2->AddSpacer(5);
    sizer2->Add(checkDraft);
    sizer2->AddSpacer(10);
    sizer2->Add(buttonSmsSend, 1, wxGROW);
    
    sizer32->Add(labelSmsSc, 0, wxLEFT | wxUP, 3);
    sizer32->AddSpacer(10);
    sizer32->Add(textSmsSc);
    
    sizer31->Add(labelValidity, 0, wxLEFT | wxUP, 3);
    sizer31->AddSpacer(10);
    sizer31->Add(spinValidity);
    
    sizer33->Add(checkSendReport, 0, wxLEFT, 3);
    sizer33->AddSpacer(10);
    sizer33->Add(buttonSmsSettingsApply, 1, wxGROW);
    
    sizer3->Add(sizer31);
    sizer3->AddSpacer(5);
    sizer3->Add(sizer32);
    sizer3->AddSpacer(5);
    sizer3->Add(sizer33, 0, wxGROW);
    
    sizerMain->Add(listSms, 0, wxGROW);
    sizerMain->AddSpacer(7);
    sizerMain->Add(sizer2, 0, wxGROW);
    sizerMain->AddSpacer(5);
    sizerMain->Add(sizer3);
    
    panel->SetSizerAndFit(sizerMain);
}

void SMS::setupControls()
{
    g_inbox = true, g_outbox = false, g_draft = false;
    
    listSms->SetFont(g_smsFont);

    int c1 = listSms->InsertColumn(0, wxT("Date"));
    int c2 = listSms->InsertColumn(1, wxT("Number"));
    int c3 = listSms->InsertColumn(2, wxT("Content"));
    listSms->SetColumnWidth(c1, 107);
    listSms->SetColumnWidth(c2, 94);
    listSms->SetColumnWidth(c3, 220);
    
    listSms->Bind(wxEVT_LIST_ITEM_ACTIVATED, [this] (wxListEvent &event) { OnListSmsItemActivated(event); });
    listSms->Bind(wxEVT_CONTEXT_MENU, [this] (wxContextMenuEvent &event) { showContextMenu(); });
    
	buttonSmsRefresh->Bind(wxEVT_BUTTON, [this](wxCommandEvent &event) { refreshSms();  updateListSms(); updateSettings(); });
    buttonSmsSettingsApply->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonSettingsApply(event); });
    
    buttonSmsRefresh->SetFocus();
    
    checkInbox->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { g_inbox = event.IsChecked(); updateListSms(); });
    checkOutbox->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { g_outbox = event.IsChecked(); updateListSms(); });
    checkDraft->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { g_draft = event.IsChecked(); updateListSms(); });
    
    buttonSmsSend->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonSmsSend(event); } );
    
    checkInbox->SetValue(g_inbox);
    checkOutbox->SetValue(g_outbox);
    checkDraft->SetValue(g_draft);
}

void SMS::updateControls()
{
	refreshSms();
    updateListSms();
    updateSettings();
}

void SMS::updateSettings()
{
    auto sms_config = hilink->api_sms_config();
    if(sms_config.size()) {
        textSmsSc->SetValue(hilink->GetSmsConfigSca());
        spinValidity->SetValue(hilink->GetSmsConfigValidity());
        checkSendReport->SetValue(hilink->GetSmsConfigSendReport() == 1);
        
        static bool once = false;
        if(!once) {
            auto smsInfo = hilink->api_sms_smscount();
            if(smsInfo.asInt("LocalUnread") == 1)
                wxMessageBox("You have unread sms message(s)!");
                
            once = true;
        }
    }
}

void SMS::updateListSms()
{
    listSms->DeleteAllItems();
    
    int j = 0;
	for (auto it : cur_sms_list) {
        bool inbox = g_inbox && it.CurBox == Hilink::BOX_INBOX;
        bool outbox = g_outbox && it.CurBox == Hilink::BOX_OUTBOX;
        bool draft = g_draft && it.CurBox == Hilink::BOX_DRAFT;
        
        if(!inbox && !outbox && !draft)
            continue;
        
        int i = listSms->InsertItem(j++, wxString::Format(" %s", it.Date));
		listSms->SetItem(i, 1, wxString(it.Phone.c_str(), wxConvUTF8));
		listSms->SetItem(i, 2, wxString(it.Content.c_str(), wxConvUTF8));
        listSms->SetItemData(i, it.Index);
        if(it.Smstat == Hilink::SMSTAT_UNREAD)
            listSms->SetItemTextColour(i, wxColor(24, 116, 205));
    }
}

void SMS::refreshSms()
{
	cur_sms_list = hilink->GetAllSms();
}

wxString getItemColumnText(wxListCtrl *listCtrl, int item, int column)
{
    wxListItem info;
    info.SetId(item);
    info.SetColumn(column);
    info.SetMask(wxLIST_MASK_TEXT);
    listCtrl->GetItem(info);
    
    return info.GetText();
}

void SMS::OnListSmsItemActivated(wxListEvent &event)
{
    int item = LISTITEM_SELECTED_P(listSms);
    if(item == -1) {
        wxPrintf("OnListSmsItemActivated: Item not found!\n");
        return;
    }

    wxString date = getItemColumnText(listSms, item, 0);
    wxString number = getItemColumnText(listSms, item, 1);
    wxString content = getItemColumnText(listSms, item, 2);
    int index = listSms->GetItemData(item);

    wxDialog *dialog = new wxDialog(panel, wxID_ANY, "View message from" + date);
    wxStaticText *labelNumber = new wxStaticText(dialog, wxID_ANY, "Number:", wxDefaultPosition, wxDefaultSize);
	wxTextCtrl *textNumber = new wxTextCtrl(dialog, wxID_ANY, number, wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxTE_CENTRE);
	wxTextCtrl *textContent = new wxTextCtrl(dialog, wxID_ANY, content, wxDefaultPosition, wxSize(-1, 200), wxTE_READONLY | wxTE_MULTILINE);
    
    wxBoxSizer *sizerMain = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer1 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer1->Add(labelNumber, 0, wxUP | wxLEFT, 3);
    sizer1->AddSpacer(10);
    sizer1->Add(textNumber, 1, wxGROW | wxRIGHT, 3);
    
    sizerMain->AddSpacer(10);
    sizerMain->Add(sizer1, 0, wxGROW);
    sizerMain->AddSpacer(6);
    sizerMain->Add(textContent, 0, wxGROW | wxALL, 3);
    
    dialog->SetSizer(sizerMain);
    
    dialog->ShowModal();
    dialog->Destroy();
    
    hilink->SmsSetReadSms(index);
    listSms->SetItemTextColour(item, listSms->GetForegroundColour());
}

void SMS::OnButtonSettingsApply(wxCommandEvent &event)
{
    auto sms_config = hilink->api_sms_config();
    
    std::string sca = hilink->GetSmsConfigSca();
    int validity = hilink->GetSmsConfigValidity();
    int sr = hilink->GetSmsConfigSendReport();
    
    wxString input_sca = textSmsSc->GetValue();
    int input_validity = spinValidity->GetValue();
    int input_sr = checkSendReport->GetValue();
    
    if(sca != input_sca && input_sca.length() > 0) {
        wxPrintf("setting smssc...\n");
        hilink->SmsSetSmsSc((char*)input_sca.char_str());
    }
    
    if(validity != input_validity && input_validity > 0) {
        wxPrintf("setting validity...\n");
        hilink->SmsSetSmsValidity(input_validity);
    }
    
    if(sr != input_sr) {
        wxPrintf("setting sms send report...\n");
        hilink->SmsSetSendReport(input_sr);
    }
}

void SMS::OnButtonSmsSend(wxCommandEvent &event)
{
    wxDialog *dialog = new wxDialog(NULL, 0x25, "Send Message", wxDefaultPosition, wxSize(300, 300));
    dialog->SetFont(g_globalFont);
    
    wxStaticText *labelNumber = new wxStaticText(dialog, wxID_ANY, "Number:");
    wxTextCtrl *textCtrlNumber = new wxTextCtrl(dialog, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize);
    wxTextCtrl *textCtrlContent = new wxTextCtrl(dialog, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(-1, 120), wxTE_MULTILINE |
                                                 wxTE_PROCESS_TAB);
    wxStaticText *labelRemaining = new wxStaticText(dialog, wxID_ANY, "160 characters remaining");
    wxButton *buttonSend = new wxButton(dialog, wxID_ANY, "Send");

    textCtrlContent->Bind(wxEVT_TEXT, [=](wxCommandEvent &event) {
        int length = event.GetString().length();
        if(length > 160) {
            wxString cut = event.GetString().SubString(0, 159);
            wxMessageBox("Too many characters!\nCutting...");
            textCtrlContent->SetValue(cut);
            textCtrlContent->SetFocus();
            return;
        }
        labelRemaining->SetLabel(wxString::Format("%i characters remaining", 160 - length));
    });

    buttonSend->Bind(wxEVT_BUTTON, [=](wxCommandEvent &event) {
        wxString number = textCtrlNumber->GetValue();
        wxString content = textCtrlContent->GetValue();

        if(!number.length()) {
            wxMessageBox("Number field is empty", "Notice", wxICON_EXCLAMATION);
            return;
        }

        if(!content.length()) {
            wxMessageBox("Message field is empty", "Notice");
            return;
        }
        
        if(content.length() > 160) {
            wxMessageBox("Too many characters", "Notice");
            return;
        }

        if(hilink->SmsSendSms((char*)number.char_str(), (char*)content.char_str()) != 0) {
            wxMessageBox("Error sending SMS", "Error");
            return;
        }
        else {
            wxMessageBox("SMS has been sent!", "Notice");
        }

        dialog->Close();
    });

    wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *bar1 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *bar2 = new wxBoxSizer(wxHORIZONTAL);

    bar1->Add(labelNumber, 1, wxLEFT | wxRIGHT | wxUP, 5);
    bar1->AddStretchSpacer();
    bar1->Add(textCtrlNumber, 5, wxEXPAND | wxLEFT | wxRIGHT | wxUP, 5);

    bar2->Add(labelRemaining, 1, wxLEFT | wxRIGHT | wxDOWN, 5);
    bar2->AddStretchSpacer();
    bar2->Add(buttonSend, 1, wxEXPAND | wxLEFT | wxRIGHT | wxDOWN, 5);
    
    main->AddSpacer(2);
    main->Add(bar1);
    main->AddSpacer(2);
    main->Add(textCtrlContent, 10, wxEXPAND | wxALL, 5);
    main->AddSpacer(1);
    main->Add(bar2);
    dialog->SetSizerAndFit(main);

    dialog->ShowModal();
    dialog->Destroy();
}

void SMS::OnContextMenu(wxCommandEvent &event)
{
    LOOP_LISTCTRL_SELECTED_P(listSms, {
        int Index = int(listSms->GetItemData(i));
        
        if(event.GetId() == 0x650) {
            hilink->SmsSetReadSms(Index);
            listSms->SetItemTextColour(i, listSms->GetForegroundColour());
        }
        
        if(event.GetId() == 0x651) {
            hilink->SmsDeleteSms(Index);
            updateControls();
        }
    });
}

void SMS::showContextMenu()
{
    wxMenu *menu = new wxMenu();
    
    wxMenuItem *itemMarkAsRead = new wxMenuItem(menu, 0x650, "mark as read");
    wxMenuItem *itemDelete = new wxMenuItem(menu, 0x651, "delete");
    
    menu->Append(itemMarkAsRead);
    menu->Append(itemDelete);
    
    menu->Bind(wxEVT_COMMAND_MENU_SELECTED, [this] (wxCommandEvent &event) { OnContextMenu(event); });
    
    listSms->PopupMenu(menu);
}