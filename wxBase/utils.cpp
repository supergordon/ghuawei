//
//  utils.cpp
//  wxBase
//
//  Created by Gordon Freeman on 11.12.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//
#pragma warning(disable: 4996)
#include "wx/wx.h"
#include "utils.h"
#include "Huawei.h"

ManageThreads *myThreads = nullptr;

int strToInt(const std::string &input, int base)
{
    return strtol(input.c_str(), 0, base);
}

uint64_t strToUInt64(const std::string &input, int base)
{
    wxString str = input;
    uint64_t tmp;
    str.ToULongLong(&tmp, base);
    return tmp;
}

std::string intToStr(int value, int base)
{
    char buf[64] = { 0 };
    if(base == 10)
        sprintf(buf, "%i", value);
    else if(base == 16)
        sprintf(buf, "%x", (unsigned int)value);
    
    return std::string(buf);
}

std::string uint64ToStr(uint64_t value, int base)
{
    char buf[64] = { 0 };
    sprintf(buf, base==16 ? "%llX" : "%llu", value);
    return std::string(buf);
}

//credits: http://stackoverflow.com/questions/667250/strptime-in-windows
#ifdef _MSC_VER
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

const char * strp_weekdays[] = { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };
const char * strp_monthnames[] = { "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december" };
bool strp_atoi(const char * & s, int & result, int low, int high, int offset)
{
    bool worked = false;
    char * end;
    unsigned long num = strtoul(s, &end, 10);
    if (num >= (unsigned long)low && num <= (unsigned long)high)
    {
        result = (int)(num + offset);
        s = end;
        worked = true;
    }
    return worked;
}
char *strptime(const char *s, const char *format, struct tm *tm)
{
    bool working = true;
    while (working && *format && *s)
    {
        switch (*format)
        {
            case '%':
            {
                ++format;
                switch (*format)
                {
                    case 'a':
                    case 'A': // weekday name
                        tm->tm_wday = -1;
                        working = false;
                        for (size_t i = 0; i < 7; ++i)
                        {
                            size_t len = strlen(strp_weekdays[i]);
                            if (!strnicmp(strp_weekdays[i], s, len))
                            {
                                tm->tm_wday = i;
                                s += len;
                                working = true;
                                break;
                            }
                            else if (!strnicmp(strp_weekdays[i], s, 3))
                            {
                                tm->tm_wday = i;
                                s += 3;
                                working = true;
                                break;
                            }
                        }
                        break;
                    case 'b':
                    case 'B':
                    case 'h': // month name
                        tm->tm_mon = -1;
                        working = false;
                        for (size_t i = 0; i < 12; ++i)
                        {
                            size_t len = strlen(strp_monthnames[i]);
                            if (!strnicmp(strp_monthnames[i], s, len))
                            {
                                tm->tm_mon = i;
                                s += len;
                                working = true;
                                break;
                            }
                            else if (!strnicmp(strp_monthnames[i], s, 3))
                            {
                                tm->tm_mon = i;
                                s += 3;
                                working = true;
                                break;
                            }
                        }
                        break;
                    case 'd':
                    case 'e': // day of month number
                        working = strp_atoi(s, tm->tm_mday, 1, 31, 0);
                        break;
                    case 'D': // %m/%d/%y
                    {
                        const char * s_save = s;
                        working = strp_atoi(s, tm->tm_mon, 1, 12, -1);
                        if (working && *s == '/')
                        {
                            ++s;
                            working = strp_atoi(s, tm->tm_mday, 1, 31, 0);
                            if (working && *s == '/')
                            {
                                ++s;
                                working = strp_atoi(s, tm->tm_year, 0, 99, 0);
                                if (working && tm->tm_year < 69)
                                    tm->tm_year += 100;
                            }
                        }
                        if (!working)
                            s = s_save;
                    }
                        break;
                    case 'H': // hour
                        working = strp_atoi(s, tm->tm_hour, 0, 23, 0);
                        break;
                    case 'I': // hour 12-hour clock
                        working = strp_atoi(s, tm->tm_hour, 1, 12, 0);
                        break;
                    case 'j': // day number of year
                        working = strp_atoi(s, tm->tm_yday, 1, 366, -1);
                        break;
                    case 'm': // month number
                        working = strp_atoi(s, tm->tm_mon, 1, 12, -1);
                        break;
                    case 'M': // minute
                        working = strp_atoi(s, tm->tm_min, 0, 59, 0);
                        break;
                    case 'n': // arbitrary whitespace
                    case 't':
                        while (isspace((int)*s))
                            ++s;
                        break;
                    case 'p': // am / pm
                        if (!strnicmp(s, "am", 2))
                        { // the hour will be 1 -> 12 maps to 12 am, 1 am .. 11 am, 12 noon 12 pm .. 11 pm
                            if (tm->tm_hour == 12) // 12 am == 00 hours
                                tm->tm_hour = 0;
                        }
                        else if (!strnicmp(s, "pm", 2))
                        {
                            if (tm->tm_hour < 12) // 12 pm == 12 hours
                                tm->tm_hour += 12; // 1 pm -> 13 hours, 11 pm -> 23 hours
                        }
                        else
                            working = false;
                        break;
                    case 'r': // 12 hour clock %I:%M:%S %p
                    {
                        const char * s_save = s;
                        working = strp_atoi(s, tm->tm_hour, 1, 12, 0);
                        if (working && *s == ':')
                        {
                            ++s;
                            working = strp_atoi(s, tm->tm_min, 0, 59, 0);
                            if (working && *s == ':')
                            {
                                ++s;
                                working = strp_atoi(s, tm->tm_sec, 0, 60, 0);
                                if (working && isspace((int)*s))
                                {
                                    ++s;
                                    while (isspace((int)*s))
                                        ++s;
                                    if (!strnicmp(s, "am", 2))
                                    { // the hour will be 1 -> 12 maps to 12 am, 1 am .. 11 am, 12 noon 12 pm .. 11 pm
                                        if (tm->tm_hour == 12) // 12 am == 00 hours
                                            tm->tm_hour = 0;
                                    }
                                    else if (!strnicmp(s, "pm", 2))
                                    {
                                        if (tm->tm_hour < 12) // 12 pm == 12 hours
                                            tm->tm_hour += 12; // 1 pm -> 13 hours, 11 pm -> 23 hours
                                    }
                                    else
                                        working = false;
                                }
                            }
                        }
                        if (!working)
                            s = s_save;
                    }
                        break;
                    case 'R': // %H:%M
                    {
                        const char * s_save = s;
                        working = strp_atoi(s, tm->tm_hour, 0, 23, 0);
                        if (working && *s == ':')
                        {
                            ++s;
                            working = strp_atoi(s, tm->tm_min, 0, 59, 0);
                        }
                        if (!working)
                            s = s_save;
                    }
                        break;
                    case 'S': // seconds
                        working = strp_atoi(s, tm->tm_sec, 0, 60, 0);
                        break;
                    case 'T': // %H:%M:%S
                    {
                        const char * s_save = s;
                        working = strp_atoi(s, tm->tm_hour, 0, 23, 0);
                        if (working && *s == ':')
                        {
                            ++s;
                            working = strp_atoi(s, tm->tm_min, 0, 59, 0);
                            if (working && *s == ':')
                            {
                                ++s;
                                working = strp_atoi(s, tm->tm_sec, 0, 60, 0);
                            }
                        }
                        if (!working)
                            s = s_save;
                    }
                        break;
                    case 'w': // weekday number 0->6 sunday->saturday
                        working = strp_atoi(s, tm->tm_wday, 0, 6, 0);
                        break;
                    case 'Y': // year
                        working = strp_atoi(s, tm->tm_year, 1900, 65535, -1900);
                        break;
                    case 'y': // 2-digit year
                        working = strp_atoi(s, tm->tm_year, 0, 99, 0);
                        if (working && tm->tm_year < 69)
                            tm->tm_year += 100;
                        break;
                    case '%': // escaped
                        if (*s != '%')
                            working = false;
                        ++s;
                        break;
                    default:
                        working = false;
                }
            }
                break;
            case ' ':
            case '\t':
            case '\r':
            case '\n':
            case '\f':
            case '\v':
                // zero or more whitespaces:
                while (isspace((int)*s))
                    ++s;
                break;
            default:
                // match character
                if (*s != *format)
                    working = false;
                else
                    ++s;
                break;
        }
        ++format;
    }
    return (working ? (char *)s : 0);
}
#endif // _MSC_VER

ManageThreads::ManageThreads()
{
    mapThread.clear();
    thread_count = 0;
    threads_aborted = false;
}

ManageThreads::~ManageThreads()
{
    if(get().size()) {
        wxPrintf("~ManageThreads(): %i possible threads are running.\n", int(get().size()));
        for (auto it : get())
            std::cout << "-) Thread " << it.second << " (" << it.first << ")" << std::endl;
        
        abort();
        //TODO: wait for the threads to finish except for the main thread
    }
}

std::map<std::thread::id, int> &ManageThreads::get()
{
    return mapThread;
}

void ManageThreads::add()
{
    if(exists())
        return;
    
    //wxPrintf("ADDED thread with id %i\n", thread_count);
    mapThread.insert( { std::this_thread::get_id(), thread_count++ } );
}

void ManageThreads::remove()
{
    if(!exists())
        return;
    
    cleanupCurlThreadHandle();
    
    auto res = mapThread.find(std::this_thread::get_id());
    //wxPrintf("REMOVED thread with id %i\n", res->second);
    
    mapThread.erase(std::this_thread::get_id());
}

bool ManageThreads::exists()
{
    for(auto it : mapThread)
        if(it.first == std::this_thread::get_id())
            return true;
    
    return false;
}

int ManageThreads::id()
{
    for(auto it : mapThread)
        if(it.first == std::this_thread::get_id())
            return it.second;
    
    return 0;
}

void ManageThreads::abort()
{
    threads_aborted = true;
}

bool ManageThreads::aborted()
{
    return threads_aborted;
}

MyMap::MyMap()
{
    entries.clear();
    currentIndex = 0;
}

MyMap::MyMap(const std::string &first, const std::string &second)
{
    MyMap();
    insert(first, second);
}

MyMap::MyMap(const std::string &first, const int &second)
{
    MyMap();
    insert(first, second);
}

MyMap::MyMap(const std::string &first, const uint64_t &second)
{
    MyMap();
    insert(first, second);
}

MyMap::~MyMap()
{
    entries.clear();
    currentIndex = 0;
}

void MyMap::insert(const std::string &first, const std::string &second)
{
    map_structure tmp = { 0, "", "" };
    tmp.index = currentIndex++;
    tmp.first = first;
    tmp.second = second;
    
    tmp.second.reserve(20);
    
    entries.push_back(tmp);
}

void MyMap::insert(const std::string &first, const int &second)
{
    map_structure tmp = { 0, "", "" };
    tmp.index = currentIndex++;
    tmp.first = first;
    tmp.second = intToStr(second);
    
    entries.push_back(tmp);
}

void MyMap::insert(const std::string &first, const uint64_t &second)
{
    map_structure tmp = { 0, "", "" };
    tmp.index = currentIndex++;
    tmp.first = first;
    tmp.second = uint64ToStr(second);
    
    entries.push_back(tmp);
}

void MyMap::modify(const std::string &first, std::string second)
{
    for(auto &it : get())
        if(it.first == first)
            strcpy((char*)it.keke, "lol");

}

const std::vector<map_structure> &MyMap::get() const
{
    return entries;
}

const std::string &MyMap::operator[](const char *key)
{
    if(get().size() == 0)
        return emptyString;
    
    for(auto &it : get())
        if(it.first == std::string(key))
            return it.second;
    
    return emptyString;
}

std::string MyMap::toXml()
{
    std::string result;
    
    for(auto it : get())
        result += "<" + it.first + ">" + it.second + "</" + it.first + ">";
    
    return result;
}

std::string MyMap::toXmlRequest()
{
    std::string result = "<request>" + toXml() + "</request>";
    return result;
}

int MyMap::size()
{
    return currentIndex;
}

long MyMap::asLong(const char *key, int base)
{
    for(auto it : get())
        if(it.first == std::string(key))
            return (long)strToInt(it.second, base);
    
    return 0;
}

int MyMap::asInt(const char *key, int base)
{
    return int(asLong(key, base));
}

uint64_t MyMap::asUint64(const char *key, int base)
{
    for(auto it : get())
        if(it.first == std::string(key))
            return strToUInt64(it.second, base);
    
    return 0;
}
