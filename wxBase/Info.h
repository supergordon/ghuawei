//
//  Info.h
//  wxBase
//
//  Created by Gordon Freeman on 05.12.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#ifndef __wxBase__Info__
#define __wxBase__Info__

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <map>


class Info {
public:
    Info(wxWindow *win);
    ~Info();
    wxPanel *getPanel();
    void updateControls(int selection);

    std::map<int, std::string> mapChoices;
    wxListCtrl *listOutput;
    wxChoice *choice;
private:
    wxPanel *panel;
};

#endif
