//
//  Graph.cpp
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "Graph.h"

Graph::Graph(wxWindow *win)
{
    panel = new wxPanel(win, wxID_ANY);
    
    new wxStaticText(panel, wxID_ANY, "Graph");
}

wxPanel *Graph::getPanel()
{
    return panel;
}