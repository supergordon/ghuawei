//
//  Profiles.cpp
//  wxBase
//
//  Created by Gordon Freeman on 08.12.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "Profiles.h"
#include "Huawei.h"
#include "MainDialog.h"

Profiles::Profiles(wxWindow *win)
{
   // wxPrintf("Profiles()\n");
    panel = new wxPanel(win, wxID_ANY);
    
    createControls();
    setupControls();
    updateControls();
    setupSizers();
}

Profiles::~Profiles()
{
    //wxPrintf("~Profiles()\n");
}

wxPanel *Profiles::getPanel()
{
    return panel;
}

void Profiles::createControls()
{
    listProfiles = new wxListCtrl(panel, wxID_ANY, wxDefaultPosition, wxSize(-1, 120), wxLC_REPORT | wxLC_SORT_ASCENDING | wxLC_VRULES | wxLC_HRULES);
    buttonRefresh = new wxButton(panel, wxID_ANY, "Refresh", wxDefaultPosition, wxSize(80, -1));
    buttonAdd = new wxButton(panel, wxID_ANY, "Add", wxDefaultPosition, wxSize(80, -1));
    buttonEdit = new wxButton(panel, wxID_ANY, "Change", wxDefaultPosition, wxSize(80, -1));
    buttonDelete = new wxButton(panel, wxID_ANY, "Delete", wxDefaultPosition, wxSize(80, -1));
    buttonDefault = new wxButton(panel, wxID_ANY, "Set Default", wxDefaultPosition, wxSize(80, -1));
    
    labelIndex = new wxStaticText(panel, wxID_ANY, "Index:", wxDefaultPosition, wxSize(50, -1));
    labelIndexLabel = new wxStaticText(panel, wxID_ANY, "5", wxDefaultPosition, wxSize(50, -1));
    checkIsValid = new wxCheckBox(panel, wxID_ANY, "valid", wxDefaultPosition, wxSize(70, -1));
    labelReadOnly = new wxStaticText(panel, wxID_ANY, "Read Only:", wxDefaultPosition, wxSize(80, -1));
    spinReadOnly = new wxSpinCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1), wxSP_ARROW_KEYS | wxALIGN_CENTRE_HORIZONTAL, 0, 2, 0);
    
    labelName = new wxStaticText(panel, wxID_ANY, "Name:", wxDefaultPosition, wxSize(50, -1));
    textName = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    labelDialupNum = new wxStaticText(panel, wxID_ANY, "Dialup Num:", wxDefaultPosition, wxSize(80, -1));
    textDialupNum = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    labelUsername = new wxStaticText(panel, wxID_ANY, "User:", wxDefaultPosition, wxSize(50, -1));
    textUsername = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    labelPassword = new wxStaticText(panel, wxID_ANY, "Password:", wxDefaultPosition, wxSize(80, -1));
    textPassword = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    
    labelApn = new wxStaticText(panel, wxID_ANY, "APN:", wxDefaultPosition, wxSize(50, -1));
    textApn = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    checkApnIsStatic = new wxCheckBox(panel, wxID_ANY, "Static APN", wxDefaultPosition, wxSize(100, -1));
    
    labelIp = new wxStaticText(panel, wxID_ANY, "IP:", wxDefaultPosition, wxSize(50, -1));
    textIp = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    checkIp = new wxCheckBox(panel, wxID_ANY, "Static IP", wxDefaultPosition, wxSize(90, -1));
    checkDns = new wxCheckBox(panel, wxID_ANY, "Static DNS", wxDefaultPosition, wxSize(90, -1));
    
    labelPrimaryDns = new wxStaticText(panel, wxID_ANY, "DNS 1:", wxDefaultPosition, wxSize(50, -1));
    textPrimaryDns = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    labelSecondaryDns = new wxStaticText(panel, wxID_ANY, "DNS 2:", wxDefaultPosition, wxSize(80, -1));
    textSecondaryDns = new wxTextCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1));
    
    labelAuthMode = new wxStaticText(panel, wxID_ANY, "Auth Mode:", wxDefaultPosition, wxSize(80, -1));
    spinAuthMode = new wxSpinCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(90, -1), wxSP_ARROW_KEYS | wxALIGN_CENTRE_HORIZONTAL, 0, 10, 0);
    labelIpType = new wxStaticText(panel, wxID_ANY, "IP Type:", wxDefaultPosition, wxSize(80, -1));
    spinIpType = new wxSpinCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1), wxSP_ARROW_KEYS | wxALIGN_CENTRE_HORIZONTAL, 0, 10, 0);
}

void Profiles::setupSizers()
{
    wxBoxSizer *sizerMain = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticBoxSizer *sizer3 = new wxStaticBoxSizer(wxVERTICAL, panel, "Details");
    wxBoxSizer *sizer31 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer32 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer33 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer34 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer35 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer36 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer37 = new wxBoxSizer(wxHORIZONTAL);
    
    int space = 50;
    auto flags = wxSizerFlags().Border(wxUP, 2);
    
    sizer31->Add(labelIndex, flags);
    sizer31->Add(labelIndexLabel, flags);
    sizer31->Add(checkIsValid);
    sizer31->AddSpacer(space);
    sizer31->Add(labelReadOnly, flags);
    sizer31->Add(spinReadOnly);
    
    sizer32->Add(labelName, flags);
    sizer32->Add(textName);
    sizer32->AddSpacer(space);
    sizer32->Add(labelDialupNum, flags);
    sizer32->Add(textDialupNum);
    
    sizer33->Add(labelUsername, flags);
    sizer33->Add(textUsername);
    sizer33->AddSpacer(space);
    sizer33->Add(labelPassword, flags);
    sizer33->Add(textPassword);

    sizer34->Add(labelApn, flags);
    sizer34->Add(textApn);
    sizer34->AddSpacer(space);
    sizer34->Add(checkApnIsStatic);
    
    sizer35->Add(labelIp, flags);
    sizer35->Add(textIp);
    sizer35->AddSpacer(space);
    sizer35->Add(checkIp);
    sizer35->Add(checkDns);
    
    sizer36->Add(labelPrimaryDns, flags);
    sizer36->Add(textPrimaryDns);
    sizer36->AddSpacer(space);
    sizer36->Add(labelSecondaryDns, flags);
    sizer36->Add(textSecondaryDns);
    
    sizer37->Add(labelAuthMode, flags);
    sizer37->Add(spinAuthMode);
    sizer37->AddSpacer(space);
    sizer37->Add(labelIpType, flags);
    sizer37->Add(spinIpType);
    
    sizer3->Add(sizer31);
    sizer3->AddSpacer(2);
    sizer3->Add(sizer32);
    sizer3->AddSpacer(2);
    sizer3->Add(sizer33);
    sizer3->AddSpacer(2);
    sizer3->Add(sizer34);
    sizer3->AddSpacer(2);
    sizer3->Add(sizer35);
    sizer3->AddSpacer(2);
    sizer3->Add(sizer36);
    sizer3->AddSpacer(2);
    sizer3->Add(sizer37);
    
    sizer2->Add(buttonRefresh);
    sizer2->AddSpacer(5);
    sizer2->Add(buttonAdd);
    sizer2->AddSpacer(5);
    sizer2->Add(buttonEdit);
    sizer2->AddSpacer(5);
    sizer2->Add(buttonDelete);
    sizer2->AddSpacer(5);
    sizer2->Add(buttonDefault);
    
    sizerMain->Add(sizer3, 0, wxLEFT | wxRIGHT | wxGROW, 5);
    sizerMain->AddStretchSpacer();
    sizerMain->Add(listProfiles, 0, wxGROW | wxALL, 5);
    sizerMain->AddStretchSpacer();
    sizerMain->Add(sizer2, 0, wxLEFT | wxRIGHT | wxGROW, 5);
    sizerMain->AddSpacer(1);
    
    panel->SetSizerAndFit(sizerMain);
}

void Profiles::setupControls()
{
    listProfiles->SetFont(g_listFont);
    
    int c0 = listProfiles->AppendColumn("Name");
    int c1 = listProfiles->AppendColumn("APN Name", wxLIST_FORMAT_CENTRE);
    listProfiles->SetColumnWidth(c0, 200);
    listProfiles->SetColumnWidth(c1, 100);
    
    buttonRefresh->Bind(wxEVT_BUTTON, [this](wxCommandEvent &event) { OnButtonRefresh(event); });
    buttonAdd->Bind(wxEVT_BUTTON, [this](wxCommandEvent &event) { OnButtonAdd(event); });
    buttonEdit->Bind(wxEVT_BUTTON, [this](wxCommandEvent &event) { OnButtonChange(event); });
    buttonDelete->Bind(wxEVT_BUTTON, [this](wxCommandEvent &event) { OnButtonDelete(event); });
    buttonDefault->Bind(wxEVT_BUTTON, [this](wxCommandEvent &event) { OnButtonSetDefault(event); });
    
    listProfiles->Bind(wxEVT_LIST_ITEM_ACTIVATED, [this](wxCommandEvent &event) { OnButtonSetDefault(event); });
    listProfiles->Bind(wxEVT_LIST_ITEM_SELECTED, [this](wxListEvent &event) { OnItemActivated(event); });
}

void Profiles::updateControls()
{
    updateListProfiles();
}

void Profiles::updateDetails(int index)
{
    auto profile = hilink->GetProfileByIndex(index);
    
    labelIndexLabel->SetLabel(wxString::Format("%i", profile.Index));
    checkIsValid->SetValue(profile.IsValid >= 1);
    spinReadOnly->SetValue(profile.ReadOnly);
    
    textName->SetValue(profile.Name);
    textDialupNum->SetValue(profile.DialupNum);
    
    textUsername->SetValue(profile.Username);
    textPassword->SetValue(profile.Password);
    
    textApn->SetValue(profile.ApnName);
    checkApnIsStatic->SetValue(profile.ApnIsStatic == 1);
    
    textIp->SetValue(profile.IpAddress);
    checkIp->SetValue(profile.IpIsStatic == 1);
    checkDns->SetValue(profile.DnsIsStatic == 1);
    
    textPrimaryDns->SetValue(profile.PrimaryDns);
    textSecondaryDns->SetValue(profile.SecondaryDns);
    
    spinAuthMode->SetValue(profile.AuthMode);
    spinIpType->SetValue(profile.iptype);
}

void Profiles::updateListProfiles()
{
    listProfiles->DeleteAllItems();
    
    auto profiles = hilink->GetProfiles();
    
    int j = 0;
    for (auto it : profiles.profiles) {
        int i = listProfiles->InsertItem(j++, wxString::Format(" %s", it.Name));
        listProfiles->SetItem(i, 1, wxString::Format("%s", it.ApnName));
        listProfiles->SetItemData(i, it.Index);
        
        if(it.Index == profiles.CurrentProfile) {
            listProfiles->SetItemTextColour(i, wxColor(24, 116, 205));
            updateDetails(it.Index);
        }
    }
}

void Profiles::OnButtonRefresh(wxCommandEvent &event)
{
    updateControls();
}

void Profiles::OnButtonAdd(wxCommandEvent &event)
{
    hilink->ProfileAdd(
                       std::string(textName->GetValue().c_str()),
                       std::string(textApn->GetValue().c_str()),
                       std::string(textUsername->GetValue().c_str()),
                       std::string(textPassword->GetValue().c_str()),
                       int(checkIsValid->GetValue()),
                       int(checkApnIsStatic->GetValue()),
                       std::string(textDialupNum->GetValue().c_str()),
                       spinAuthMode->GetValue(),
                       int(checkIp->GetValue()),
                       std::string(textIp->GetValue().c_str()),
                       int(checkDns->GetValue()),
                       std::string(textPrimaryDns->GetValue().c_str()),
                       std::string(textSecondaryDns->GetValue().c_str()),
                       spinReadOnly->GetValue(),
                       spinIpType->GetValue());
    
    updateControls();
}

void Profiles::OnButtonChange(wxCommandEvent &event)
{
    hilink->ProfileModify(strToInt(std::string(labelIndexLabel->GetLabel().c_str())),
                          std::string(textName->GetValue().c_str()),
                          std::string(textApn->GetValue().c_str()),
                          std::string(textUsername->GetValue().c_str()),
                          std::string(textPassword->GetValue().c_str()),
                          int(checkIsValid->GetValue()),
                          int(checkApnIsStatic->GetValue()),
                          std::string(textDialupNum->GetValue().c_str()),
                          spinAuthMode->GetValue(),
                          int(checkIp->GetValue()),
                          std::string(textIp->GetValue().c_str()),
                          int(checkDns->GetValue()),
                          std::string(textPrimaryDns->GetValue().c_str()),
                          std::string(textSecondaryDns->GetValue().c_str()),
                          spinReadOnly->GetValue(),
                          spinIpType->GetValue());
    
    updateControls();
}

void Profiles::OnButtonDelete(wxCommandEvent &event)
{
    int itemIndex = LISTITEM_SELECTED_P(listProfiles);
    if(itemIndex == -1)
        return;
    
    int profileIndex = listProfiles->GetItemData(itemIndex);
    hilink->ProfileDelete(profileIndex);
    
    updateControls();
}

void Profiles::OnButtonSetDefault(wxCommandEvent &event)
{
    int itemIndex = LISTITEM_SELECTED_P(listProfiles);
    if(itemIndex == -1)
        return;
    
    int profileIndex = listProfiles->GetItemData(itemIndex);
    hilink->ProfileSetDefault(profileIndex);
    
    updateControls();
}

void Profiles::OnItemActivated(wxListEvent &event)
{
    int itemIndex = LISTITEM_SELECTED_P(listProfiles);
    if(itemIndex == -1)
        return;
    
    int profileIndex = listProfiles->GetItemData(itemIndex);
    updateDetails(profileIndex);
}