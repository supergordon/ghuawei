//
//  Settings.cpp
//  wxBase
//
//  Created by Gordon Freeman on 17.11.14.
//  Copyright (c) 2014 Gordon Freeman. All rights reserved.
//

#include "MainDialog.h"
#include "SMS.h"

#include <wx/spinctrl.h>
#include <wx/choice.h>
#include <wx/busyinfo.h>

Settings::Settings(wxWindow *win)
{
    //wxPrintf("Settings()\n");
    
    panel = new wxPanel(win, wxID_ANY);
    
    buttonProjectMode = new wxButton(panel, wxID_ANY, wxT("Project Mode"));
    buttonDebugMode = new wxButton(panel, wxID_ANY, wxT("Debug Mode"));
    buttonReboot = new wxButton(panel, wxID_ANY, wxT("Reboot"));
    buttonReconnect = new wxButton(panel, wxID_ANY, wxT("Reconnect"));
	buttonHop = new wxButton(panel, wxID_ANY, wxT("Hop"));
    checkUpnp = new wxCheckBox(panel, wxID_ANY, wxT("UPnP"));
    checkDataRoaming = new wxCheckBox(panel, wxID_ANY, wxT("Data Roaming"));
    checkDmz = new wxCheckBox(panel, wxID_ANY, wxT("DMZ"));
    checkSipAlg = new wxCheckBox(panel, wxID_ANY, wxT("SIP ALG"));
    checkFirewall = new wxCheckBox(panel, wxID_ANY, wxT("Firewall"));
    
    labelUpdateRate = new wxStaticText(panel, wxID_ANY, "Update Rate:", wxDefaultPosition, wxSize(100, -1), wxALIGN_LEFT);
    spinUpdateRate = new wxSpinCtrl(panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(120, -1), wxSP_ARROW_KEYS, 50, 10000, status->updateRate);
    
    cb_automatic = new wxCheckBox(panel, wxID_ANY, "Automatic");
	cb_gsm1900 = new wxCheckBox(panel, wxID_ANY, "GSM1900");
    cb_gsm1800 = new wxCheckBox(panel, wxID_ANY, "GSM1800");
    cb_gsm900 = new wxCheckBox(panel, wxID_ANY, "GSM900");
	cb_gsm850 = new wxCheckBox(panel, wxID_ANY, "GSM850");
    cb_wcdma2100 = new wxCheckBox(panel, wxID_ANY, "UMTS2100");
    cb_wcdma900 = new wxCheckBox(panel, wxID_ANY, "UMTS900");
    
    cb_lte_automatic = new wxCheckBox(panel, wxID_ANY, "Automatic");
    cb_lte_b1 = new wxCheckBox(panel, wxID_ANY, "LTE B1 (FDD 2100)");
    cb_lte_b3 = new wxCheckBox(panel, wxID_ANY, "LTE B3 (FDD 1800)");
    cb_lte_b7 = new wxCheckBox(panel, wxID_ANY, "LTE B7 (FDD 2600)");
    cb_lte_b8 = new wxCheckBox(panel, wxID_ANY, "LTE B8 (FDD 900)");
    cb_lte_b20 = new wxCheckBox(panel, wxID_ANY, "LTE B20 (FDD 800)");
	cb_lte_b38 = new wxCheckBox(panel, wxID_ANY, "LTE B38 (TDD 2600)");
	cb_lte_b40 = new wxCheckBox(panel, wxID_ANY, "LTE B40 (TDD 2300)");

    
    buttonApply = new wxButton(panel, wxID_ANY, "Apply");
    
    wxString arrayModes[] = { "auto", "2G only", "3G only", "4G only", "WCDMA -> GSM", "LTE -> WCDMA" };
    //labelMode = new wxStaticText(panel, wxID_ANY, "Mode:", wxDefaultPosition, wxSize(50, -1));
    choiceMode = new wxChoice(panel, wxID_ANY, wxDefaultPosition, wxSize(100, -1), 6, arrayModes);
    
    buttonConnect = new wxButton(panel, wxID_ANY, "Connect");
    buttonDisconnect = new wxButton(panel, wxID_ANY, "Disconnect");
    
    buttonSetPlmn = new wxButton(panel, wxID_ANY, "Plmn...");
    
    buttonWebPanel = new wxButton(panel, wxID_ANY, "Web Panel");
    
    setupSizer();
    setupControls();
}

Settings::~Settings()
{
    //wxPrintf("~Settings()\n");
}

wxPanel *Settings::getPanel()
{
    return panel;
}

void Settings::setupSizer()
{
    wxBoxSizer *sizer1 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer11 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *sizer12 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer11->Add(buttonProjectMode, 2);
    sizer11->Add(buttonDebugMode, 2);
    sizer11->Add(buttonReboot, 1);
    sizer11->Add(buttonReconnect, 2);
    
	sizer12->Add(buttonHop);
	sizer12->Add(buttonConnect);
    sizer12->Add(buttonDisconnect);
    sizer12->Add(buttonSetPlmn);
    sizer12->Add(buttonWebPanel);

    sizer1->Add(sizer11);
    sizer1->AddSpacer(3);
    sizer1->Add(sizer12);
    
    wxBoxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);
    sizer2->Add(checkUpnp);
    sizer2->Add(checkDataRoaming);
    sizer2->Add(checkDmz);
    sizer2->Add(checkSipAlg);
    sizer2->Add(checkFirewall);
    
    wxBoxSizer *sizer3 = new wxBoxSizer(wxHORIZONTAL);
    sizer3->Add(labelUpdateRate, 0, wxUP, 3);
    sizer3->Add(spinUpdateRate);
    
    wxStaticBoxSizer *sizer4 = new wxStaticBoxSizer(wxVERTICAL, panel, "Band and Mode");
    wxStaticBoxSizer *sizer41 = new wxStaticBoxSizer(wxVERTICAL, panel, "GSM / WCDMA");
    wxStaticBoxSizer *sizer42 = new wxStaticBoxSizer(wxVERTICAL, panel, "LTE");
    wxBoxSizer *sizer410 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer40 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer41->Add(cb_automatic);
	sizer41->Add(cb_gsm1900);
    sizer41->Add(cb_gsm1800);
    sizer41->Add(cb_gsm900);
	sizer41->Add(cb_gsm850);
    sizer41->Add(cb_wcdma2100);
    sizer41->Add(cb_wcdma900);
    
    sizer410->Add(sizer41);
    sizer410->Add(choiceMode, 0, wxALIGN_CENTRE | wxGROW | wxLEFT | wxRIGHT | wxDOWN, 5);
    
    sizer42->Add(cb_lte_automatic);
    sizer42->Add(cb_lte_b1);
    sizer42->Add(cb_lte_b3);
    sizer42->Add(cb_lte_b7);
    sizer42->Add(cb_lte_b8);
    sizer42->Add(cb_lte_b20);
	sizer42->Add(cb_lte_b38);
	sizer42->Add(cb_lte_b40);
    
    sizer40->Add(sizer410);
    sizer40->Add(sizer42);
    sizer4->Add(sizer40);
    
    sizer4->Add(buttonApply, 0, wxALIGN_CENTRE | wxGROW);
    
    wxBoxSizer *sizerMain = new wxBoxSizer(wxVERTICAL);
    sizerMain->AddSpacer(10);
    sizerMain->Add(sizer2);
    sizerMain->AddSpacer(10);
    sizerMain->Add(sizer4);
    //sizerMain->AddSpacer(10);
    //sizerMain->Add(sizer5);
    sizerMain->AddStretchSpacer();
    sizerMain->Add(sizer3);
    sizerMain->AddSpacer(10);
    sizerMain->Add(sizer1, 0, wxGROW);
    sizerMain->AddSpacer(1);
    
    panel->SetSizerAndFit(sizerMain);
}

void Settings::setupControls()
{
    if(hilink->IsDeviceOnline()) {
        checkUpnp->SetValue(hilink->IsUpnpEnabled());
        checkDataRoaming->SetValue(hilink->IsDataRoaming());
        checkDmz->SetValue(hilink->IsDmzEnabled());
        checkSipAlg->SetValue(hilink->IsSipAlgEnabled());
        checkFirewall->SetValue(hilink->IsFirewallEnabled());
    }
    spinUpdateRate->SetValue(status->updateRate);
    
    spinUpdateRate->Bind(wxEVT_SPINCTRL, [this] (wxSpinEvent &event) { OnSpinUpdaterate(event); });
    buttonProjectMode->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonProjectMode(event); });
    buttonDebugMode->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonDebugMode(event); });
    buttonReboot->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonReboot(event); });
    buttonReconnect->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonReconnect(event); });
	buttonHop->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event) { OnButtonHop(event); });
    buttonWebPanel->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event) { OnButtonWebPanel(event); });
    buttonConnect->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonConnect(event); });
    buttonDisconnect->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonDisconnect(event); });
    buttonSetPlmn->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonPlmn(event); });
    buttonApply->Bind(wxEVT_BUTTON, [this] (wxCommandEvent &event) { OnButtonApply(event); });
    
    checkUpnp->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckUpnp(event); });
    checkDataRoaming->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckDataRoaming(event); });
    checkDmz->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckDmz(event); });
    checkSipAlg->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckSipAlg(event); });
    checkFirewall->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckFirewall(event); });
    
    cb_automatic->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckAutomatic(event); });
    cb_lte_automatic->Bind(wxEVT_CHECKBOX, [this] (wxCommandEvent &event) { OnCheckLteAutomatic(event); });
    
    setBandValues();
}

void Settings::setBandValues()
{
    auto result = hilink->api_net_netmode();
    uint64_t networkband = result.asUint64("NetworkBand");
    uint64_t lteband = result.asUint64("LTEBand");
    
    if(networkband & Hilink::BAND_GSM1800)
        cb_gsm1800->SetValue(true);
    else
        cb_gsm1800->SetValue(false);

	if (networkband & Hilink::BAND_GSM1900)
		cb_gsm1900->SetValue(true);
	else
		cb_gsm1900->SetValue(false);
    
    if(networkband & Hilink::BAND_GSM900)
        cb_gsm900->SetValue(true);
    else
        cb_gsm900->SetValue(false);

	if (networkband & Hilink::BAND_GSM850)
		cb_gsm850->SetValue(true);
	else
		cb_gsm850->SetValue(false);
    
    if(networkband & Hilink::BAND_WCDMA2100)
        cb_wcdma2100->SetValue(true);
    else
        cb_wcdma2100->SetValue(false);
    
    if(networkband & Hilink::BAND_WCDMA900)
        cb_wcdma900->SetValue(true);
    else
        cb_wcdma900->SetValue(false);
    
    if(networkband == Hilink::BAND_ALL)
        cb_automatic->SetValue(true);
    else
        cb_automatic->SetValue(false);
    
    if(lteband & Hilink::LTEBAND_B1)
        cb_lte_b1->SetValue(true);
    else
        cb_lte_b1->SetValue(false);
    
    if(lteband & Hilink::LTEBAND_B3)
        cb_lte_b3->SetValue(true);
    else
        cb_lte_b3->SetValue(false);
    
    if(lteband & Hilink::LTEBAND_B7)
        cb_lte_b7->SetValue(true);
    else
        cb_lte_b7->SetValue(false);
    
    if(lteband & Hilink::LTEBAND_B8)
        cb_lte_b8->SetValue(true);
    else
        cb_lte_b8->SetValue(false);
    
    if(lteband & Hilink::LTEBAND_B20)
        cb_lte_b20->SetValue(true);
    else
        cb_lte_b20->SetValue(false);

	if (lteband & Hilink::LTEBAND_B38)
		cb_lte_b38->SetValue(true);
	else
		cb_lte_b38->SetValue(false);

	if (lteband & Hilink::LTEBAND_B40)
		cb_lte_b40->SetValue(true);
	else
		cb_lte_b40->SetValue(false);
    
    if(lteband == Hilink::LTEBAND_ALL)
        cb_lte_automatic->SetValue(true);
    else
        cb_lte_automatic->SetValue(false);
    
    int choiceStartIndex = 0;
    int netmode = hilink->GetNetworkMode();
    if(netmode == Hilink::NETMODE_AUTO) choiceStartIndex = 0;
    if(netmode == Hilink::NETMODE_2G_ONLY) choiceStartIndex = 1;
    if(netmode == Hilink::NETMODE_3G_ONLY) choiceStartIndex = 2;
    if(netmode == Hilink::NETMODE_4G_ONLY) choiceStartIndex = 3;
    if(netmode == Hilink::NETMODE_WCDMA_GSM) choiceStartIndex = 4;
    if(netmode == Hilink::NETMODE_LTE_WCDMA) choiceStartIndex = 5;
    
    choiceMode->SetSelection(choiceStartIndex);
}

std::string Settings::ScanBand(wxListCtrl* list, std::string bandName, int NetworkMode, uint64_t NetworkBand, uint64_t LTEBand)
{
    static int j = 0;
    std::string result = "Scanning on " + bandName + "\n";
    
    wxBusyInfo *busyInfo = new wxBusyInfo(result + "This can take a while!");
    
    hilink->SetNetworkModeBand(NetworkMode, NetworkBand, LTEBand);
    
    auto plmns = hilink->GetPlmnList();
    
    delete busyInfo;
    
    for(auto plmn : plmns)
        result += std::string(wxString::Format("[%s]: %s|%s [%s] (%s)\n", bandName, plmn.FullName, plmn.ShortName, plmn.Numeric, Hilink::GetPlmnStateText(plmn.State)).c_str());
    
    for(auto plmn : plmns) {
        int i = list->InsertItem(j++, wxString::Format(" %s", bandName));
        list->SetItem(i, 1, plmn.FullName);
        list->SetItem(i, 2, plmn.ShortName);
        list->SetItem(i, 3, plmn.Numeric);
        list->SetItem(i, 4, wxString::Format("%s", Hilink::GetPlmnStateText(plmn.State)));
        if(plmn.State == Hilink::PLMN_REGISTERED)
            list->SetItemTextColour(i, wxColor(24, 116, 205));
        else if(plmn.State == Hilink::PLMN_FORBIDDEN)
            list->SetItemTextColour(i, wxColor(255, 0, 0));
    }

    result += "\n";
    
    return result;
}

void Settings::OnSpinUpdaterate(wxSpinEvent &event)
{
    if(status) {
        status->updateRate = spinUpdateRate->GetValue();
        timer->Start(status->updateRate);
    }
}

void Settings::OnButtonProjectMode(wxCommandEvent &event)
{
    if(Settings::confirmChanges())
        hilink->DeviceSwitchToProjectMode();
}

void Settings::OnButtonDebugMode(wxCommandEvent &event)
{
    if(Settings::confirmChanges())
        hilink->DeviceSwitchToDebugMode();
}

void Settings::OnButtonReboot(wxCommandEvent &event)
{
    if(Settings::confirmChanges())
        hilink->DeviceReboot();
}

void Settings::OnButtonReconnect(wxCommandEvent& event)
{
	hilink->MobileReconnect();
}

void Settings::OnButtonHop(wxCommandEvent &event)
{
	hilink->MobileRenewIp();
}

void Settings::OnButtonConnect(wxCommandEvent &event)
{
    hilink->MobileReconnect();
}

void Settings::OnButtonDisconnect(wxCommandEvent &event)
{
    hilink->MobileConnect(false);
}

void Settings::OnButtonApply(wxCommandEvent &event)
{
    if(!Settings::confirmChanges())
        return;
    
    uint64_t band = 0;
    uint64_t lteband = 0;
    int netmode = 0;
    
	if(cb_gsm1900->GetValue()) band |= Hilink::BAND_GSM1900;
    if(cb_gsm1800->GetValue()) band |= Hilink::BAND_GSM1800;
    if(cb_gsm900->GetValue()) band |= Hilink::BAND_GSM900;
	if(cb_gsm850->GetValue()) band |= Hilink::BAND_GSM850;
    if(cb_wcdma2100->GetValue()) band |= Hilink::BAND_WCDMA2100;
    if(cb_wcdma900->GetValue()) band |= Hilink::BAND_WCDMA900;
    if(cb_automatic->GetValue()) band = Hilink::BAND_ALL;
    
    if(cb_lte_b1->GetValue()) lteband |= Hilink::LTEBAND_B1;
    if(cb_lte_b3->GetValue()) lteband |= Hilink::LTEBAND_B3;
    if(cb_lte_b7->GetValue()) lteband |= Hilink::LTEBAND_B7;
    if(cb_lte_b8->GetValue()) lteband |= Hilink::LTEBAND_B8;
    if(cb_lte_b20->GetValue()) lteband |= Hilink::LTEBAND_B20;
	if(cb_lte_b38->GetValue()) lteband |= Hilink::LTEBAND_B38;
	if(cb_lte_b40->GetValue()) lteband |= Hilink::LTEBAND_B40;
    if(cb_lte_automatic->GetValue()) lteband = Hilink::LTEBAND_ALL;
    
    int sel = choiceMode->GetSelection();
    switch(sel) {
        case 0: netmode = Hilink::NETMODE_AUTO; break;
        case 1: netmode = Hilink::NETMODE_2G_ONLY; break;
        case 2: netmode = Hilink::NETMODE_3G_ONLY; break;
        case 3: netmode = Hilink::NETMODE_4G_ONLY; break;
        case 4: netmode = Hilink::NETMODE_WCDMA_GSM; break;
        case 5: netmode = Hilink::NETMODE_LTE_WCDMA; break;
    };
    
    bool mobileConnected = hilink->IsMobileConnected();
    long ret1 = hilink->SetNetworkModeBand(netmode, band, lteband);
    if(mobileConnected) {
        hilink->MobileConnect(false);
        hilink->MobileConnect(true);
    }
    if(ret1)
        wxMessageBox(hilink->Errortext(ret1), "SetNetworkModeBand", wxICON_ERROR);
    
    auto ret = hilink->api_net_netmode();
    setBandValues();
}

void Settings::OnButtonPlmn(wxCommandEvent &event)
{
    wxDialog *dialog = new wxDialog(panel, wxID_ANY, "Plmn list", wxDefaultPosition, wxSize(400, 300));
    dialog->SetFont(g_globalFont);
    
    listPlmn = new wxListCtrl(dialog, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_VRULES | wxLC_HRULES);
    wxButton *buttonScan = new wxButton(dialog, wxID_ANY, "Scan", wxDefaultPosition, wxDefaultSize);
    wxButton *buttonFullScan = new wxButton(dialog, wxID_ANY, "Full Scan", wxDefaultPosition, wxDefaultSize);
    wxButton *buttonRegister = new wxButton(dialog, wxID_ANY, "Register", wxDefaultPosition, wxDefaultSize);
    dialogPlmn = dialog;
    
    int c0 = listPlmn->AppendColumn("Rat");
    int c1 = listPlmn->AppendColumn("Full Name");
    int c2 = listPlmn->AppendColumn("Short", wxLIST_FORMAT_CENTRE);
    int c3 = listPlmn->AppendColumn("Numeric", wxLIST_FORMAT_CENTRE);
    int c4 = listPlmn->AppendColumn("State");
    
    listPlmn->SetColumnWidth(c0, 90);
    listPlmn->SetColumnWidth(c1, 78);
    listPlmn->SetColumnWidth(c2, 50);
    listPlmn->SetColumnWidth(c3, 60);
    listPlmn->SetColumnWidth(c4, 75);
    
    listPlmn->SetFont(g_listFont);

    buttonScan->Bind(wxEVT_BUTTON, [=] (wxCommandEvent &event) { settings->OnScan(); });
    buttonFullScan->Bind(wxEVT_BUTTON, [=] (wxCommandEvent &event) { settings->OnFullScan(); });
    listPlmn->Bind(wxEVT_LIST_ITEM_ACTIVATED, [=] (wxListEvent &event) { settings->OnRegister(); });
    buttonRegister->Bind(wxEVT_BUTTON, [=] (wxCommandEvent &event) { settings->OnRegister(); });
    
    wxBoxSizer *sizerMain = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);
    
    sizer2->Add(buttonScan);
    sizer2->AddSpacer(5);
    sizer2->Add(buttonFullScan);
    sizer2->AddSpacer(5);
    sizer2->Add(buttonRegister);
    
    sizerMain->Add(listPlmn, 1, wxGROW | wxALL, 5);
    sizerMain->Add(sizer2, 0, wxDOWN | wxLEFT | wxRIGHT, 5);
    
    dialog->SetSizer(sizerMain);
    dialog->CenterOnScreen();
    dialog->ShowModal();
    dialog->Destroy();
}

void Settings::OnButtonWebPanel(wxCommandEvent &event)
{
#if defined(WIN32) || defined(_AMD64_)
    ShellExecute("open", "http://192.168.8.1/html/home.html", 0, 0);
#else //OSX
//#include <CoreData/CoreData.h>
/*#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
    CFStringRef url = CFStringCreateWithCString(NULL, "http://192.168.8.1/html/home.html", kCFStringEncodingASCII);
    CFURLRef pathRef = CFURLCreateWithString(NULL, url, NULL);
        
    if(pathRef) {
        LSOpenCFURLRef(pathRef, NULL);
        CFRelease(pathRef);
    }
    CFRelease(url);*/
#endif
//}
}

void Settings::OnRegister()
{
    if(listPlmn->GetSelectedItemCount() == 0) {
        bool mobileConnected = hilink->IsMobileConnected();
        hilink->MobileConnect(false);
        
        wxBusyInfo *busyInfo = new wxBusyInfo("Registering with network (auto)...\nPlease wait!");
        long ret = hilink->RegisterNetwork(true);
        
        hilink->MobileConnect(false);
        if(mobileConnected) {
            hilink->MobileConnect(true);
        }
        
        delete busyInfo;
        
        if(ret != 0)
            wxMessageBox(hilink->Errortext(ret), "Error", wxICON_ERROR);
        else {
            wxMessageBox("OK", "Success", wxICON_INFORMATION);
            dialogPlmn->Close();
        }
    }
    else {
        int item = LISTITEM_SELECTED_P(listPlmn);
        wxString rat = getItemColumnText(listPlmn, item, 0);
        wxString fullName = getItemColumnText(listPlmn, item, 1);
        wxString shortName = getItemColumnText(listPlmn, item, 2);
        wxString numeric = getItemColumnText(listPlmn, item, 3);
        wxString name = fullName; if(!name.length()) name = shortName; if(!name.length()) name = "Unknown";
        
        int _rat = -1;
        if(rat.Contains("2G"))
            _rat = 0;
        else if(rat.Contains("3G"))
            _rat = 2;
        else if(rat.Contains("4G"))
            _rat = 7;
        
        bool mobileConnected = hilink->IsMobileConnected();
        hilink->MobileConnect(false);
        
        wxBusyInfo *busyInfo = new wxBusyInfo(wxString::Format("Registering with network %s (%s) ...\nPlease wait!", name, numeric));
        long ret = 0;
        if(_rat != -1) {
            ret = hilink->RegisterNetwork(false, std::string(numeric.c_str()), _rat);
        }
        else {
            auto old = hilink->api_net_netmode();
            
            long error = 0;
			if(rat.Contains("GSM850")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM850, 0); _rat = Hilink::NETWORK_2G; }
			if(rat.Contains("GSM900")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM900, 0); _rat = Hilink::NETWORK_2G; }
			if(rat.Contains("GSM1800")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM1800, 0); _rat = Hilink::NETWORK_2G; }
			if(rat.Contains("GSM1900")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM1900, 0); _rat = Hilink::NETWORK_2G; }
			if(rat.Contains("WCDMA900")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_3G_ONLY, Hilink::BAND_WCDMA900, 0); _rat = Hilink::NETWORK_3G; }
            if(rat.Contains("WCDMA2100")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_3G_ONLY, Hilink::BAND_WCDMA2100, 0); _rat = Hilink::NETWORK_3G; }
            if(rat.Contains("LTE Band 1")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B1); _rat = Hilink::NETWORK_4G; }
            if(rat.Contains("LTE Band 3")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B3); _rat = Hilink::NETWORK_4G; }
            if(rat.Contains("LTE Band 7")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B7); _rat = Hilink::NETWORK_4G; }
            if(rat.Contains("LTE Band 8")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B8); _rat = Hilink::NETWORK_4G; }
            if(rat.Contains("LTE Band 20")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B20); _rat = Hilink::NETWORK_4G; }
			if(rat.Contains("LTE Band 38")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B38); _rat = Hilink::NETWORK_4G; }
			if(rat.Contains("LTE Band 40")) { error = hilink->SetNetworkModeBand(Hilink::NETMODE_4G_ONLY, 0, Hilink::LTEBAND_B40); _rat = Hilink::NETWORK_4G; }

            if(error != 0) { //fix above^^
                hilink->SetNetworkModeBand(old.asInt("NetworkMode"), old.asUint64("NetworkBand"), old.asUint64("LTEBand"));
                wxMessageBox(hilink->Errortext(error), "Error", wxICON_ERROR);
            }
            else {
                ret = hilink->RegisterNetwork(false, std::string(numeric.c_str()), _rat);
            }
        }
        delete busyInfo;
        
        if(ret != 0) {
            wxMessageBox(hilink->Errortext(ret), "Error", wxICON_ERROR);
        }
        else {
            listPlmn->SetItemTextColour(item, listPlmn->GetForegroundColour());
            wxMessageBox("OK", "Success", wxICON_INFORMATION);
        }
        
        if(mobileConnected) {
            hilink->MobileConnect(true);
        }
    }
}

void Settings::OnScan()
{
    bool mobileConnected = hilink->IsMobileConnected();
    hilink->MobileConnect(false);
    
    auto *busyInfo = new wxBusyInfo("Scanning for networks...\nPlease wait!");
    auto plmns = hilink->GetPlmnList();
    
    if(mobileConnected)
        hilink->MobileConnect(true);
    
    delete busyInfo;
    
    int j = 0;
    
    wxListItem item;
    item.SetText("Rat");
    listPlmn->SetColumn(0, item);
    listPlmn->DeleteAllItems();
    
    for(auto plmn : plmns) {
        int i = listPlmn->InsertItem(j++, wxString::Format(" %s", Hilink::GetNetworkModeText(plmn.Rat)));
        listPlmn->SetItem(i, 1, plmn.FullName);
        listPlmn->SetItem(i, 2, plmn.ShortName);
        listPlmn->SetItem(i, 3, plmn.Numeric);
        listPlmn->SetItem(i, 4, wxString::Format("%s", Hilink::GetPlmnStateText(plmn.State)));
        listPlmn->SetItemData(i, plmn.Index);
        if(plmn.State == Hilink::PLMN_REGISTERED)
            listPlmn->SetItemTextColour(i, wxColor(24, 116, 205));
        else if(plmn.State == Hilink::PLMN_FORBIDDEN)
            listPlmn->SetItemTextColour(i, wxColor(255, 0, 0));
    }
}

void Settings::OnFullScan()
{
    auto old = hilink->api_net_netmode();
    
    bool mobileConnected = hilink->IsMobileConnected();
    hilink->MobileConnect(false);
    
    wxListItem item;
    item.SetText("Band");
    listPlmn->SetColumn(0, item);
    listPlmn->DeleteAllItems();
    
    time_t time_ = time(NULL);
    char date[32] = { 0 };
    strftime(date, 32, "%d.%m.%y %H:%M:%S", localtime(&time_));
    
    result_scan.clear();
    result_scan += std::string(date) + "\n\n";
	result_scan += ScanBand(listPlmn, "GSM850", Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM850, Hilink::LTEBAND_NONE);
    result_scan += ScanBand(listPlmn, "GSM900", Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM900, Hilink::LTEBAND_NONE);
    result_scan += ScanBand(listPlmn, "GSM1800", Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM1800, Hilink::LTEBAND_NONE);
	result_scan += ScanBand(listPlmn, "GSM1900", Hilink::NETMODE_2G_ONLY, Hilink::BAND_GSM1900, Hilink::LTEBAND_NONE);
	result_scan += ScanBand(listPlmn, "WCDMA900", Hilink::NETMODE_3G_ONLY, Hilink::BAND_WCDMA900, Hilink::LTEBAND_NONE);
    result_scan += ScanBand(listPlmn, "WCDMA2100", Hilink::NETMODE_3G_ONLY, Hilink::BAND_WCDMA2100, Hilink::LTEBAND_NONE);
    result_scan += ScanBand(listPlmn, "LTE Band 1", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B1);
    result_scan += ScanBand(listPlmn, "LTE Band 3", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B3);
    result_scan += ScanBand(listPlmn, "LTE Band 7", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B7);
    result_scan += ScanBand(listPlmn, "LTE Band 8", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B8);
    result_scan += ScanBand(listPlmn, "LTE Band 20", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B20);
	result_scan += ScanBand(listPlmn, "LTE Band 38", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B38);
	result_scan += ScanBand(listPlmn, "LTE Band 40", Hilink::NETMODE_4G_ONLY, Hilink::BAND_NONE, Hilink::LTEBAND_B40);
	if(hilink->SetNetworkModeBand(old.asInt("NetworkMode"), old.asUint64("NetworkBand"), old.asUint64("LTEBand")) != 0)
        wxMessageBox("error restoring old modes and bands\n", "Error", wxICON_ERROR);
    
    hilink->MobileConnect(false);
    if(mobileConnected)
        hilink->MobileConnect(true);
}

void Settings::OnCheckUpnp(wxCommandEvent &event)
{
    if(Settings::confirmChanges())
        hilink->EnableUpnp(event.IsChecked());
    else
        checkUpnp->SetValue(!event.IsChecked());
    
    checkUpnp->SetValue(hilink->IsUpnpEnabled());
}

void Settings::OnCheckDataRoaming(wxCommandEvent &event)
{
    if(Settings::confirmChanges())
        hilink->SetDataRoaming(event.IsChecked());
    else
        checkDataRoaming->SetValue(!event.IsChecked());
    
    checkDataRoaming->SetValue(hilink->IsDataRoaming());
}

void Settings::OnCheckDmz(wxCommandEvent &event)
{
    if(Settings::confirmChanges()) {
        if(event.IsChecked()) {
            wxString ipAddressFilter[11] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."};
            wxArrayString arraystrIPAddress(11, ipAddressFilter);
            wxTextValidator ip_validator(wxFILTER_INCLUDE_CHAR_LIST);
            ip_validator.SetIncludes(arraystrIPAddress);
            
            auto dmz = hilink->api_security_dmz();
            
            wxTextEntryDialog *ted = new wxTextEntryDialog(NULL, "DMZ ip address needed", "DMZ", dmz["DmzIPAddress"].c_str());
            ted->SetTextValidator(ip_validator);
            ted->ShowModal();
            ted->Destroy();
            
            hilink->EnableDmz(event.IsChecked(), std::string(ted->GetValue().c_str()));
        }
        else {
            hilink->EnableDmz(event.IsChecked(), "");
        }
    }
    else {
        checkDmz->SetValue(!event.IsChecked());
    }
    
    checkDmz->SetValue(hilink->IsDmzEnabled());
}

void Settings::OnCheckSipAlg(wxCommandEvent &event)
{
    if(Settings::confirmChanges()) {
        if(event.IsChecked()) {
            auto sip = hilink->api_security_sip();
            
            wxTextEntryDialog *ted = new wxTextEntryDialog(NULL, "Choose a port", "SIP ALG", sip["SipPort"].c_str());
            ted->ShowModal();
            ted->Destroy();
            
            long port = -1;
            ted->GetValue().ToLong(&port);
            if(port == 0) port = 5060;
            if(port > 0xFFFF) port = 5060;
            
            hilink->EnableSipAlg(true, port);
        }
        else {
            hilink->EnableSipAlg(false);
        }
    }
    else {
        checkSipAlg->SetValue(!event.IsChecked());
    }
    
    checkSipAlg->SetValue(hilink->IsSipAlgEnabled());
}

void Settings::OnCheckFirewall(wxCommandEvent &event)
{
    if(Settings::confirmChanges())
        hilink->EnableFirewall(event.IsChecked());
    else
        checkFirewall->SetValue(!event.IsChecked());
    
    checkFirewall->SetValue(hilink->IsFirewallEnabled());
}

void Settings::OnCheckAutomatic(wxCommandEvent &event)
{
    bool toggle = event.IsChecked();
    
    cb_gsm1800->SetValue(toggle);
    cb_gsm900->SetValue(toggle);
    cb_wcdma2100->SetValue(toggle);
    cb_wcdma900->SetValue(toggle);
}

void Settings::OnCheckLteAutomatic(wxCommandEvent &event)
{
    bool toggle = event.IsChecked();
    
    cb_lte_b1->SetValue(toggle);
    cb_lte_b3->SetValue(toggle);
    cb_lte_b7->SetValue(toggle);
    cb_lte_b8->SetValue(toggle);
    cb_lte_b20->SetValue(toggle);
}

bool Settings::confirmChanges()
{
    return true;
    int yesno = wxMessageBox("Do you want to process this operation?", "Please confirm your changes", wxYES_NO | wxYES_DEFAULT | wxICON_EXCLAMATION);
    
    return yesno == wxYES;
}
